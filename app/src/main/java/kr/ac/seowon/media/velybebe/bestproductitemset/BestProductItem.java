package kr.ac.seowon.media.velybebe.bestproductitemset;

public class BestProductItem {
    public int pk;

    public String imageUrl;
    public String prodoctTitle;
    public String productPrice;

    public boolean coupon;
    public boolean sale;
    public boolean gift;

    public BestProductItemViewHolder viewHolder;



    public void setPk(int n) {
        this.pk = n;
    }
    public int getPk() {
        return this.pk;
    }



    public void setProductImageUrl(String s) {
        imageUrl = s;
    }
    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setProductTitle(String s) {
        this.prodoctTitle = s;
    }
    public String getProdoctTitle() {
        return this.prodoctTitle;
    }


    public void setProductPrice(String s) {
        this.productPrice = s;
    }
    public String getProductPrice() {
        return this.productPrice;
    }

    public void setCoupon(boolean b) {
        this.coupon = b;
    }
    public boolean getCoupon() {
        return this.coupon;
    }

    public void setSale(boolean b) {
        this.sale = b;
    }
    public boolean getSale() {
        return this.sale;
    }

    public void setGift(boolean b) {
        this.gift = b;
    }
    public boolean getGift() {
        return this.gift;
    }

    public void setViewHolder(BestProductItemViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }
    public BestProductItemViewHolder getViewHolder() {
        return this.viewHolder;
    }
}

