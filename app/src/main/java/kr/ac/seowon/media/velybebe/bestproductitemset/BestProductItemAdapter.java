package kr.ac.seowon.media.velybebe.bestproductitemset;

import android.app.Activity;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


import java.util.List;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.LoadingViewHolder;
import kr.ac.seowon.media.velybebe.common.OnLoadMoreListener;
import kr.ac.seowon.media.velybebe.common.UrlImageGetDisplay;
import kr.ac.seowon.media.velybebe.common.UseFul;

public class BestProductItemAdapter extends RecyclerView.Adapter {
    private AppCompatActivity activity;

    private List<BestProductItem> items;

    public boolean isLoading;
    public final int VIEW_TYPE_ITEM = 0;
    public final int VIEW_TYPE_LOADING = 1;
    public int visibleThreshold = 5;
    public int lastVisibleItem, totalItemCount;
    public LinearLayoutManager linearLayoutManager;

    public OnLoadMoreListener onLoadMoreListener;
    private View.OnClickListener itemsOnClickListener;

    public boolean isMoreInfo;
    public String getMoreInfoType = "scroll"; // or button

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    public BestProductItemAdapter(RecyclerView recyclerView, List<BestProductItem> items, final AppCompatActivity activity, View.OnClickListener itemsOnClickListener) {
        this.items = items;
        this.activity = activity;
        this.itemsOnClickListener = itemsOnClickListener;

        linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(activity).inflate(R.layout.best_product_item, parent, false);
            return new BestProductItemViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof BestProductItemViewHolder) {
            BestProductItem item = items.get(position);
            BestProductItemViewHolder userViewHolder = (BestProductItemViewHolder) holder;

            item.setViewHolder(userViewHolder);

            userViewHolder.view.setTag(item);
            userViewHolder.view.setOnClickListener(itemsOnClickListener);

            // 상품 이미지 뿌리기
            UrlImageGetDisplay u = new UrlImageGetDisplay(activity);
            u.setImageUrl(item.getImageUrl());
            u.setImageView(userViewHolder.product_image);
            u.start();

            userViewHolder.product_title.setText(item.getProdoctTitle());
            userViewHolder.product_price.setText(item.getProductPrice());

            if (item.getCoupon()) {
                userViewHolder.coupon.setVisibility(View.VISIBLE);
            }

            if (item.getGift()) {
                userViewHolder.gift.setVisibility(View.VISIBLE);
            }

            if (item.getSale()) {
                userViewHolder.sale.setVisibility(View.VISIBLE);
            }

            // 왼쪽 뷰는 marginRight 5dp로 오른쪽 뷰는 marginLeft 5dp로  설정
            if ((position+1) % 2 != 0) {
                GridLayoutManager.LayoutParams params1 = (GridLayoutManager.LayoutParams) userViewHolder.view.getLayoutParams();
                params1.setMarginEnd(new UseFul(activity).getRdimen(R.dimen.mobile05100_best_product_item_interval));
                userViewHolder.view.setLayoutParams(params1);
            } else {
                GridLayoutManager.LayoutParams params1 = (GridLayoutManager.LayoutParams) userViewHolder.view.getLayoutParams();
                params1.setMarginStart(new UseFul(activity).getRdimen(R.dimen.mobile05100_best_product_item_interval));
                userViewHolder.view.setLayoutParams(params1);
            }

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    /**************************************************************/
}
