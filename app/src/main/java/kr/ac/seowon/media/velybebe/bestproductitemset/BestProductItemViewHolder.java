package kr.ac.seowon.media.velybebe.bestproductitemset;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import kr.ac.seowon.media.velybebe.R;


public class BestProductItemViewHolder extends RecyclerView.ViewHolder {
    View view;

    public AppCompatImageView product_image;
    public TextView product_title;
    public TextView product_price;

    public LinearLayout coupon;
    public LinearLayout gift;
    public LinearLayout sale;

    /**************************************************************/

    public BestProductItemViewHolder(View view) {
        super(view);
        this.view = view;

        product_image = (AppCompatImageView) view.findViewById(R.id.product_image);
        product_title = (TextView) view.findViewById(R.id.product_title);
        product_price = (TextView) view.findViewById(R.id.product_price);

        coupon = (LinearLayout) view.findViewById(R.id.coupon);
        gift = (LinearLayout) view.findViewById(R.id.gift);
        sale = (LinearLayout) view.findViewById(R.id.sale);
    }
}
