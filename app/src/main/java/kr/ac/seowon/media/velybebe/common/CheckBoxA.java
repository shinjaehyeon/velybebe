package kr.ac.seowon.media.velybebe.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import kr.ac.seowon.media.velybebe.R;

public class CheckBoxA extends ConstraintLayout {
    View v;
    AppCompatActivity activity;
    Context context;
    UseFul useFul;

    /*===================================================================================================================*/

    int value;

    View chainView;

    boolean isChecked = false;

    ConstraintLayout check_icon_box;
    AppCompatImageView check_icon;

    /*===================================================================================================================*/

    public CheckBoxA(Context context) {
        super(context);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        inflateView();
    }

    public CheckBoxA(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        getAttrs(attrs);
        inflateView();
    }

    public CheckBoxA(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        getAttrs(attrs, defStyleAttr);
        inflateView();
    }


    /*===================================================================================================================*/

    public void inflateView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.checkbox_a, this, false);
        addView(v);

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기
    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        useFul = new UseFul(activity);


        check_icon_box = (ConstraintLayout) v.findViewById(R.id.check_icon_box);
        check_icon = (AppCompatImageView) v.findViewById(R.id.check_icon);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        // 체크박스를 눌렀을 때 이벤트 설정하기
        check_icon_box.setOnClickListener(checkBoxTouchEvent);
    }

    View.OnClickListener checkBoxTouchEvent = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isChecked == false) {
                isChecked = true;

                check_icon_box.setBackgroundResource(R.drawable.background_border_4);
                check_icon.setImageResource(R.drawable.ic_mobile00000_check_icon_white);
            } else {
                isChecked = false;

                check_icon_box.setBackgroundResource(R.drawable.background_border_3);
                check_icon.setImageResource(R.drawable.ic_mobile00000_check_icon_color);
            }
        }
    };

    /*===================================================================================================================*/

    // 체크되어 있는지 안되어 있는지 여부 값을 반환함
    public boolean getValue() {
        return isChecked;
    }

    public void setCheckboxValue(int v) {
        this.value = v;
    }
    public int getCheckboxValue() {
        return this.value;
    }

    // 체크된 혹은 체크되지 않은 상태로 셋팅함
    public void setChecked(boolean b) {
        if (b) {
            isChecked = true;

            check_icon_box.setBackgroundResource(R.drawable.background_border_4);
            check_icon.setImageResource(R.drawable.ic_mobile00000_check_icon_white);
        } else {
            isChecked = false;

            check_icon_box.setBackgroundResource(R.drawable.background_border_3);
            check_icon.setImageResource(R.drawable.ic_mobile00000_check_icon_color);
        }
    }

    // 체크박스와 연동하여 터치시 같은 역할을 할 뷰 셋팅
    public void setChainView(View v) {
        chainView = v;
        chainView.setOnClickListener(checkBoxTouchEvent);
    }

    // 다른 이벤트를 주고 싶을 경우 설정
    public void setCheckboxCustomEvent(View.OnClickListener e) {
        check_icon_box.setOnClickListener(e);
        if (chainView != null) {
            chainView.setOnClickListener(e);
        }
    }





    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CheckBoxA);
        setTypeArray(typedArray);
    }

    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CheckBoxA, defStyle, 0);
        setTypeArray(typedArray);

    }

    private void setTypeArray(TypedArray typedArray) {
        // SNS 로그인 아이콘 설정
        int int_value = typedArray.getInt(R.styleable.CheckBoxA_setIntegerValue, 0);
        Log.i("MyTags", "int_value = "+int_value);
        this.value = int_value;


        typedArray.recycle();
    }
}
