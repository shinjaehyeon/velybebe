package kr.ac.seowon.media.velybebe.common;

import android.view.animation.Interpolator;

public class HesitateInterpolator implements Interpolator {
    public HesitateInterpolator() {}
    public float getInterpolation(float t) {
        float x=2.0f*t-1.0f;

        float a = 0.17f;
        float b = 0.67f;
        float c = 0f;
        float d = 0.97f;

        float y = (a * (1-t)*(1-t)*(1-t)) + (3 * b * t * (1-t) * (1-t)) + (3 * c * t * t * (1-t)) + (d * t * t * t);

        // return 0.5f*(x*x*x + 1.0f);
        return y;
    }
}
