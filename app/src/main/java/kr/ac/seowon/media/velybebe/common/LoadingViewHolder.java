package kr.ac.seowon.media.velybebe.common;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import kr.ac.seowon.media.velybebe.R;


/**
 * Created by shinjaehyeon on 2018-02-10.
 */

public class LoadingViewHolder extends RecyclerView.ViewHolder {
    public ProgressBar progressBar;

    public LoadingViewHolder(View view) {
        super(view);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
    }
}
