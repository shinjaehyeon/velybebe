package kr.ac.seowon.media.velybebe.common;

/*===================================================================================================================*/

public class Memo {

    public void inLetSet() {
        /*
        ContentValues params = new ContentValues();
        params.put("act", "mobile1001");
        params.put("member", new ShareInfo(this).getMember());
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(1);
        inLet.start();
        */
    }

    public void dialog() {
        /*
        */
    }

    public void customViewSet() {
        /*===================================================================================================================*/
        /*
        AppCompatActivity activity;
        Context context;
        UseFul useFul;
        View v;


        public 생성자(Context context) {
            super(context);
            this.context = context;
            this.activity = (AppCompatActivity) context;
            useFul = new UseFul(activity);
        }

        public 생성자(Context context, AttributeSet attrs) {
            super(context, attrs);
            this.context = context;
            this.activity = (AppCompatActivity) context;
            useFul = new UseFul(activity);
        }

        public 생성자(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
            this.context = context;
            this.activity = (AppCompatActivity) context;
            useFul = new UseFul(activity);
        }

        public void inflateView() {
            String infService = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
            v = li.inflate(R.layout.mobile5020_filter, this, false);
            addView(v);
        }

        // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        public void setConnectViewId() {


        }

        public void setRealInItView() {
            setViewSize(); // 필요한 뷰 사이즈 조정하기
            setObjectSetting(); // 객체에 여러가지 설정하기
            setObjectEvent(); // 객체에 이벤트 설정하기
        }



        // 필요한 뷰 사이즈 조정하기
        public void setViewSize() {

        }

        // 객체에 여러가지 설정하기
        public void setObjectSetting() {

        }

        // 객체에 이벤트 설정하기
        public void setObjectEvent() {

        }

         */
        /*===================================================================================================================*/
    }

    public void setFragmentSet() {
        /*
        AppCompatActivity activity;
        Context context;
        UseFul useFul;
        View v;

        @SuppressLint("ValidFragment")
        public Mobile5120(Context context) {
            this.context = context;
            this.activity = (AppCompatActivity) context;
            useFul = new UseFul(activity);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
            v = inflater.inflate(R.layout.mobile4600, parentViewGroup, false);

            setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
            setObjectSetting(); // 객체에 여러가지 설정하기
            setObjectEvent(); // 객체에 이벤트 설정하기
            setViewSize(); // 필요한 뷰 사이즈 조정하기

            return v;
        }

        // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        public void setConnectViewId() {

        }

        // 객체에 여러가지 설정하기
        public void setObjectSetting() {

        }

        // 객체에 이벤트 설정하기
        public void setObjectEvent() {

        }

        // 필요한 뷰 사이즈 조정하기
        public void setViewSize() {

        }
         */
    }

    public void activityBasicMethodSet() {
        /*===================================================================================================================*/
        /*

        Activity activity;
        Context context;
        DrawerLayoutSetting drawerLayoutSetting;
        CustomToolbar topbar;
        CustomBottomMenubar bottomMenubar;
        CustomBottomMenubarEventSet bottomMenubarEventSet;
        UseFul useFul;
        LayoutInflater mInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);


        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);

                switch (msg.what) {
                    case 9999:

                        break;
                    default:

                        break;
                }
            }
        };





        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        getIntentInfo(); // 인텐트 정보 받아오기

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setDrawerLayout(); // DrawerLayout 설정하기

        // 액티비티 값을 글로벌 변수에 저장하기
        public void setSaveActivity(Activity activity) {
            this.activity = activity;
            this.context = activity;
        }

        // 인테트 정보 받아오기
        public void getIntentInfo() {

        }

        // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        public void setConnectObjectAndId() {
            topbar = (CustomToolbar) findViewById(R.id.topbar);
            bottomMenubar = (CustomBottomMenubar) findViewById(R.id.bottomMenubar);
            bottomMenubarEventSet = new CustomBottomMenubarEventSet(activity, bottomMenubar);
            useFul = new UseFul(activity);


        }

        // 필요한 뷰 사이즈 조정하기
        public void setViewSize() {

        }

        // 객체에 여러가지 설정하기
        public void setObjectSetting() {

        }

        // 객체에 이벤트 설정하기
        public void setObjectEvent() {

        }

        // DrawerLayout 설정하기
        public void setDrawerLayout() {
            drawerLayoutSetting = new DrawerLayoutSetting(activity);
        }





        public void callVelyBebeNormalDialog(String title, String content) {
            final VelyBebeNormalDialog dialog = new VelyBebeNormalDialog(this);
            dialog.show();
            dialog.setDialogTitle(title);
            dialog.setDialogContent(content);
            dialog.buttonVisibleOptions(true, true);
            // dialog.setCancelButtonText("");
            // dialog.setConfirmButtonText("");
            dialog.setCancelButtonEvent(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.setConfirmButtonEvent(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    finish();
                }
            });
            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {

                }
            });
        }




        @Override
        public void onBackPressed() {
            //super.onBackPressed();
            if (!bottomMenubar.isMoved()) { // DrawerLayout이 움직이지 않을 때만 실행
                if (bottomMenubar.isOpened() == true) {
                    bottomMenubar.closeDrawerLayout();
                } else {
                    callSUHDialogType1Dialog("안내", "정말 서울대학교병원 앱을 종료하시겠습니까?");
                }
            }
        }

        @Override
        protected void attachBaseContext(Context newBase) {
            super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
        }

         */

        /*===================================================================================================================*/
    }

    public void goToActivityCode() {
        /*
        public void goToActivity (Class c){
            Intent intent = new Intent(context, c);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            ((Activity) context).startActivity(intent);
        }
        */
    }

    public void customViewAttrCode() {
        /*
        // 상단바 배경 설정
        int bgRes = typedArray.getResourceId(R.styleable.Mobile0000_customtopbar_setTopBarBackground, R.drawable.ripple_button_type_3);
        custom_toolbar.setBackgroundResource(bgRes);

        // 상단바 타이틀 텍스트 설정
        String title_str = typedArray.getString(R.styleable.Mobile0000_customtopbar_setTitle);
        title.setText(title_str);

        // 상단바 타이틀 텍스트 색상 설정
        int textColor = typedArray.getColor(R.styleable.Mobile0000_customtopbar_setTitleColor, 0);
        title.setTextColor(textColor);

        // 상단바 타이틀 텍스트 크기 설정
        float textSize = (float) typedArray.getDimension(R.styleable.Mobile0000_customtopbar_setTitleSize, 0);
        textSize = textSize / getResources().getDisplayMetrics().density;
        title.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);

        // 상단바 상단,하단 선 높이 설정
        int topHeight = (int) typedArray.getDimension(R.styleable.Mobile0000_customtopbar_setBorderTopHeight, 0);
        int bottomHeight = (int) typedArray.getDimension(R.styleable.Mobile0000_customtopbar_setBorderBottomHeight, 0);
        LayoutParams lp1 = (LayoutParams) top_border.getLayoutParams();
        lp1.height = topHeight;
        top_border.setLayoutParams(lp1);
        LayoutParams lp2 = (LayoutParams) bottom_border.getLayoutParams();
        lp2.height = bottomHeight;
        bottom_border.setLayoutParams(lp2);

        // 상단바 상단,하단 선 색상 설정
        int topColor = typedArray.getColor(R.styleable.Mobile0000_customtopbar_setBorderTopColor, 0);
        top_border.setBackgroundColor(topColor);
        int bottomColor = typedArray.getColor(R.styleable.Mobile0000_customtopbar_setBorderBottomColor, 0);
        bottom_border.setBackgroundColor(bottomColor);

        // 뒤로가기 버튼을 보이게 할건지 말건지 설정
        boolean isVisible = typedArray.getBoolean(R.styleable.Mobile0000_customtopbar_setBackButtonVisible, true);
        if (isVisible) {
            backButton.setVisibility(View.VISIBLE);
        } else {
            backButton.setVisibility(View.GONE);
        }
         */
    }

    public void recyclerViewSetting() {
        /*

        // 리사이클러뷰 관련 환경 설정하기
        public void setRecyclerView() {
            recyclerView.setNestedScrollingEnabled(false);

            mLayoutManager = new GridLayoutManager(activity,1);
            mLayoutManager.setAutoMeasureEnabled(true);

            recyclerView.setLayoutManager(mLayoutManager);

            crhItemAdapter = new CRHItemAdapter(recyclerView, crhItems, activity, null, scrollView);
            recyclerView.setAdapter(crhItemAdapter);

            crhItemAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    crhItems.add(null);
                    crhItemAdapter.notifyItemInserted(crhItems.size() - 1);

                    mobile3010();
                }
            });

            // 스크롤로 더보기 구현하고 싶으면
            scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
                @Override
                public void onScrollChanged() {
                    int scrollViewPos = scrollView.getScrollY();
                    int TextView_lines = scrollView.getChildAt(0).getBottom() - scrollView.getHeight();

                    final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                    if (crhItemAdapter.isMoreInfo == true) {
                        if (TextView_lines == scrollViewPos) {
                            crhItemAdapter.totalItemCount = linearLayoutManager.getItemCount();
                            crhItemAdapter.lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                            if (!crhItemAdapter.isLoading && crhItemAdapter.totalItemCount <= (crhItemAdapter.lastVisibleItem + crhItemAdapter.visibleThreshold)) {
                                crhItemAdapter.isLoading = true;

                                if (crhItemAdapter.onLoadMoreListener != null) {
                                    crhItemAdapter.onLoadMoreListener.onLoadMore();
                                }

                            }
                        }
                    }

                }
            });

            // 버튼으로 더보기 구현하고 싶으면
            moreViewButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (crhItemAdapter.isMoreInfo == true) {
                        if (!crhItemAdapter.isLoading) {
                            crhItemAdapter.isLoading = true;
                            if (crhItemAdapter.onLoadMoreListener != null) {
                                crhItemAdapter.onLoadMoreListener.onLoadMore();
                            }
                        }
                    }
                }
            });
        }

         */
    }


    public void progressLayoutSet() {
        /*
        <!-- 로딩 이벤트 레이아웃 시작 -->
        <RelativeLayout
            android:id="@+id/progressBarLayout"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:layout_alignParentLeft="true"
            android:layout_alignParentTop="true"
            android:background="#B3000000"
            android:visibility="gone">
            <ProgressBar
                android:id="@+id/progressBar"
                android:layout_width="50dp"
                android:layout_height="50dp"
                android:layout_marginLeft="30dp"
                android:layout_marginRight="30dp"
                android:indeterminateTintMode="src_atop"
                android:indeterminateTint="#008ace"
                android:layout_centerInParent="true" />
        </RelativeLayout>
        <!-- 로딩 이벤트 레이아웃 종료 -->
         */
    }

    public void theSetAndGetMethod() {
        /*




         */
    }
}
