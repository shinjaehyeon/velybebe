package kr.ac.seowon.media.velybebe.common;

/**
 * Created by shinjaehyeon on 2018-02-10.
 */

public interface OnLoadMoreListener {
     void onLoadMore();
}
