package kr.ac.seowon.media.velybebe.common;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import kr.ac.seowon.media.velybebe.permission.Mobile02000;

public class PermissionCheck {
    AppCompatActivity activity;
    Context context;

    /*===================================================================================================================*/

    private static String[] PERMISSION_ALL = {
            Manifest.permission.INTERNET,
            Manifest.permission.CHANGE_WIFI_MULTICAST_STATE,
            Manifest.permission.CHANGE_WIFI_STATE,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.VIBRATE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static String[] PERMISSION_ALL_CHECK = {
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    /*===================================================================================================================*/

    public PermissionCheck(AppCompatActivity activity) {
        this.activity = activity;
        this.context = (Context) activity;
    }

    public PermissionCheck(Context context) {
        this.context = context;
        this.activity = (AppCompatActivity) context;
    }

    /*===================================================================================================================*/

    public boolean checkPermissions() throws IllegalAccessException {
        int[] grantlist = new int[PERMISSION_ALL.length];
        Log.i("MyTag", "permission.length = "+PERMISSION_ALL.length);
        for (int i=0; i<PERMISSION_ALL.length; i++) {
            grantlist[i] = ContextCompat.checkSelfPermission(activity, PERMISSION_ALL[i]);
        }
        for (int i=0; i<grantlist.length; i++) {
            Log.i("MyTag", "grantlist["+i+"] = "+grantlist[i]);
            if (grantlist[i] == PackageManager.PERMISSION_DENIED) {
                return false;
            }
        }
        return true;
    }

    public void isAllPermissionChecked() {
        try {
            if (!checkPermissions()) {
                // 하나라도 권한이 허용 되지 않은게 있다면
                // 경고창 띄우고 앱 종료하기
                callVelyBebeNormalDialog("경고", "임의로 권한을 해제한 부분이 있습니다. 모든 권한을 허용으로 다시 체크하신 후 앱을 재실행해주세요.");
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /*===================================================================================================================*/

    public void callVelyBebeNormalDialog(String title, String content) {
        final VelyBebeNormalDialog dialog = new VelyBebeNormalDialog(activity);
        dialog.show();
        dialog.setDialogTitle(title);
        dialog.setDialogContent(content);
        dialog.buttonVisibleOptions(false, true);
        dialog.startDialogAnimation();
        // dialog.setCancelButtonText("");
        // dialog.setConfirmButtonText("");
        dialog.setCancelButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setConfirmButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                activity.finishAffinity();
            }
        });
    }

    public void goActivity(Class c) {
        Intent intent = new Intent(activity, c);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        activity.startActivity(intent);
    }
}
