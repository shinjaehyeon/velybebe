package kr.ac.seowon.media.velybebe.common;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import kr.ac.seowon.media.velybebe.R;

public class RadioButtonA extends ConstraintLayout {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;
    View v;


    int value;
    boolean isChecked = false;
    View containView;


    ConstraintLayout radio_button_box;
    LinearLayout radio_symbol;



    public RadioButtonA(Context context) {
        super(context);
        this.activity = (AppCompatActivity) context;
        this.context = context;
        useFul = new UseFul(activity);
        inflateView();
    }

    public RadioButtonA(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.activity = (AppCompatActivity) context;
        this.context = context;
        useFul = new UseFul(activity);
        inflateView();
    }

    public RadioButtonA(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.activity = (AppCompatActivity) context;
        this.context = context;
        useFul = new UseFul(activity);
        inflateView();
    }

    public void inflateView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.radio_button_a, this, false);
        addView(v);

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기
    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        useFul = new UseFul(activity);

        radio_button_box = (ConstraintLayout) v.findViewById(R.id.radio_button_box);
        radio_button_box.setTag(this);
        radio_symbol = (LinearLayout) v.findViewById(R.id.radio_symbol);

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {


    }



    public void setChecked(boolean b) {
        if (b) {
            isChecked = true;

            radio_symbol.setVisibility(View.VISIBLE);
        } else {
            isChecked = false;

            radio_symbol.setVisibility(View.GONE);
        }
    }

    public boolean isChecked() {
        return this.isChecked;
    }


    public void setValue(int n) {
        this.value = n;
    }

    public int getValue() {
        return this.value;
    }

    public void setContainView(View v) {
        this.containView = v;
        containView.setTag(this);

    }

    public void setRadioButtonClickEvent(View.OnClickListener e) {
        radio_button_box.setOnClickListener(e);
        if (containView != null) {
            containView.setOnClickListener(e);
        }
    }


}
