package kr.ac.seowon.media.velybebe.common;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

public class RadioButtonManager {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;

    /*===================================================================================================================*/

    ArrayList<RadioButtonA> radios = new ArrayList<RadioButtonA>();

    /*===================================================================================================================*/

    public RadioButtonManager(AppCompatActivity activity) {
        this.activity = activity;
        this.context = activity;
        useFul = new UseFul(activity);
    }



    /*===================================================================================================================*/

    // 라디오 버튼 객체(참조주소)를 배열에 저장하기
    public void pushRadioButton(RadioButtonA r) {
        this.radios.add(r);
    }

    //
    public void pushComplete() {
        for (int i=0; i<radios.size(); i++) {
            radios.get(i).setRadioButtonClickEvent(radioButtonClickEvent);
        }
    }

    // 라디오 버튼중에 체크된 라디오의 값을 가져온다.
    public int getCheckedRadioValue() {
        for (int i=0; i<radios.size(); i++) {
            if (radios.get(i).isChecked()) {
                return radios.get(i).getValue();
            }
        }
        return -1;
    }

    public void setDefaultCheckedIndex(int index) {
        radios.get(index).setChecked(true);
    }

    View.OnClickListener radioButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            for (int i=0; i<radios.size(); i++) {
                if (radios.get(i).isChecked()) {
                    radios.get(i).setChecked(false);
                }
            }


            RadioButtonA radio = (RadioButtonA) v.getTag();
            Log.i("MyYagss", "value = "+radio.getValue());
            radio.setChecked(true);

        }
    };

}
