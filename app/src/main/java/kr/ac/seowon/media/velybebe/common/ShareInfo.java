package kr.ac.seowon.media.velybebe.common;

import android.content.Context;
import android.content.SharedPreferences;

public class ShareInfo {
    String FILE_NAME = "velybebe";

    /*************************************************************/

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    /*************************************************************/

    Context context;

    /*************************************************************/

    public ShareInfo(Context context) {
        this.context = context;

        setConnectViewId();
    }
    public ShareInfo(Context context, String filename) {
        this.context = context;
        FILE_NAME = filename;

        setConnectViewId();
    }

    /*************************************************************/

    public void setConnectViewId() {
        sharedPreferences = context.getSharedPreferences(FILE_NAME, context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    /*************************************************************/

    public void setInputIntValue(String key, int value) {
        editor.putInt(key, value);
        editor.commit();
    }

    public void setInputIntValue(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    /*************************************************************/

    public int getMember() {
        return sharedPreferences.getInt("member", 0);
    }

    public int getMemberSingUpMethod() {
        return sharedPreferences.getInt("member_signupmethod", 0);
    }

    public String getMemberAccessToken() {
        return sharedPreferences.getString("member_access_token", "");
    }

    public int getAppRunning() {
        return sharedPreferences.getInt("appRunning", 0);
    }
}
