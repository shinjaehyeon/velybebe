package kr.ac.seowon.media.velybebe.common;

import android.content.ContentValues;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.mypage.Mobile09000;
import kr.ac.seowon.media.velybebe.sidebar.SideBarMenuButton;

public class SideMenuBar extends LinearLayout {
    Context context;
    AppCompatActivity activity;
    UseFul useFul;
    View v;

    LinearLayout side_menu_bar_box;

    LinearLayout rental_product_list_box;
    LinearLayout event_list_box;
    LinearLayout donate_list_box;


    TextView name_textview;

    Button mypage_button;
    Button order_delivery_button;
    Button donate_history_button;
    Button customer_center_button;



    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 99: // 프로필 정보 가져오기 후처리
                    side_bar_profile_info_get_after(msg.obj);
                    break;
            }
        }
    };



    public SideMenuBar(Context context) {
        super(context);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }

    public SideMenuBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }

    public SideMenuBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }


    public void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.side_menu_bar, this, false);
        addView(v);

        ConstraintLayout.LayoutParams params1 = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT);
        params1.width = new UseFul(context).getRdimen(R.dimen.side_menu_bar_width);
        params1.bottomToBottom = ConstraintLayout.LayoutParams.BOTTOM;
        params1.topToTop = ConstraintLayout.LayoutParams.TOP;
        params1.leftToLeft = ConstraintLayout.LayoutParams.LEFT;
        params1.orientation = LinearLayout.VERTICAL;
        setLayoutParams(params1);


        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기


        setMenuButtons(); // 메뉴버튼 뿌리기

        side_bar_profile_info_get(); // 프로필 정보 가져오기
    }

    public void setConnectObjectAndId() {
        side_menu_bar_box = (LinearLayout) v.findViewById(R.id.side_menu_bar_box);

        rental_product_list_box = (LinearLayout) v.findViewById(R.id.rental_product_list_box);
        event_list_box = (LinearLayout) v.findViewById(R.id.event_list_box);
        donate_list_box = (LinearLayout) v.findViewById(R.id.donate_list_box);

        name_textview = (TextView) v.findViewById(R.id.name_textview);

        mypage_button = (Button) findViewById(R.id.mypage_button);
        order_delivery_button = (Button) findViewById(R.id.order_delivery_button);
        donate_history_button = (Button) findViewById(R.id.donate_history_button);
        customer_center_button = (Button) findViewById(R.id.customer_center_button);

    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        mypage_button.setOnClickListener(topButtonClickEvent);
        order_delivery_button.setOnClickListener(topButtonClickEvent);
        donate_history_button.setOnClickListener(topButtonClickEvent);
        customer_center_button.setOnClickListener(topButtonClickEvent);
    }

    public void setMenuButtons() {
        Log.i("MyTags", "setMenuButtons 실행");

        ArrayList<SideBarMenuButton> aa1 = new ArrayList<SideBarMenuButton>();
        String[] aa = {"전체", "장난감", "출산/육아", "교육/완구", "의류", "가구/침구", "외출", "기타"};
        for (int i=0; i<aa.length; i++) {
            SideBarMenuButton button = new SideBarMenuButton(context);
            button.setButtonText(aa[i]);
            button.setButtonOnClickListener(sideMenuButtonClickEvent);
            aa1.add(button);
        }
        final ArrayList<SideBarMenuButton> aa2 = aa1;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (int i=0; i<aa2.size(); i++) {
                    rental_product_list_box.addView(aa2.get(i));
                }
            }
        });



        ArrayList<SideBarMenuButton> bb1 = new ArrayList<SideBarMenuButton>();
        String[] bb = {"진행중인 이벤트", "종료된 이벤트"};
        for (int i=0; i<bb.length; i++) {
            SideBarMenuButton button = new SideBarMenuButton(context);
            button.setButtonText(bb[i]);
            button.setButtonOnClickListener(sideMenuButtonClickEvent);
            bb1.add(button);
        }
        final ArrayList<SideBarMenuButton> bb2 = bb1;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (int i=0; i<bb2.size(); i++) {
                    event_list_box.addView(bb2.get(i));
                }

            }
        });



        ArrayList<SideBarMenuButton> cc1 = new ArrayList<SideBarMenuButton>();
        String[] cc = {"기부 내역", "기부 정보"};
        for (int i=0; i<cc.length; i++) {
            SideBarMenuButton button = new SideBarMenuButton(context);
            button.setButtonText(cc[i]);
            button.setButtonOnClickListener(sideMenuButtonClickEvent);
            cc1.add(button);
        }
        final ArrayList<SideBarMenuButton> cc2 = cc1;


        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (int i=0; i<cc2.size(); i++) {
                    donate_list_box.addView(cc2.get(i));
                }
            }
        });
    }

    View.OnClickListener sideMenuButtonClickEvent = new OnClickListener() {
        @Override
        public void onClick(View v) {
            String text = ((TextView) v.getTag()).getText().toString().trim();
            // Log.i("MyTags", "text = "+text);

            switch (text) {
                case "전체":
                    Log.i("MyTags", "솔루션 실행!");
                    break;
                case "장난감":

                    break;
                case "출산/육아":

                    break;
                case "교육/완구":

                    break;
                case "의류":

                    break;
                case "가구/침구":

                    break;
                case "외출":

                    break;
                case "기타":

                    break;



                case "진행중인 이벤트":
                    Log.i("MyTags", "진행중인 이벤트 실행!");
                    break;
                case "종료된 이벤트":




                    break;
                case "기부내역":

                    break;
                case "기부정보":

                    break;
            }
        }
    };

    View.OnClickListener topButtonClickEvent = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.mypage_button: // 마이페이지
                    useFul.goActivity(Mobile09000.class);
                    break;
                case R.id.order_delivery_button: // 주문/배송

                    break;
                case R.id.donate_history_button: // 기부내역

                    break;
                case R.id.customer_center_button: // 고객센터

                    break;
            }
        }
    };


    public void setMenuBarBoxWidth(int width) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        params.width = width;
        side_menu_bar_box.setLayoutParams(params);
    }



    // 프로필 정보 가져오기
    public void side_bar_profile_info_get() {
        ContentValues params = new ContentValues();
        params.put("act", "side_bar_profile_info_get");
        params.put("member", new ShareInfo(activity).getMember());
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(99);
        inLet.start();
    }



    // 프로필 정보 가져오기 후처리
    public void side_bar_profile_info_get_after(Object obj) {
        String data = obj.toString();

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");
            if (result.equals("ok")) {
                String name = jsonObject.getString("name");
                name_textview.setText(name);
            } else {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
