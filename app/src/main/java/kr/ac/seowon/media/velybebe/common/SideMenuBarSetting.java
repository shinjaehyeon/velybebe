package kr.ac.seowon.media.velybebe.common;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;

import com.daasuu.ei.Ease;
import com.daasuu.ei.EasingInterpolator;

import java.util.logging.Handler;

import kr.ac.seowon.media.velybebe.R;

public class SideMenuBarSetting {
    AppCompatActivity activity;
    Context context;

    ConstraintLayout best_parent_layout; // 최상위 부모 레이아웃
    View button; // 이 객체를 클릭 시 사이드 메뉴바가 나올 것임


    boolean isOpened = false; // 현재 사이드 메뉴바가 열렸는지 닫혔는지
    boolean isMoving = false; // 현재 사이드 메뉴바가 움직이고 있는 중인지 아닌지



    ConstraintLayout black; // 검정 음영 부분과 터치 이벤트를 막을 레이아웃
    ConstraintLayout click; // 클릭되면 사이드 메뉴바가 닫히게 할 레이아웃
    SideMenuBar sideMenuBar; // 사이드 메뉴바

    int side_menu_bar_width; // 사이드 메뉴바 너비
    float device_width_n_percent = 0.7f; // 전체 디바이스의 몇 퍼센트

    // 생성자
    public SideMenuBarSetting(AppCompatActivity activity, View button) {
        this.activity = activity;
        this.context = activity;
        this.button = button;
        init();
    }

    // 셋팅 시작 함수
    public void init() {
        // 최상위 부모 뷰를 참조함
        best_parent_layout = activity.findViewById(R.id.best_parent_layout);


        // 검정 레이아웃 생성 및 설정, 부모 뷰에 추가
        black = new ConstraintLayout(context);
        ConstraintLayout.LayoutParams params1 = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT);
        params1.bottomToBottom = ConstraintLayout.LayoutParams.BOTTOM;
        params1.topToTop = ConstraintLayout.LayoutParams.TOP;
        params1.leftToLeft = ConstraintLayout.LayoutParams.LEFT;
        params1.rightToRight = ConstraintLayout.LayoutParams.RIGHT;
        black.setLayoutParams(params1);
        black.setBackgroundColor(Color.parseColor("#000000"));
        black.setVisibility(View.GONE);
        best_parent_layout.addView(black);


        // 클릭됬을 때 메뉴바 사라지게할 레이아웃 생성 및 설정, 부모 뷰에 추가
        click = new ConstraintLayout(context);
        ConstraintLayout.LayoutParams params2 = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT);
        params2.bottomToBottom = ConstraintLayout.LayoutParams.BOTTOM;
        params2.topToTop = ConstraintLayout.LayoutParams.TOP;
        params2.leftToLeft = ConstraintLayout.LayoutParams.LEFT;
        params2.rightToRight = ConstraintLayout.LayoutParams.RIGHT;
        click.setLayoutParams(params2);
        click.setAlpha(0);
        click.setVisibility(View.GONE);
        best_parent_layout.addView(click);


        // 만들어둔 사이드 메뉴바 객체 선언, 부모 뷰에 추가
        sideMenuBar = new SideMenuBar(activity);
        sideMenuBar.setVisibility(View.GONE);
        best_parent_layout.addView(sideMenuBar);


        // 사이드메뉴바 너비를 정하기 위한 현 디바이스의 너비 값을 구함
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        Log.i("FFFFF", "width = "+width);
        Log.i("FFFFF", "height = "+height);

        side_menu_bar_width = (int) (width * device_width_n_percent);



        // 사이드메뉴바를 너비의 device_width_n_percent 만큼만 차지하도록 레이아웃 파라미터를 셋팅함
        ConstraintLayout.LayoutParams params3 = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.MATCH_PARENT);
        params3.width = side_menu_bar_width;
        params3.bottomToBottom = ConstraintLayout.LayoutParams.BOTTOM;
        params3.topToTop = ConstraintLayout.LayoutParams.TOP;
        params3.leftToLeft = ConstraintLayout.LayoutParams.LEFT;
        params3.orientation = LinearLayout.VERTICAL;
        sideMenuBar.setLayoutParams(params3);
        sideMenuBar.setMenuBarBoxWidth(side_menu_bar_width);



        // 넘겨받은 뷰 객체에 클릭하면 사이드 메뉴바가 나오는 onClickListener 객체를 연결함
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("MyTags", "메뉴버튼 눌러짐");
                Log.i("MyTags", "isOpened = "+isOpened);

                if (!isMoving) {
                    if (!isOpened) {
                        showMenuBar();
                    }
                }
            }
        });


        // 뒤에 있는 내용이 클릭되지 않도록 black에서 터치이벤트를 종료시킴
        black.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });


        // 이 레이아웃이 클릭되면 사이드 메뉴바가 닫히도록 onClickListener를 셋팅함
        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isMoving) {
                    if (isOpened) {
                        hideMenuBar();
                    }
                }
            }
        });
    }


    // 사이드 메뉴바 보이기
    public void showMenuBar() {
        isOpened = true;
        isMoving = true;

        sideMenuBar.setVisibility(View.VISIBLE);
        black.setVisibility(View.VISIBLE);
        click.setVisibility(View.VISIBLE);


        ObjectAnimator animator2 = ObjectAnimator.ofFloat(black, "alpha", 0f, 0.7f);
        animator2.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT));
        animator2.setDuration(600);
        animator2.start();

        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                isMoving = false;
            }
        }, 600);


        ObjectAnimator animator = ObjectAnimator.ofFloat(sideMenuBar, "translationX", -side_menu_bar_width, 0);
        animator.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT));
        animator.setDuration(600);
        animator.start();

    }

    // 사이드 메뉴바 감추기
    public void hideMenuBar() {
        isOpened = false;
        isMoving = true;

        ObjectAnimator animator = ObjectAnimator.ofFloat(black, "alpha", 0.7f, 0f);
        animator.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT));
        animator.setDuration(600);
        animator.start();
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                isMoving = false;
                black.setVisibility(View.GONE);
            }
        }, 600);

        click.setVisibility(View.GONE);

        ObjectAnimator animator2 = ObjectAnimator.ofFloat(sideMenuBar, "translationX", 0, -side_menu_bar_width);
        animator2.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT));
        animator2.setDuration(600);
        animator2.start();
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                isMoving = false;
                sideMenuBar.setVisibility(View.GONE);
            }
        }, 600);
    }
}
