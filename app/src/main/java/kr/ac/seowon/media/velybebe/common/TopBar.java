package kr.ac.seowon.media.velybebe.common;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.mypage.Mobile09000;

public class TopBar extends LinearLayout {
    View v;
    AppCompatActivity activity;
    Context context;
    UseFul useFul;

    /*===================================================================================================================*/

    String title;

    ConstraintLayout body_constraintlayout;
    AppCompatImageView shadow_imageview;

    ConstraintLayout back_button;
    ConstraintLayout menu_button;
    ConstraintLayout mypage_button;

    LinearLayout logo_linearlayout;
    TextView title_textview;


    SideMenuBarSetting sideMenuBar;

    /*===================================================================================================================*/

    public TopBar(Context context) {
        super(context);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        inflateView();
    }

    public TopBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        inflateView();
    }

    public TopBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        inflateView();
    }

    /*===================================================================================================================*/

    public void inflateView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.topbar, this, false);
        addView(v);

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기
    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        useFul = new UseFul(activity);

        body_constraintlayout = (ConstraintLayout) v.findViewById(R.id.body_constraintlayout);
        shadow_imageview = (AppCompatImageView) v.findViewById(R.id.shadow_imageview);

        back_button = (ConstraintLayout) v.findViewById(R.id.back_button);
        menu_button = (ConstraintLayout) v.findViewById(R.id.menu_button);
        mypage_button = (ConstraintLayout) v.findViewById(R.id.mypage_button);

        logo_linearlayout = (LinearLayout) v.findViewById(R.id.logo_linearlayout);
        title_textview = (TextView) v.findViewById(R.id.title_textview);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        back_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // useFul.showToast("뒤로가기버튼 클릭");
                activity.finish();
            }
        });

        menu_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                useFul.showToast("메뉴버튼 클릭");
            }
        });

        mypage_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // useFul.showToast("마이페이지버튼 클릭");
                useFul.goActivity(Mobile09000.class);
            }
        });
    }

    /*===================================================================================================================*/



    /*===================================================================================================================*/

    // 그림자 이미지 바꾸기
    public void setShadowImageResoure(int res) {
        shadow_imageview.setImageResource(res);
    }

    // 메뉴버튼 눌렀을 때 이벤트 설정하기
    public void setMenuButtonClickEvent(View.OnClickListener e) {
        menu_button.setOnClickListener(e);
    }

    // 메뉴버튼 숨기기
    public void hideMenuButton() {
        menu_button.setVisibility(View.GONE);
    }

    // 마이페이지버튼 눌렀을 때 이벤트 설정하기
    public void setMypageButtonClickEvent(View.OnClickListener e) {
        mypage_button.setOnClickListener(e);
    }

    // 마이페이지버튼 숨기기
    public void hideMyPageButton() {
        mypage_button.setVisibility(View.GONE);
    }

    // 뒤로가기버튼 눌렀을 때 이벤트 설정하기
    public void setBackButtonEvent(View.OnClickListener e) {
        back_button.setOnClickListener(e);
    }

    // 뒤로가기버튼 보이기
    public void showBackButton() {
        back_button.setVisibility(View.VISIBLE);
    }


    // 로고 숨기기
    public void hideLogo() {
        logo_linearlayout.setVisibility(View.GONE);
    }

    // 타이틀 보이기
    public void showTitle() {
        title_textview.setVisibility(View.VISIBLE);
    }

    // 타이틀 설정하기
    public void setTitle(String s) {
        this.title = s;
        title_textview.setText(s);
    }

    // 타이틀 색상 설정하기
    public void setTitleColor(int res) {
        title_textview.setTextColor(res);
    }

    // 사이드 메뉴바 설정하기
    public void setMenuSideBar() {
        sideMenuBar = new SideMenuBarSetting(activity, menu_button);
    }


    // 사이드 메뉴바 닫히기
    public void hideSideMenubar() {
        sideMenuBar.hideMenuBar();
    }

    // 사이드 메뉴바 열기
    public void showSideMenubar() {
        sideMenuBar.showMenuBar();
    }
}
