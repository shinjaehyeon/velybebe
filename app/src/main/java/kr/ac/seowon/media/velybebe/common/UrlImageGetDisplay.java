package kr.ac.seowon.media.velybebe.common;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class UrlImageGetDisplay extends Thread {
    private String imageUrl;
    private AppCompatImageView imageView;
    private AppCompatActivity activity;
    private boolean isBlur;
    private ProgressBar progressBar;


    // 생성자
    public UrlImageGetDisplay(AppCompatActivity activity) {
        this.activity = activity;
    }

    // 이미지 주소 셋팅
    public void setImageUrl(String s) {
        this.imageUrl = s;
    }

    // 뿌려질 이미지뷰 셋팅
    public void setImageView(AppCompatImageView imageView) {
        this.imageView = imageView;
    }

    public void setProgressBar(ProgressBar p) {
        this.progressBar = p;
    }

    public void setBlur(boolean b) {
        this.isBlur = b;
    }

    // 핸들러
    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 90003001:
                    Bitmap bitmap = (Bitmap) msg.obj;
                    if (isBlur) {
                        bitmap = new UseFul(activity).blur3(bitmap, 25f);
                    }
                    imageView.setImageBitmap(bitmap);
                    if (progressBar != null) {
                        progressBar.setVisibility(View.GONE);
                    }

                    break;
            }
        }
    };

    @Override
    public void run() {
        super.run();

        try {
            URL url = new URL(imageUrl);

            // Web에서 이미지를 가져온 뒤
            // ImageView에 지정할 Bitmap을 만든다
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true); // 서버로 부터 응답 수신
            conn.connect();

            InputStream is = conn.getInputStream(); // InputStream 값 가져오기
            Bitmap bitmap = BitmapFactory.decodeStream(is); // Bitmap으로 변환

            Message msg = handler.obtainMessage();
            msg.obj = bitmap;
            msg.what = 90003001;

            handler.sendMessage(msg);

        } catch (MalformedURLException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
