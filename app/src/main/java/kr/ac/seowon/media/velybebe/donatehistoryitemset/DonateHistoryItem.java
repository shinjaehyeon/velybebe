package kr.ac.seowon.media.velybebe.donatehistoryitemset;

import kr.ac.seowon.media.velybebe.bestproductitemset.BestProductItemViewHolder;

public class DonateHistoryItem {
    public String imageUrl;
    public String companyTitle;
    public String donateHistoryString;
    public String donateDatetime;
    public DonateHistoryItemViewHolder viewHolder;

    public void setImageUrl(String s) {
        this.imageUrl = s;
    }
    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setCompanyTitle(String s) {
        this.companyTitle = s;
    }
    public String getCompanyTitle() {
        return this.companyTitle;
    }

    public void setDonateHistoryString(String s) {
        this.donateHistoryString = s;
    }
    public String getDonateHistoryString() {
        return this.donateHistoryString;
    }

    public void setDonateDatetime(String s) {
        this.donateDatetime = s;
    }
    public String getDonateDatetime() {
        return this.donateDatetime;
    }

    public void setViewHolder(DonateHistoryItemViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }
    public DonateHistoryItemViewHolder getViewHolder() {
        return this.viewHolder;
    }

}

