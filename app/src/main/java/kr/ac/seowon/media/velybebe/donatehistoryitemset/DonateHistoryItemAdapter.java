package kr.ac.seowon.media.velybebe.donatehistoryitemset;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.LoadingViewHolder;
import kr.ac.seowon.media.velybebe.common.OnLoadMoreListener;
import kr.ac.seowon.media.velybebe.common.UrlImageGetDisplay;
import kr.ac.seowon.media.velybebe.common.UseFul;

public class DonateHistoryItemAdapter extends RecyclerView.Adapter {
    private AppCompatActivity activity;

    private List<DonateHistoryItem> items;

    public boolean isLoading;
    public final int VIEW_TYPE_ITEM = 0;
    public final int VIEW_TYPE_LOADING = 1;
    public int visibleThreshold = 5;
    public int lastVisibleItem, totalItemCount;
    public LinearLayoutManager linearLayoutManager;

    public OnLoadMoreListener onLoadMoreListener;
    private View.OnClickListener itemsOnClickListener;

    public boolean isMoreInfo;
    public String getMoreInfoType = "scroll"; // or button

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    public DonateHistoryItemAdapter(RecyclerView recyclerView, List<DonateHistoryItem> items, final AppCompatActivity activity, View.OnClickListener itemsOnClickListener) {
        this.items = items;
        this.activity = activity;
        this.itemsOnClickListener = itemsOnClickListener;

        linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(activity).inflate(R.layout.donate_history_item, parent, false);
            return new DonateHistoryItemViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof DonateHistoryItemViewHolder) {
            DonateHistoryItem item = items.get(position);
            DonateHistoryItemViewHolder userViewHolder = (DonateHistoryItemViewHolder) holder;

            item.setViewHolder(userViewHolder);

            userViewHolder.view.setTag(item);
            userViewHolder.view.setOnClickListener(itemsOnClickListener);


            // 기부업체(받는업체) 이미지 뿌리기
            UrlImageGetDisplay u = new UrlImageGetDisplay(activity);
            u.setImageUrl(item.getImageUrl());
            u.setImageView(userViewHolder.company_imageview);
            u.start();


            // 기부 업체 명 표시
            userViewHolder.company_title.setText(item.getCompanyTitle());

            // 기부 내용
            userViewHolder.donate_historys.setText(item.getDonateHistoryString());

            // 기부한 날짜
            userViewHolder.donate_datetime.setText(item.getDonateDatetime());







        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    /**************************************************************/
}
