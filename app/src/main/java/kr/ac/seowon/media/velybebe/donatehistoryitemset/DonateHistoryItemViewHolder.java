package kr.ac.seowon.media.velybebe.donatehistoryitemset;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import kr.ac.seowon.media.velybebe.R;


public class DonateHistoryItemViewHolder extends RecyclerView.ViewHolder {
    View view;

    public AppCompatImageView company_imageview;
    public TextView company_title;
    public TextView donate_historys;
    public TextView donate_datetime;

    /**************************************************************/

    public DonateHistoryItemViewHolder(View view) {
        super(view);
        this.view = view;

        company_imageview = (AppCompatImageView) view.findViewById(R.id.company_imageview);
        company_title = (TextView) view.findViewById(R.id.company_title);
        donate_historys = (TextView) view.findViewById(R.id.donate_historys);
        donate_datetime = (TextView) view.findViewById(R.id.donate_datetime);


    }
}
