package kr.ac.seowon.media.velybebe.eventdetail;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.InLet;
import kr.ac.seowon.media.velybebe.common.ShareInfo;
import kr.ac.seowon.media.velybebe.common.TopBar;
import kr.ac.seowon.media.velybebe.common.UrlImageGetDisplay;
import kr.ac.seowon.media.velybebe.common.UseFul;
import kr.ac.seowon.media.velybebe.common.VelyBebeNormalDialog;
import kr.ac.seowon.media.velybebe.rentalapply.PayWayItem;
import kr.ac.seowon.media.velybebe.rentalapply.RentalPriceItem;

public class Mobile08000 extends AppCompatActivity {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;
    TopBar topBar;

    boolean isLoading = false;

    /*===================================================================================================================*/

    ConstraintLayout best_parent_layout;
    int event_pk;

    /*===================================================================================================================*/

    AppCompatImageView event_imageview;

    TextView title_textview;
    TextView period_textview;
    TextView target_textview;
    TextView content_textview;
    TextView apply_way_textview;

    Button link_button;

    /*===================================================================================================================*/


    /*===================================================================================================================*/


    /*===================================================================================================================*/

    /*===================================================================================================================*/


    /*===================================================================================================================*/

    /*===================================================================================================================*/

    /*===================================================================================================================*/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 8002: // 이벤트 상세정보 가져오기 후처리
                    mobile08002(msg.obj);
                    break;

                default:

                    break;
            }
        }
    };

    /*===================================================================================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile08000);


        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        getIntentInfo(); // 인텐트 정보 받아오기

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기


        mobile08001(); // 이벤트 상세정보 가져오기


    }

    @Override
    protected void onStart() {
        super.onStart();
        // Log.i("MyTags", "Main onStart 시작");
        new UseFul(activity).setAppRunning(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Log.i("MyTags", "Main onPause 시작");
        new UseFul(activity).setAppRunning(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Log.i("MyTags", "Main onResume 시작");
        new UseFul(activity).setAppRunning(1);
    }







    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(AppCompatActivity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // 인테트 정보 받아오기
    public void getIntentInfo() {
        event_pk = getIntent().getIntExtra("pk", 0);
    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        useFul = new UseFul(activity);
        best_parent_layout = (ConstraintLayout) findViewById(R.id.best_parent_layout);
        topBar = (TopBar) findViewById(R.id.topbar);

        /*===================================================================================================================*/

        event_imageview = (AppCompatImageView) findViewById(R.id.event_imageview);

        title_textview = (TextView) findViewById(R.id.title_textview);
        period_textview = (TextView) findViewById(R.id.period_textview);
        target_textview = (TextView) findViewById(R.id.target_textview);
        content_textview = (TextView) findViewById(R.id.content_textview);
        apply_way_textview = (TextView) findViewById(R.id.apply_way_textview);

        link_button = (Button) findViewById(R.id.link_button);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {
        // topBar.setTitle("회원가입");
        // topBar.setMenuSideBar();
        topBar.hideMenuButton();
        topBar.showBackButton();


    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }


    /*===================================================================================================================*/

    // 이벤트 상세정보 가져오기
    public void mobile08001() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile08001");
        params.put("member", new ShareInfo(this).getMember());
        params.put("event_pk", event_pk);

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(8002);
        inLet.start();
    }

    // 이벤트 상세정보 가져오기 후처리
    public void mobile08002(Object obj) {
        String data = obj.toString();
        Log.i("MyTags", "data = "+data);


        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");
            if (result.equals("ok")) {

                final String photo_url = jsonObject.getString("photo_url");
                String title = jsonObject.getString("title");
                String start_datetime = jsonObject.getString("start_datetime");
                String end_datetime = jsonObject.getString("end_datetime");
                String target = jsonObject.getString("target");
                String content = jsonObject.getString("content");
                String apply_way = jsonObject.getString("apply_way");
                int link_button_isvisible = jsonObject.getInt("link_button_isvisible");
                String link_button_text = jsonObject.getString("link_button_text");
                final String link_addr = jsonObject.getString("link_addr");
                int status = jsonObject.getInt("status");


                // 이미지 띄우기
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        UrlImageGetDisplay u = new UrlImageGetDisplay(activity);
                        u.setImageUrl(photo_url);
                        u.setImageView(event_imageview);
                        u.start();
                    }
                });

                // 이벤트 제목 뿌리기
                title_textview.setText(title);

                // 기간 뿌리기
                String start_only_date = start_datetime.split(" ")[0];
                String end_only_date = end_datetime.split(" ")[0];

                String start_day = useFul.getDateDay(start_only_date);
                String end_day = useFul.getDateDay(end_only_date);

                String fullPeriod = start_only_date+"("+start_day+") ~ "+end_only_date+"("+end_day+")";
                period_textview.setText(fullPeriod);

                // 대상고객 뿌리기
                target_textview.setText(target);

                // 혜택 뿌리기
                content_textview.setText(content);

                // 참여방법 뿌리기
                apply_way_textview.setText(apply_way);


                // 버튼 여부 확인
                if (link_button_isvisible == 30004101) {
                    link_button.setVisibility(View.VISIBLE);
                    link_button.setText(link_button_text+"");
                    link_button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Uri uri = Uri.parse(link_addr);
                            Intent it = new Intent(Intent.ACTION_VIEW, uri);
                            startActivity(it);
                        }
                    });


                } else {
                    link_button.setVisibility(View.GONE);
                }



            } else {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /*===================================================================================================================*/

    // 뒤로가기 할 시 경고 팝업창 띄우기
    public void callVelyBebeNormalDialog(String title, String content) {
        final VelyBebeNormalDialog dialog = new VelyBebeNormalDialog(this);
        dialog.show();
        dialog.startDialogAnimation();
        dialog.setDialogTitle(title);
        dialog.setDialogContent(content);
        dialog.buttonVisibleOptions(true, true);
        // dialog.setCancelButtonText("");
        // dialog.setConfirmButtonText("");
        dialog.setCancelButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setConfirmButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }


    boolean isFinish;
    // 통보 형식의 다이얼로그 호출
    public void callVelyBebeNormalDialog2(String title, String content) {
        final VelyBebeNormalDialog dialog = new VelyBebeNormalDialog(this);
        dialog.show();
        dialog.startDialogAnimation();
        dialog.setDialogTitle(title);
        dialog.setDialogContent(content);
        dialog.buttonVisibleOptions(false, true);
        // dialog.setCancelButtonText("");
        // dialog.setConfirmButtonText("");
        dialog.setCancelButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setConfirmButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (isFinish) {
                    finish();
                }
            }
        });
    }



    /*===================================================================================================================*/





    @Override
    public void onBackPressed() {
        super.onBackPressed();

        // callVelyBebeNormalDialog("안내", "정말 블리베베 앱을 종료하시겠습니까?"); // 뒤로가기 할 시 경고 팝업창 띄우기
    }
}
