package kr.ac.seowon.media.velybebe.firebase.push;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.util.TypedValue;
import android.widget.RemoteViews;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.NotificationTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.request.transition.Transition;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.net.URL;
import java.util.ArrayList;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.ShareInfo;
import kr.ac.seowon.media.velybebe.common.UseFul;
import kr.ac.seowon.media.velybebe.eventdetail.Mobile08000;
import kr.ac.seowon.media.velybebe.main.Mobile05000;
import kr.ac.seowon.media.velybebe.mypage.Mobile09000;
import kr.ac.seowon.media.velybebe.rentaldetail.Mobile06000;
import kr.ac.seowon.media.velybebe.splash.Mobile01000;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM";
    int mLastId = 0;
    ArrayList<Integer> mActiveIdList = new ArrayList<Integer>();
    NotificationManager nm;

    private NotificationChannel mChannel;
    private NotificationManager notifManager;


    String title;
    String message;
    String linkPage;
    String linkPageValue;
    String imageUrl;

    Bitmap bigPicture;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("NEW_TOKEN", s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.i("MyTags", "메세지 받음! remoteMessage = "+remoteMessage);

        String title = remoteMessage.getData().get("title");
        String message = remoteMessage.getData().get("message");
        // String push_group_pk = remoteMessage.getData().get("push_group_pk");
        String linkPage = remoteMessage.getData().get("linkPage");
        String linkPageValue = remoteMessage.getData().get("linkPageValue");
        String imageUrl = remoteMessage.getData().get("imageUrl");

        Log.i("MyTags", "푸쉬 받음!!");
        Log.i("MyTags", "title = "+title);
        Log.i("MyTags", "message = "+message);
        Log.i("MyTags", "linkPage = "+linkPage);
        Log.i("MyTags", "linkPageValue = "+linkPageValue);
        Log.i("MyTags", "imageUrl = "+imageUrl);


        this.title = title;
        this.message = message;
        this.linkPage = linkPage;
        this.linkPageValue = linkPageValue;
        this.imageUrl = imageUrl;

        if (new ShareInfo(getApplicationContext()).getMember() != 0) {
            sendPushNotification();
        }
    }

    private void createNotificationId() {
        int id = ++mLastId;
        mActiveIdList.add(id);
    }

    public void sendPushNotification() {
        createNotificationId();

        // 큰 메세지 박스 설정하기 (푸쉬 내용 더보기 가능)
        RemoteViews bigView = null;
        if (imageUrl.length() >= 10) {
            bigView = new RemoteViews(getApplicationContext().getPackageName(), R.layout.notification_layout_big_image);
        } else {
            bigView = new RemoteViews(getApplicationContext().getPackageName(), R.layout.notification_layout_big);
        }
        bigView.setTextViewText(R.id.push_title, title);
        bigView.setTextViewTextSize(R.id.push_title, TypedValue.COMPLEX_UNIT_DIP, 14f);
        bigView.setTextViewText(R.id.push_content, message);
        bigView.setTextViewTextSize(R.id.push_content, TypedValue.COMPLEX_UNIT_DIP, 14f);
        if (imageUrl.length() >= 10) {
            try {
                URL url = new URL(imageUrl);
                bigPicture = BitmapFactory.decodeStream(url.openConnection().getInputStream());

                bigView.setImageViewBitmap(R.id.push_image, bigPicture);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // 푸쉬 메세지 박스 셋팅
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder builder;
        notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // 안드로이드 버전이 8.0 이상이면 채널을 설정해줘야 한다.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();
            if (mChannel == null) {
                mChannel = new NotificationChannel("0", title, importance);
                mChannel.setLightColor(Color.rgb(221,93,94));
                mChannel.setShowBadge(true);
                mChannel.setSound(defaultSoundUri, audioAttributes);
                mChannel.setDescription(message);
                mChannel.enableVibration(true);
                notifManager.createNotificationChannel(mChannel);
            }
        }

        builder = new NotificationCompat.Builder(this, "0");


        builder.setSmallIcon(getNotificationIcon()) // required
                .setContentTitle(title)
                .setContentText(message)  // required
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setLights(Color.argb(1, 221, 93, 94), 500, 2000)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher_round))
                .setBadgeIconType(R.mipmap.ic_launcher_round)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setCustomBigContentView(bigView);


        switch (Integer.parseInt(linkPage)) {
            case 0: // 없음

                break;
            case 999991001: // 메인페이지
                if (new ShareInfo(getApplicationContext()).getAppRunning() == 1) {
                    Intent intent = new Intent(getApplicationContext(), Mobile05000.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    // intent.putExtra("cardnews_primaryKey", Integer.parseInt(pagevalue));

                    PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(resultPendingIntent);
                } else {
                    Intent intent = new Intent(getApplicationContext(), Mobile01000.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("linkPage", Integer.parseInt(linkPage));
                    intent.putExtra("linkPageValue", Integer.parseInt(linkPageValue));

                    PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(resultPendingIntent);
                }
                break;
            case 999991002: // 마이페이지
                if (new ShareInfo(getApplicationContext()).getAppRunning() == 1) {
                    Intent intent2 = new Intent(getApplicationContext(), Mobile09000.class);
                    intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    // intent.putExtra("cardnews_primaryKey", Integer.parseInt(pagevalue));

                    PendingIntent resultPendingIntent2 = PendingIntent.getActivity(this, 0, intent2, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(resultPendingIntent2);
                } else {
                    Intent intent2 = new Intent(getApplicationContext(), Mobile01000.class);
                    intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent2.putExtra("linkPage", Integer.parseInt(linkPage));
                    intent2.putExtra("linkPageValue", Integer.parseInt(linkPageValue));

                    PendingIntent resultPendingIntent2 = PendingIntent.getActivity(this, 0, intent2, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(resultPendingIntent2);
                }
                break;
            case 999991003: // 제품상세페이지
                if (new ShareInfo(getApplicationContext()).getAppRunning() == 1) {
                    Intent intent3 = new Intent(getApplicationContext(), Mobile06000.class);
                    intent3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent3.putExtra("pk", Integer.parseInt(linkPageValue));

                    PendingIntent resultPendingIntent3 = PendingIntent.getActivity(this, 0, intent3, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(resultPendingIntent3);
                } else {
                    Intent intent3 = new Intent(getApplicationContext(), Mobile01000.class);
                    intent3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent3.putExtra("linkPage", Integer.parseInt(linkPage));
                    intent3.putExtra("linkPageValue", Integer.parseInt(linkPageValue));

                    PendingIntent resultPendingIntent3 = PendingIntent.getActivity(this, 0, intent3, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(resultPendingIntent3);
                }
                break;
            case 999991004: // 이벤트상세페이지
                if (new ShareInfo(getApplicationContext()).getAppRunning() == 1) {
                    Intent intent4 = new Intent(getApplicationContext(), Mobile08000.class);
                    intent4.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent4.putExtra("pk", Integer.parseInt(linkPageValue));

                    PendingIntent resultPendingIntent4 = PendingIntent.getActivity(this, 0, intent4, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(resultPendingIntent4);
                } else {
                    Intent intent4 = new Intent(getApplicationContext(), Mobile01000.class);
                    intent4.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent4.putExtra("linkPage", Integer.parseInt(linkPage));
                    intent4.putExtra("linkPageValue", Integer.parseInt(linkPageValue));

                    PendingIntent resultPendingIntent4 = PendingIntent.getActivity(this, 0, intent4, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(resultPendingIntent4);
                }
                break;
            default:

                break;
        }





        Notification notification = builder.build();

        if (imageUrl.length() >= 10) {

        }




        notifManager.notify(0, notification);


    }




    private int getNotificationIcon() {
        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap.ic_launcher_round : R.mipmap.ic_launcher_round;
    }

}
