package kr.ac.seowon.media.velybebe.login;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.kakao.auth.ISessionCallback;
import com.kakao.auth.Session;
import com.kakao.network.ErrorResult;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.MeV2ResponseCallback;
import com.kakao.usermgmt.response.MeV2Response;
import com.kakao.util.exception.KakaoException;

import org.json.JSONException;
import org.json.JSONObject;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.InLet;
import kr.ac.seowon.media.velybebe.common.PermissionCheck;
import kr.ac.seowon.media.velybebe.common.ShareInfo;
import kr.ac.seowon.media.velybebe.common.UseFul;
import kr.ac.seowon.media.velybebe.main.Mobile05000;
import kr.ac.seowon.media.velybebe.permission.Mobile02000;
import kr.ac.seowon.media.velybebe.signup.Mobile04000;

public class Mobile03000 extends AppCompatActivity {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;


    /*===================================================================================================================*/

    ConstraintLayout best_parent_layout;


    boolean isLoading = false;

    /*===================================================================================================================*/

    EditText id_edittext;
    EditText pw_edittext;
    Button normal_login_button;

    Button sign_up_button;
    Button pw_find_button;

    AppCompatImageButton facebook_login_button;
    com.facebook.login.widget.LoginButton facebookLoginButtonReal;

    AppCompatImageButton kakao_login_button;
    AppCompatImageButton naver_login_button;

    /*===================================================================================================================*/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 03002: // 페이스북 정보로 가입시킨후 정보나 가입되어있는 정보 가져오기 후처리
                    mobile03002(msg.obj);
                    break;
                case 03004: // 카카오 정보로 가입시킨후 정보나 가입되어있는 정보 가져오기
                    mobile03004(msg.obj);
                    break;

                case 03011: // 일반 로그인 버튼 클릭시 서버에 정보 확인하기 후처리
                    mobile03011(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };

    /*===================================================================================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile03000);


        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        getIntentInfo(); // 인텐트 정보 받아오기

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기




        setFacebook(); // 페이스북 로그인 구현 부분
        setKakao(); // 카카오 로그인 구현 부분
    }

    @Override
    protected void onStart() {
        super.onStart();
        new UseFul(activity).setAppRunning(1);
        new PermissionCheck(this).isAllPermissionChecked();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new UseFul(activity).setAppRunning(1);
        new PermissionCheck(this).isAllPermissionChecked();
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Log.i("MyTags", "Main onPause 시작");
        new UseFul(activity).setAppRunning(0);
    }




    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(AppCompatActivity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // 인테트 정보 받아오기
    public void getIntentInfo() {

    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        useFul = new UseFul(activity);
        best_parent_layout = (ConstraintLayout) findViewById(R.id.best_parent_layout);


        id_edittext = (EditText) findViewById(R.id.id_edittext);
        pw_edittext = (EditText) findViewById(R.id.pw_edittext);
        normal_login_button = (Button) findViewById(R.id.normal_login_button);

        sign_up_button = (Button) findViewById(R.id.sign_up_button);
        pw_find_button = (Button) findViewById(R.id.pw_find_button);

        facebook_login_button = (AppCompatImageButton) findViewById(R.id.facebook_login_button);
        facebookLoginButtonReal = (com.facebook.login.widget.LoginButton) findViewById(R.id.facebookLoginButtonReal);

        kakao_login_button = (AppCompatImageButton) findViewById(R.id.kakao_login_button);
        kakaoLoginButton = (com.kakao.usermgmt.LoginButton) findViewById(R.id.com_kakao_login);

        naver_login_button = (AppCompatImageButton) findViewById(R.id.naver_login_button);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        normal_login_button.setOnClickListener(normalLoginChainButtonEvent);
        sign_up_button.setOnClickListener(normalLoginChainButtonEvent);
        pw_find_button.setOnClickListener(normalLoginChainButtonEvent);

        facebook_login_button.setOnClickListener(snsLoginButtonEvent);
        kakao_login_button.setOnClickListener(snsLoginButtonEvent);
        naver_login_button.setOnClickListener(snsLoginButtonEvent);

    }

    /*===================================================================================================================*/

    View.OnClickListener normalLoginChainButtonEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.normal_login_button: // (일반) 로그인 버튼 클릭 시 이벤트
                    if (!isLoading) {
                        isLoading = true;
                        mobile03010(); // 일반 로그인 버튼 클릭시 서버에 정보 확인하기
                    } else {
                        new UseFul(activity).showToast("작업이 진행중입니다.");
                    }
                    break;
                case R.id.sign_up_button: // 회원가입 버튼 클릭 시 이벤트
                    goActivity(Mobile04000.class);
                    break;
                case R.id.pw_find_button: // 비밀번호찾기 버튼 클릭 시 이벤트
                    useFul.showToast("비밀번호찾기 버튼을 클릭하셨습니다.");
                    break;
                default: break;
            }
        }
    };

    View.OnClickListener snsLoginButtonEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.facebook_login_button: // 페이스북로그인 버튼 클릭 시 이벤트
                    facebookLoginButtonReal.callOnClick();
                    break;
                case R.id.kakao_login_button: // 카카오계정로그인 버튼 클릭 시 이벤트
                    kakaoLoginButton.callOnClick();
                    break;
                case R.id.naver_login_button: // 네이버로그인 버튼 클릭 시 이벤트
                    useFul.showToast("네이버로그인 버튼을 클릭하셨습니다.");
                    break;
                default: break;
            }
        }
    };


    // 일반 로그인 버튼 클릭시 서버에 정보 확인하기
    public void mobile03010() {
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();
                String id = id_edittext.getText().toString();
                String pw = pw_edittext.getText().toString();

                ContentValues params = new ContentValues();
                params.put("act", "mobile03010");
                params.put("member", new ShareInfo(activity).getMember());
                params.put("pushkey", newToken);
                params.put("id", id);
                params.put("pw", pw);
                InLet inLet = new InLet(activity);
                inLet.setInLetAct("basic");
                inLet.setProtocolType("http");
                // inLet.setUrl("");
                inLet.setParams(params);
                inLet.setHandler(handler);
                inLet.setHandlerRequestNumber(03011);
                inLet.start();
            }
        });
    }

    // 일반 로그인 버튼 클릭시 서버에 정보 확인하기 후처리
    public void mobile03011(Object obj) {
        isLoading = false;



        String data = obj.toString();
        Log.i("MyTags", "data = "+data);


        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {

                int member = jsonObject.getInt("member");
                int status = jsonObject.getInt("status");

                if (status == 30002001) {
                    ShareInfo shareInfo = new ShareInfo(activity);
                    shareInfo.setInputIntValue("member", member);
                    shareInfo.setInputIntValue("member_signupmethod", 30001001);

                    goActivity(Mobile05000.class);
                    finish();

                } else if (status == 30002002) {
                    UseFul.callVelyBebeNormalDialog(activity, "안내", "해당 계정은 휴먼 상태입니다. 자세한 사항은 관리자에게 문의바랍니다.");
                } else if (status == 30002003) {
                    UseFul.callVelyBebeNormalDialog(activity, "안내", "해당 계정은 정지 상태입니다. 자세한 사항은 관리자에게 문의바랍니다.");
                } else if (status == 30002004) {
                    UseFul.callVelyBebeNormalDialog(activity, "안내", "해당 계정은 탈퇴되었습니다. 자세한 사항은 관리자에게 문의바랍니다.");
                }

            } else {
                UseFul.callVelyBebeNormalDialog(activity, "안내", "입력하신 정보와 일치하는 계정이 없습니다.");
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /*===================================================================================================================*/
    // 페이스북 로그인 부분 구현 시작


    private CallbackManager callbackManager;

    public void setFacebook() {
        // FacebookSdk.sdkInitialize(getApplicationContext());
        // AppEventsLogger.activateApp(this);

        callbackManager = CallbackManager.Factory.create();
        // facebookLoginButtonReal.setReadPermissions("email");
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.d("MyTags","onSucces LoginResult="+loginResult);
                String id = Profile.getCurrentProfile().getId();
                String name = Profile.getCurrentProfile().getName();

                Log.d("MyTags","id = "+id+", name = "+name);

                mobile03001(name, id); // 페이스북 정보로 가입시킨후 정보나 가입되어있는 정보 가져오기
            }

            @Override
            public void onCancel() {
                // App code
                Log.d("MyTags","onCancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.d("MyTags","onError = "+exception.toString());
            }
        });
    }

    // 페이스북 정보로 가입시킨후 정보나 가입되어있는 정보 가져오기
    public void mobile03001(final String name, final String id) {
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();

                ContentValues params = new ContentValues();
                params.put("act", "mobile03001");
                params.put("member", new ShareInfo(activity).getMember());
                params.put("name", name);
                params.put("id", id);
                params.put("pushkey", newToken);
                params.put("signup_type", 30001002);
                InLet inLet = new InLet(activity);
                inLet.setInLetAct("basic");
                inLet.setProtocolType("http");
                // inLet.setUrl("");
                inLet.setParams(params);
                inLet.setHandler(handler);
                inLet.setHandlerRequestNumber(03002);
                inLet.start();
            }
        });
    }

    // 페이스북 정보로 가입시킨후 정보나 가입되어있는 정보 가져오기 후처리
    public void mobile03002(Object obj) {
        String data = obj.toString();
        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {

                int member = jsonObject.getInt("member");
                int status = jsonObject.getInt("status");

                if (status == 30002001) {
                    ShareInfo shareInfo = new ShareInfo(activity);
                    shareInfo.setInputIntValue("member", member);
                    shareInfo.setInputIntValue("member_signupmethod", 30001002);

                    goActivity(Mobile05000.class);
                    finish();

                } else if (status == 30002002) {
                    UseFul.callVelyBebeNormalDialog(activity, "안내", "해당 계정은 휴먼 상태입니다. 자세한 사항은 관리자에게 문의바랍니다.");
                } else if (status == 30002003) {
                    UseFul.callVelyBebeNormalDialog(activity, "안내", "해당 계정은 정지 상태입니다. 자세한 사항은 관리자에게 문의바랍니다.");
                } else if (status == 30002004) {
                    UseFul.callVelyBebeNormalDialog(activity, "안내", "해당 계정은 탈퇴되었습니다. 자세한 사항은 관리자에게 문의바랍니다.");
                }



            } else {
                UseFul.callVelyBebeNormalDialog(this, "경고", "회원가입 조회중 오류가 발생하였습니다.");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // 페이스북 로그인 부분 구현 종료
    /*===================================================================================================================*/





    /*===================================================================================================================*/
    // 카카오 로그인 부분 구현 시작


    SessionCallback callback;
    com.kakao.usermgmt.LoginButton kakaoLoginButton;

    public void setKakao() {
        callback = new SessionCallback();

        Session.getCurrentSession().addCallback(callback);
    }

    // 카카오 로그인 구현 부분
    private class SessionCallback implements ISessionCallback {
        @Override
        public void onSessionOpened() {
            Log.i("MyTags", "kakao 로그인 onSessionOpened 실행됨..!");
            UserManagement.getInstance().me(new MeV2ResponseCallback() {
                @Override
                public void onFailure(ErrorResult errorResult) {
                    super.onFailure(errorResult);
                    Log.i("MyTags", "kakao 로그인 onFailure 실행됨..!");
                }

                @Override
                public void onFailureForUiThread(ErrorResult errorResult) {
                    super.onFailureForUiThread(errorResult);
                    Log.i("MyTags", "kakao 로그인 onFailureForUiThread 실행됨..!");
                }

                @Override
                public void onDidStart() {
                    super.onDidStart();
                    Log.i("MyTags", "kakao 로그인 onDidStart 실행됨..!");
                }

                @Override
                public void onDidEnd() {
                    super.onDidEnd();
                    Log.i("MyTags", "kakao 로그인 onDidEnd 실행됨..!");
                }

                @Override
                public void onSuccessForUiThread(MeV2Response result) {
                    super.onSuccessForUiThread(result);
                    Log.i("MyTags", "onSuccessForUiThread 로그인 onDidEnd 실행됨..!");
                    Log.i("MyTags", "result.getId() = "+result.getId());
                    Log.i("MyTags", "result.getNickname() = "+result.getNickname());

                    String kakaoUserPk = result.getId()+"";
                    String kakaoUserName = result.getNickname()+"";

                    mobile03003(kakaoUserName, kakaoUserPk); // 카카오 정보로 가입시킨후 정보나 가입되어있는 정보 가져오기
                }

                @Override
                public void onSuccess(MeV2Response result) {
                    Log.i("MyTags", "kakao 로그인 onSuccess(MeV2Response result) 실행됨..!");

                }

                @Override
                public void onSessionClosed(ErrorResult errorResult) {
                    Log.i("MyTags", "onSessionClosed 로그인 onDidEnd 실행됨..!");
                }
            });

        }

        @Override
        public void onSessionOpenFailed(KakaoException exception) {
            Log.i("MyTags", "kakao 로그인 onSessionOpenFailed 실행됨..!");
            Log.i("MyTags", exception.toString());
        }
    }

    // 카카오 정보로 가입시킨후 정보나 가입되어있는 정보 가져오기
    public void mobile03003(final String name, final String id) {
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();

                ContentValues params = new ContentValues();
                params.put("act", "mobile03003");
                params.put("member", new ShareInfo(activity).getMember());
                params.put("name", name);
                params.put("id", id);
                params.put("pushkey", newToken);
                params.put("signup_type", 30001003);
                InLet inLet = new InLet(activity);
                inLet.setInLetAct("basic");
                inLet.setProtocolType("http");
                // inLet.setUrl("");
                inLet.setParams(params);
                inLet.setHandler(handler);
                inLet.setHandlerRequestNumber(03004);
                inLet.start();
            }
        });
    }

    // 카카오 정보로 가입시킨후 정보나 가입되어있는 정보 가져오기 후처리
    public void mobile03004(Object obj) {
        String data = obj.toString();
        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {

                int member = jsonObject.getInt("member");
                int status = jsonObject.getInt("status");

                if (status == 30002001) {
                    ShareInfo shareInfo = new ShareInfo(activity);
                    shareInfo.setInputIntValue("member", member);
                    shareInfo.setInputIntValue("member_signupmethod", 30001003);

                    goActivity(Mobile05000.class);
                    finish();

                } else if (status == 30002002) {
                    UseFul.callVelyBebeNormalDialog(activity, "안내", "해당 계정은 휴먼 상태입니다. 자세한 사항은 관리자에게 문의바랍니다.");
                } else if (status == 30002003) {
                    UseFul.callVelyBebeNormalDialog(activity, "안내", "해당 계정은 정지 상태입니다. 자세한 사항은 관리자에게 문의바랍니다.");
                } else if (status == 30002004) {
                    UseFul.callVelyBebeNormalDialog(activity, "안내", "해당 계정은 탈퇴되었습니다. 자세한 사항은 관리자에게 문의바랍니다.");
                }



            } else {
                UseFul.callVelyBebeNormalDialog(this, "경고", "회원가입 조회중 오류가 발생하였습니다.");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    // 카카오 로그인 부분 구현 종료
    /*===================================================================================================================*/



    public void goActivity(Class c) {
        Intent intent = new Intent(activity, c);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    /*===================================================================================================================*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }




    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
