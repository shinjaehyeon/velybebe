package kr.ac.seowon.media.velybebe.main;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.UseFul;

public class DonateItem extends LinearLayout {
    View v;
    AppCompatActivity activity;
    Context context;
    UseFul useFul;


    ConstraintLayout body;
    TextView title;
    TextView content;
    Button donate_button;
    Button status_view_button;


    int pk;

    public void setPk(int pk) {
        this.pk = pk;
    }
    public int getPk() {
        return this.pk;
    }


    public void setDonateTitle(String s) {
        title.setText(s);
    }
    public void setDonateContent(String s) {
        content.setText(s);
    }

    public void setStatus(int n) {
        if (n == 30006001) {
            // 기부 받는 상태 이면
            title.setTextColor(useFul.getRcolor(R.color.donate_item_active_title_color));
            content.setTextColor(useFul.getRcolor(R.color.donate_item_active_content_color));
            status_view_button.setTextColor(useFul.getRcolor(R.color.donate_item_active_status_color));

            body.setBackgroundResource(R.drawable.background_border_1);
            donate_button.setVisibility(View.VISIBLE);
        } else {
            // 기부 받는 상태가 아니면
            title.setTextColor(useFul.getRcolor(R.color.donate_item_inactive_title_color));
            content.setTextColor(useFul.getRcolor(R.color.donate_item_inactive_content_color));
            status_view_button.setTextColor(useFul.getRcolor(R.color.donate_item_inactive_status_color));

            body.setBackgroundResource(R.drawable.background_border_2);
            donate_button.setVisibility(View.GONE);
        }
    }



    public DonateItem(Context context) {
        super(context);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }
    public DonateItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }
    public DonateItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }

    public void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.donate_item, this, false);
        addView(v);

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기


    }




    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        body = (ConstraintLayout) v.findViewById(R.id.body);
        title = (TextView) v.findViewById(R.id.title);
        content = (TextView) v.findViewById(R.id.content);
        donate_button = (Button) v.findViewById(R.id.donate_button);
        status_view_button = (Button) v.findViewById(R.id.status_view_button);
    }


    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }
}
