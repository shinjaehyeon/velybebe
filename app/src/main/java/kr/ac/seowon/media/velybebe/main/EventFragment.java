
package kr.ac.seowon.media.velybebe.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

// import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import java.util.Timer;
import java.util.TimerTask;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.UrlImageGetDisplay;
import kr.ac.seowon.media.velybebe.common.UseFul;

@SuppressLint("ValidFragment")
public class EventFragment extends Fragment {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;
    View rootView;

    /*===================================================================================================================*/

    ViewPager event_slide_viewpager;


    int event_pk;
    String imageUrl;

    AppCompatImageView event_image;

    int currentEventIndex;


    public EventFragment(AppCompatActivity activity) {
        this.activity = activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.event_fragment, parentViewGroup, false);

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기


        viewEventImage(); // 이벤트 이미지 뿌리기
        setImageClickEvent(); // 이미지 클릭시 해당 이벤트 상세페이지로 이동하게 셋팅하기

        return rootView;
    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        useFul = new UseFul(activity);

        /*===================================================================================================================*/

        event_image = (AppCompatImageView) rootView.findViewById(R.id.event_image);
    }


    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {


    }

    /*===================================================================================================================*/

    // 이벤트 이미지 뿌리기
    public void viewEventImage() {
        UrlImageGetDisplay u = new UrlImageGetDisplay(activity);
        u.setImageUrl(imageUrl);
        u.setImageView(event_image);
        u.start();
    }

    // 이미지 클릭시 해당 이벤트 상세페이지로 이동하게 셋팅하기
    public void setImageClickEvent() {
        event_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("MyTags", "선택한 이벤트의 pk는 "+event_pk+" 입니다.");
            }
        });
    }



    // 메인 이벤트 슬라이드 뷰페이저 셋팅
    public void setEventSlideViewPager(ViewPager viewPager) {
        this.event_slide_viewpager = viewPager;
    }




    // 이미지 url 셋팅하기
    public void setImageUrl(String s) {
        this.imageUrl = s;
    }

    // 이벤트 pk 셋팅하기
    public void setEventPk(int pk) {
        this.event_pk = pk;
    }




    public void setCurrentEventIndex(int currentEventIndex) {
        this.currentEventIndex = currentEventIndex;
    }
}
