package kr.ac.seowon.media.velybebe.main;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.UrlImageGetDisplay;
import kr.ac.seowon.media.velybebe.common.UseFul;

public class EventItem extends LinearLayout {
    View v;
    AppCompatActivity activity;
    Context context;
    UseFul useFul;

    /**************************************************************/

    int event_pk;
    AppCompatImageView event_imageview;
    TextView paly_end_textview;
    TextView datetime_textview;
    TextView event_title_textview;
    TextView view_index_textview;

    /**************************************************************/

    public void setPk(int n) {
        this.event_pk = n;
    }
    public int getPk() {
        return this.event_pk;
    }

    public void setStatus(int n) {
        if (n == 30004001) {
            paly_end_textview.setText("진행중 이벤트");
            paly_end_textview.setTextColor(useFul.getRcolor(R.color.event_item_text_play_color));
        } else {
            paly_end_textview.setText("종료된 이벤트");
            paly_end_textview.setTextColor(useFul.getRcolor(R.color.event_item_text_end_color));
        }
    }

    public void setEventPeriod(String s) {
        datetime_textview.setText(s);
    }

    public void setViewIndex(int n) {
        view_index_textview.setText(n+"");
    }

    public void setOnItemClickListener(View.OnClickListener e, EventItem item) {
        setTag(item);
        setOnClickListener(e);
    }

    public void setEventTitle(String s) {
        event_title_textview.setText(s);
    }

    public void setEventImageUrl(String s) {
        UrlImageGetDisplay u = new UrlImageGetDisplay(activity);
        u.setImageUrl(s);
        u.setImageView(event_imageview);
        u.start();
    }


    /**************************************************************/

    public EventItem(Context context) {
        super(context);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }
    public EventItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }
    public EventItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }

    public void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.event_item, this, false);
        addView(v);

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기


    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        event_imageview = (AppCompatImageView) v.findViewById(R.id.event_imageview);
        paly_end_textview = (TextView) v.findViewById(R.id.paly_end_textview);
        datetime_textview = (TextView) v.findViewById(R.id.datetime_textview);
        event_title_textview = (TextView) v.findViewById(R.id.event_title_textview);
        view_index_textview = (TextView) v.findViewById(R.id.view_index_textview);
    }


    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }
}
