package kr.ac.seowon.media.velybebe.main;

import android.animation.ObjectAnimator;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.daasuu.ei.Ease;
import com.daasuu.ei.EasingInterpolator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.InLet;
import kr.ac.seowon.media.velybebe.common.ShareInfo;
import kr.ac.seowon.media.velybebe.common.TopBar;
import kr.ac.seowon.media.velybebe.common.UseFul;
import kr.ac.seowon.media.velybebe.common.VelyBebeNormalDialog;
import kr.ac.seowon.media.velybebe.login.Mobile03000;
import kr.ac.seowon.media.velybebe.signup.Mobile04100;
import kr.ac.seowon.media.velybebe.signup.Mobile04200;
import kr.ac.seowon.media.velybebe.signup.Mobile04300;

public class Mobile05000 extends AppCompatActivity {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;
    TopBar topBar;

    /*===================================================================================================================*/

    ConstraintLayout best_parent_layout;

    /*===================================================================================================================*/


    boolean isLoading = false;


    ArrayList<Button> tab_buttons = new ArrayList<Button>();

    int line_width;
    LinearLayout position_line_box;
    LinearLayout position_line;

    int currentViewIndex;
    Fragment home;
    Fragment rental;
    Fragment event;
    Fragment donate;


    ViewPager main_viewpager;

    /*==================================================================================================================*/



    /*===================================================================================================================*/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 00000:

                    break;
                default:

                    break;
            }
        }
    };

    /*===================================================================================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile05000);


        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        getIntentInfo(); // 인텐트 정보 받아오기

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Log.i("MyTags", "Main onStart 시작");
        new UseFul(activity).setAppRunning(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Log.i("MyTags", "Main onPause 시작");
        new UseFul(activity).setAppRunning(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Log.i("MyTags", "Main onResume 시작");
        new UseFul(activity).setAppRunning(1);
    }





    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(AppCompatActivity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // 인테트 정보 받아오기
    public void getIntentInfo() {

    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        useFul = new UseFul(activity);
        best_parent_layout = (ConstraintLayout) findViewById(R.id.best_parent_layout);
        topBar = (TopBar) findViewById(R.id.topbar);

        /*===================================================================================================================*/

        tab_buttons.add((Button) findViewById(R.id.tab1_button));
        tab_buttons.add((Button) findViewById(R.id.tab2_button));
        tab_buttons.add((Button) findViewById(R.id.tab3_button));
        tab_buttons.add((Button) findViewById(R.id.tab4_button));

        position_line_box = (LinearLayout) findViewById(R.id.position_line_box);
        position_line = (LinearLayout) findViewById(R.id.position_line);


        main_viewpager = (ViewPager) findViewById(R.id.main_viewpager);
        home = new Mobile05100(activity); // 홈
        rental = new Mobile05200(activity); // 렌탈
        event = new Mobile05300(activity); // 이벤트
        donate = new Mobile05400(activity); // 기부
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {
        // topBar.setTitle("회원가입");
        topBar.setMenuSideBar();

        main_viewpager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        main_viewpager.setCurrentItem(0);
        main_viewpager.setOffscreenPageLimit(4);


        // 탭 위치표시 선의 너비 설정 (디바이스 너비의 1/4)
        line_width = useFul.getDeviceWidth() / 4;
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) position_line.getLayoutParams();
        params.width = line_width;
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        for (int i=0; i<tab_buttons.size(); i++) {
            tab_buttons.get(i).setOnClickListener(tabButtonEvent);
        }


    }

    /*===================================================================================================================*/

    View.OnClickListener tabButtonEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int index = Integer.parseInt(v.getTag().toString());

            activeEffect(index);
            main_viewpager.setCurrentItem(index, true);
        }
    };


    public void activeEffect(int n) {
        tab_buttons.get(currentViewIndex).setTextColor(useFul.getRcolor(R.color.mobile05000_tab_button_text_normal_color));
        tab_buttons.get(n).setTextColor(useFul.getRcolor(R.color.mobile05000_tab_button_text_active_color));

        float toX = position_line.getTranslationX();
        float fromX = line_width * n;

        ObjectAnimator animator = ObjectAnimator.ofFloat(position_line, "translationX", toX, fromX);
        animator.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT)); // github에서 라이브러리 가져와야 함.
        animator.setDuration(300);
        animator.start();

        currentViewIndex = n;
    }


    private class ViewPagerAdapter extends FragmentPagerAdapter {
        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            activeEffect(position);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            // super.destroyItem(container, position, object);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    return home;
                case 1:
                    return rental;
                case 2:
                    return event;
                case 3:
                    return donate;
                default:
                    return null;
            }
        }
        @Override
        public int getCount() {
            return 4;
        }
    }






    /*===================================================================================================================*/

    // 뒤로가기 할 시 경고 팝업창 띄우기
    public void callVelyBebeNormalDialog(String title, String content) {
        final VelyBebeNormalDialog dialog = new VelyBebeNormalDialog(this);
        dialog.show();
        dialog.startDialogAnimation();
        dialog.setDialogTitle(title);
        dialog.setDialogContent(content);
        dialog.buttonVisibleOptions(true, true);
        // dialog.setCancelButtonText("");
        // dialog.setConfirmButtonText("");
        dialog.setCancelButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setConfirmButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    // 통보 형식의 다이얼로그 호출
    public void callVelyBebeNormalDialog2(String title, String content) {
        final VelyBebeNormalDialog dialog = new VelyBebeNormalDialog(this);
        dialog.show();
        dialog.startDialogAnimation();
        dialog.setDialogTitle(title);
        dialog.setDialogContent(content);
        dialog.buttonVisibleOptions(false, true);
        // dialog.setCancelButtonText("");
        // dialog.setConfirmButtonText("");
        dialog.setCancelButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setConfirmButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }


    public void goActivity(Class c) {
        Intent intent = new Intent(activity, c);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    /*===================================================================================================================*/

    @Override
    public void onBackPressed() {
        // super.onBackPressed();

        callVelyBebeNormalDialog("안내", "정말 블리베베 앱을 종료하시겠습니까?"); // 뒤로가기 할 시 경고 팝업창 띄우기
    }
}
