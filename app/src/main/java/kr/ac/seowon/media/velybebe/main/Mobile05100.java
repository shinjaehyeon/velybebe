package kr.ac.seowon.media.velybebe.main;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

// import com.bumptech.glide.Glide;
import com.daasuu.ei.Ease;
import com.daasuu.ei.EasingInterpolator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

// import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.bestproductitemset.BestProductItem;
import kr.ac.seowon.media.velybebe.bestproductitemset.BestProductItemAdapter;
import kr.ac.seowon.media.velybebe.common.CheckBoxA;
import kr.ac.seowon.media.velybebe.common.InLet;
import kr.ac.seowon.media.velybebe.common.OnLoadMoreListener;
import kr.ac.seowon.media.velybebe.common.ShareInfo;
import kr.ac.seowon.media.velybebe.common.UrlImageGetDisplay;
import kr.ac.seowon.media.velybebe.common.UseFul;
import kr.ac.seowon.media.velybebe.donatehistoryitemset.DonateHistoryItem;
import kr.ac.seowon.media.velybebe.donatehistoryitemset.DonateHistoryItemAdapter;
import kr.ac.seowon.media.velybebe.popularproductitemset.PopularProductItem;
import kr.ac.seowon.media.velybebe.rentaldetail.Mobile06000;

@SuppressLint("ValidFragment")
// 회원가입 STEP 01
public class Mobile05100 extends Fragment {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;

    /*===================================================================================================================*/

    View rootView;


    int currentEventIndex;
    ViewPager event_slide_viewpager;
    ArrayList<EventFragment> eventFragments = new ArrayList<EventFragment>();
    ArrayList<LinearLayout> tabButtons = new ArrayList<LinearLayout>();
    int slide_num;
    LinearLayout slide_symbol_box;




    ArrayList<AppCompatImageView> special_price_product_images = new ArrayList<AppCompatImageView>();
    ArrayList<TextView> special_price_product_titles = new ArrayList<TextView>();
    ArrayList<TextView> special_price_product_price_infos = new ArrayList<TextView>();



    // 베스트 상품
    RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<BestProductItem> items = new ArrayList<BestProductItem>();
    BestProductItemAdapter itemAdapter;




    // 인기상품
    LinearLayout popular_item_box;





    // 기부내역
    RecyclerView recyclerView2;
    private RecyclerView.LayoutManager mLayoutManager2;
    ArrayList<DonateHistoryItem> items2 = new ArrayList<DonateHistoryItem>();
    DonateHistoryItemAdapter itemAdapter2;



    /*===================================================================================================================*/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 9000:
                    popular_item_box.addView((PopularProductItem) msg.obj);
                    break;

                case 9999:
                    event_slide_viewpager.setCurrentItem(msg.arg1, true);
                    break;

                case 05102: // 진행중인 이벤트 최대 8개 까지 가져오기 후처리
                    mobile05102(msg.obj);
                    break;

                case 05104: // 특가제품 가져오기 후처리
                    mobile05104(msg.obj);
                    break;

                case 05106: // 인기제품 가져오기 후처리
                    mobile05106(msg.obj);
                    break;

                case 05112: // 베스트제품 가져오기 후처리
                    mobile05112(msg.obj);
                    break;

                case 05122: // 최근 기부내역 10개(업체기준)까지 가져오기 후처리
                    mobile05122(msg.obj);
                    break;

            }
        }
    };

    /*===================================================================================================================*/

    public Mobile05100() {

    }

    public Mobile05100(AppCompatActivity activity) {
        this.activity = activity;
    }

    /*===================================================================================================================*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.mobile05100, parentViewGroup, false);

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기


        mobile05101(); // 진행중인 이벤트 최대 8개 까지 가져오기

        mobile05103(); // 특가제품 가져오기


        mobile05105(); // 인기제품 가져오기

        setBestProductRecyclerView(); // 베스트상품 리사이클러뷰 셋팅하기
        mobile05111(); // 베스트제품 가져오기


        setDonateHistoryRecyclerView(); // 기부내역 리사이클러뷰 셋팅하기
        mobile05121(); // 최근 기부내역 10개(업체기준)까지 가져오기


        return rootView;
    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        useFul = new UseFul(activity);

        /*===================================================================================================================*/

        event_slide_viewpager = (ViewPager) rootView.findViewById(R.id.event_slide_viewpager);
        slide_symbol_box  = (LinearLayout) rootView.findViewById(R.id.slide_symbol_box);


        // 특가 상품 이미지
        special_price_product_images.add((AppCompatImageView) rootView.findViewById(R.id.special_price_product_1_image));
        special_price_product_images.add((AppCompatImageView) rootView.findViewById(R.id.special_price_product_2_image));
        special_price_product_images.add((AppCompatImageView) rootView.findViewById(R.id.special_price_product_3_image));

        // 특가 상품 명
        special_price_product_titles.add((TextView) rootView.findViewById(R.id.special_price_product_1_title));
        special_price_product_titles.add((TextView) rootView.findViewById(R.id.special_price_product_2_title));
        special_price_product_titles.add((TextView) rootView.findViewById(R.id.special_price_product_3_title));

        // 특가 상품 가격정보
        special_price_product_price_infos.add((TextView) rootView.findViewById(R.id.special_price_product_1_price_info));
        special_price_product_price_infos.add((TextView) rootView.findViewById(R.id.special_price_product_2_price_info));
        special_price_product_price_infos.add((TextView) rootView.findViewById(R.id.special_price_product_3_price_info));


        popular_item_box = (LinearLayout) rootView.findViewById(R.id.popular_item_box);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.best_product_recyclerview);
        recyclerView2 = (RecyclerView) rootView.findViewById(R.id.donate_history_recyclerview);
    }


    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
         event_slide_viewpager.setOnTouchListener(new View.OnTouchListener() {
             @Override
             public boolean onTouch(View v, MotionEvent event) {
                 switch (event.getAction()) {
                     case MotionEvent.ACTION_DOWN:
                         Log.i("MyTags", "다운실행");
                         eventSlideStop();
                         break;
                     case MotionEvent.ACTION_UP:
                         Log.i("MyTags", "업실행");
                         eventSlideStart();
                         break;
                     case MotionEvent.ACTION_MOVE:
                         Log.i("MyTags", "이동실행");
                         eventSlideStop();
                         break;
                 }
                 return false;
             }
         });

    }


    public void setBestProductRecyclerView() {
        recyclerView.setNestedScrollingEnabled(false);

        mLayoutManager = new GridLayoutManager(activity,2);
        mLayoutManager.setAutoMeasureEnabled(true);

        recyclerView.setLayoutManager(mLayoutManager);

        itemAdapter = new BestProductItemAdapter(recyclerView, items, activity, itemOnClickListener);
        recyclerView.setAdapter(itemAdapter);
    }


    View.OnClickListener itemOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            BestProductItem item = (BestProductItem) v.getTag();

            int pk = item.getPk(); // 제품 고유번호

            Intent intent = new Intent(activity, Mobile06000.class);
            intent.putExtra("pk", pk);
            startActivity(intent);

        }
    };

    /*===================================================================================================================*/

    // 진행중인 이벤트 최대 8개 까지 가져오기
    public void mobile05101() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile05101");
        params.put("member", new ShareInfo(activity).getMember());
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(05102);
        inLet.start();
    }


    TimerTask eventSlideAutoSlide;
    Timer eventSlideAutoTimer;

    // 진행중인 이벤트 최대 8개 까지 가져오기 후처리
    public void mobile05102(Object obj) {
        String data = obj.toString();
        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {

                slide_num = jsonObject.getInt("slide_num");

                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    int event = jsonObject1.getInt("event");
                    String slide_photo_url = jsonObject1.getString("slide_photo_url");

                    EventFragment fragment = new EventFragment(activity);
                    fragment.setEventPk(event);
                    fragment.setImageUrl(slide_photo_url);
                    fragment.setEventSlideViewPager(event_slide_viewpager);
                    eventFragments.add(fragment);


                    LinearLayout linearLayout = new LinearLayout(activity);
                    linearLayout.setBackgroundResource(R.drawable.background_border_7);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.width = useFul.getRdimen(R.dimen.mobile05100_event_slide_symbol_width);
                    params.height = useFul.getRdimen(R.dimen.mobile05100_event_slide_symbol_height);
                    if (i != jsonArray.length() - 1) {
                        params.setMarginEnd(useFul.getRdimen(R.dimen.mobile05100_event_slide_symbol_marginEnd));
                    }
                    linearLayout.setLayoutParams(params);
                    slide_symbol_box.addView(linearLayout);
                    tabButtons.add(linearLayout);
                }

                event_slide_viewpager.setAdapter(new ViewPagerAdapter(activity.getSupportFragmentManager()));
                event_slide_viewpager.setCurrentItem(0);
                event_slide_viewpager.setOffscreenPageLimit(slide_num);


                eventSlideStart();





            } else {

            }



        } catch (JSONException e) {
            e.printStackTrace();
        }

    }




    // 특가제품 가져오기
    public void mobile05103() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile05103");
        params.put("member", new ShareInfo(activity).getMember());
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(05104);
        inLet.start();
    }

    // 특가제품 가져오기 후처리
    public void mobile05104(Object obj) {
        String data = obj.toString();
        Log.i("MyTags", "mobile05104 data = "+data);

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {

                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    int vbp = jsonObject1.getInt("vbp");
                    String normal_image_url = jsonObject1.getString("normal_image_url");
                    String title = jsonObject1.getString("title");
                    String period_string = jsonObject1.getString("period_string");
                    int price = jsonObject1.getInt("price");


                    UrlImageGetDisplay u = new UrlImageGetDisplay(activity);
                    u.setImageUrl(normal_image_url);
                    u.setImageView(special_price_product_images.get(i));
                    u.start();

                    special_price_product_images.get(i).setTag(vbp);
                    special_price_product_titles.get(i).setText(useFul.cutString(title, 8));
                    special_price_product_images.get(i).setOnClickListener(special_price_image_touch_event);
                    special_price_product_price_infos.get(i).setText(period_string+" - "+useFul.toNumFormat(price));
                }

                switch (jsonArray.length()) {
                    case 1:
                        special_price_product_images.get(1).setVisibility(View.GONE);
                        special_price_product_images.get(2).setVisibility(View.GONE);
                        break;
                    case 2:
                        special_price_product_images.get(2).setVisibility(View.GONE);
                        break;
                    case 3:

                        break;
                }

            } else {

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    View.OnClickListener special_price_image_touch_event = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int pk = Integer.parseInt(v.getTag().toString());

            Intent intent = new Intent(activity, Mobile06000.class);
            intent.putExtra("pk", pk);
            startActivity(intent);
        }
    };






    // 인기제품 가져오기
    public void mobile05105() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile05105");
        params.put("member", new ShareInfo(activity).getMember());
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(05106);
        inLet.start();
    }

    // 인기제품 가져오기 후처리
    public void mobile05106(Object obj) {
        String data = obj.toString();
        Log.i("MyTags", "data = "+data);


        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {

                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    int vbp = jsonObject1.getInt("vbp");
                    String normal_image_url = jsonObject1.getString("normal_image_url");
                    String title = jsonObject1.getString("title");
                    String period_string = jsonObject1.getString("period_string");
                    int price = jsonObject1.getInt("price");


                    final PopularProductItem item = new PopularProductItem(activity);
                    item.setPk(vbp);
                    item.setImageUrl(normal_image_url);
                    item.setTitle(useFul.cutString(title, 8));
                    item.setPrice(period_string+" - "+useFul.toNumFormat(price));
                    item.setItemOnClickListener(popularItemClickEvent, item);

                    popular_item_box.post(new Runnable() {
                        @Override
                        public void run() {
                            Message msg = handler.obtainMessage();
                            msg.what = 9000;
                            msg.obj = item;
                            handler.sendMessage(msg);
                        }
                    });
                }

            } else {

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    View.OnClickListener popularItemClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int pk = ((PopularProductItem) v.getTag()).getPk();

            Intent intent = new Intent(activity, Mobile06000.class);
            intent.putExtra("pk", pk);
            startActivity(intent);
        }
    };





    // 베스트제품 가져오기
    public void mobile05111() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile05111");
        params.put("member", new ShareInfo(activity).getMember());
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(05112);
        inLet.start();
    }

    // 베스트제품 가져오기 후처리
    public void mobile05112(Object obj) {
        String data = obj.toString();
        Log.i("MyTags", "data = "+data);

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");


            if (result.equals("ok")) {

                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    int vbp = jsonObject1.getInt("vbp");
                    String normal_image_url = jsonObject1.getString("normal_image_url");
                    String title = jsonObject1.getString("title");
                    String period_string = jsonObject1.getString("period_string");
                    int price = jsonObject1.getInt("price");
                    int coupon = jsonObject1.getInt("coupon");
                    int sale = jsonObject1.getInt("sale");
                    int gift = jsonObject1.getInt("gift");

                    BestProductItem item = new BestProductItem();
                    item.setPk(vbp);
                    item.setProductImageUrl(normal_image_url);
                    item.setProductTitle(useFul.cutString(title, 8));
                    item.setProductPrice(period_string+" - "+useFul.toNumFormat(price));
                    if (coupon != 90001001) {
                        item.setCoupon(true);
                    } else {
                        item.setCoupon(false);
                    }
                    if (sale != 90001001) {
                        item.setSale(true);
                    } else {
                        item.setSale(false);
                    }
                    if (gift != 90001001) {
                        item.setGift(true);
                    } else {
                        item.setGift(false);
                    }

                    items.add(item);
                }
                itemAdapter.setLoaded();
                itemAdapter.notifyItemChanged(0);


            } else {

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }



    // 기부내역 리사이클러뷰 셋팅하기
    public void setDonateHistoryRecyclerView() {
        recyclerView2.setNestedScrollingEnabled(false);

        mLayoutManager2 = new GridLayoutManager(activity,1);
        mLayoutManager2.setAutoMeasureEnabled(true);

        recyclerView2.setLayoutManager(mLayoutManager2);

        itemAdapter2 = new DonateHistoryItemAdapter(recyclerView2, items2, activity, null);
        recyclerView2.setAdapter(itemAdapter2);
    }


    // 최근 기부내역 10개(업체기준)까지 가져오기
    public void mobile05121() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile05121");
        params.put("member", new ShareInfo(activity).getMember());
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(05122);
        inLet.start();
    }

    // 최근 기부내역 10개(업체기준)까지 가져오기 후처리
    public void mobile05122(Object obj) {
        String data = obj.toString();
        Log.i("MyTags", "mobile05122 data = "+data);

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {

                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    int vbdl = jsonObject1.getInt("vbdl");
                    int company_pk = jsonObject1.getInt("company_pk");
                    int member = jsonObject1.getInt("member");
                    String member_name = jsonObject1.getString("member_name");
                    String company_title = jsonObject1.getString("company_title");
                    String company_photo_url = jsonObject1.getString("company_photo_url");
                    String datetime = jsonObject1.getString("datetime");
                    String onlyDate = datetime.split(" ")[0];
                    String dayString = useFul.getDateDay(onlyDate);


                    DonateHistoryItem item = new DonateHistoryItem();
                    item.setImageUrl(company_photo_url);
                    item.setCompanyTitle("<"+company_title+">");
                    item.setDonateHistoryString(member_name+"님이 기부하셨습니다.");
                    item.setDonateDatetime(onlyDate+" "+dayString);

                    items2.add(item);
                }
                itemAdapter2.setLoaded();
                itemAdapter2.notifyItemChanged(0);


            } else {

            }



        } catch (JSONException e) {
            e.printStackTrace();
        }
    }





    public void eventSlideStart() {
        Log.i("MyTags", "eventSlideStart 시작");
        eventSlideAutoSlide = new TimerTask() {
            @Override
            public void run() {
                int nextIndex = currentEventIndex + 1;
                if (nextIndex >= slide_num) {
                    nextIndex = 0;
                }

                // Log.i("MyTags", "slide_num = "+slide_num);
                // Log.i("MyTags", "nextIndex = "+nextIndex);

                Message msg = handler.obtainMessage();
                msg.what = 9999;
                msg.arg1 = nextIndex;
                handler.sendMessage(msg);

            }
        };

        if (eventSlideAutoTimer != null) {
            this.eventSlideAutoTimer.cancel();
        }
        Timer eventSlideAutoTimer = new Timer();
        this.eventSlideAutoTimer = eventSlideAutoTimer;
        eventSlideAutoTimer.schedule(eventSlideAutoSlide, 3000, 3000);
    }

    public void eventSlideStop() {
        Log.i("MyTags", "eventSlideStop 시작");

        eventSlideAutoTimer.cancel();
        eventSlideAutoTimer.purge();
    }



    public void activeEffect(int n) {
        tabButtons.get(currentEventIndex).setBackgroundResource(R.drawable.background_border_7);
        tabButtons.get(n).setBackgroundResource(R.drawable.background_border_8);



        currentEventIndex = n;
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {
        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            activeEffect(position);
            for (int i=0; i<eventFragments.size(); i++) {
                eventFragments.get(i).setCurrentEventIndex(position);
            }
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            // super.destroyItem(container, position, object);
        }

        @Override
        public Fragment getItem(int position) {
            return eventFragments.get(position);
        }
        @Override
        public int getCount() {
            return slide_num;
        }
    }



}
