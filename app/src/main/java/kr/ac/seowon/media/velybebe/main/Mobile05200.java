package kr.ac.seowon.media.velybebe.main;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableRow;
import android.widget.TextView;

import com.daasuu.ei.Ease;
import com.daasuu.ei.EasingInterpolator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.logging.ConsoleHandler;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.CheckBoxA;
import kr.ac.seowon.media.velybebe.common.InLet;
import kr.ac.seowon.media.velybebe.common.ShareInfo;
import kr.ac.seowon.media.velybebe.common.UseFul;
import kr.ac.seowon.media.velybebe.rentaldetail.Mobile06000;
import kr.ac.seowon.media.velybebe.rentalitemset.RentalItem;

@SuppressLint("ValidFragment")
// 회원가입 STEP 01
public class Mobile05200 extends Fragment {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;

    boolean isLoading = false;

    /*===================================================================================================================*/

    View rootView;



    int index = 0;
    int view_num = 10;

    int bigCategory = 0;
    int smallCategory = 0;
    String searchTitle = "";
    int alignBase = 40001001; // 기본값 = 최신순

    LinearLayout rental_product_list_box;
    Button more_view_button;
    ProgressBar loading_bar;



    AppCompatImageView one_grid_button;
    AppCompatImageView two_grid_button;



    ArrayList<Button> tab_buttons = new ArrayList<Button>();
    int tab_buttons_resIDs[] = {
        R.id.tab_button_index_0, R.id.tab_button_index_1, R.id.tab_button_index_2, R.id.tab_button_index_3,
        R.id.tab_button_index_4, R.id.tab_button_index_5, R.id.tab_button_index_6, R.id.tab_button_index_7
    };
    int[] tab_value_array = {
        0,30008000,30009000,30010000,30020000,30030000,30040000,30050000
    };

    /*===================================================================================================================*/


    Button search_button;
    boolean isSearchVisible = false;

    ConstraintLayout search_bar;
    EditText search_edittext;

    ConstraintLayout search_close_button_box;
    Button search_close_button;

    ConstraintLayout filter_button_box;
    Button filter_button;

    ConstraintLayout filter_dialog_box;

    ArrayList<CheckBoxA> checkBoxAs = new ArrayList<CheckBoxA>();
    int[] checkboxa_resIDs = {
        R.id.checkbox_index_0, R.id.checkbox_index_1, R.id.checkbox_index_2, R.id.checkbox_index_3, R.id.checkbox_index_4, R.id.checkbox_index_5, R.id.checkbox_index_6,
        R.id.checkbox_index_7, R.id.checkbox_index_8, R.id.checkbox_index_9, R.id.checkbox_index_10, R.id.checkbox_index_11, R.id.checkbox_index_12, R.id.checkbox_index_13,
        R.id.checkbox_index_14, R.id.checkbox_index_15, R.id.checkbox_index_16, R.id.checkbox_index_17, R.id.checkbox_index_18, R.id.checkbox_index_19, R.id.checkbox_index_20,
        R.id.checkbox_index_21, R.id.checkbox_index_22, R.id.checkbox_index_23, R.id.checkbox_index_24, R.id.checkbox_index_25, R.id.checkbox_index_26
    };
    ArrayList<TextView> checkBoxTextViews = new ArrayList<TextView>();
    int[] checkboxa_text_resIDs = {
        R.id.checkbox_index_0_textview, R.id.checkbox_index_1_textview, R.id.checkbox_index_2_textview, R.id.checkbox_index_3_textview, R.id.checkbox_index_4_textview, R.id.checkbox_index_5_textview, R.id.checkbox_index_6_textview,
        R.id.checkbox_index_7_textview, R.id.checkbox_index_8_textview, R.id.checkbox_index_9_textview, R.id.checkbox_index_10_textview, R.id.checkbox_index_11_textview, R.id.checkbox_index_12_textview, R.id.checkbox_index_13_textview,
        R.id.checkbox_index_14_textview, R.id.checkbox_index_15_textview, R.id.checkbox_index_16_textview, R.id.checkbox_index_17_textview, R.id.checkbox_index_18_textview, R.id.checkbox_index_19_textview, R.id.checkbox_index_20_textview,
        R.id.checkbox_index_21_textview, R.id.checkbox_index_22_textview, R.id.checkbox_index_23_textview, R.id.checkbox_index_24_textview, R.id.checkbox_index_25_textview, R.id.checkbox_index_26_textview
    };


    ArrayList<Button> align_buttons = new ArrayList<Button>();
    int[] align_button_resIDs = {
        R.id.align_button_index_0,R.id.align_button_index_1,R.id.align_button_index_2,R.id.align_button_index_3,R.id.align_button_index_4
    };


    Button clear_button;
    Button apply_button;

    /*===================================================================================================================*/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 05202: // 렌탄제품 리스트 가져오기 후처리
                    mobile05202(msg.obj);
                    break;
            }
        }
    };


    /*===================================================================================================================*/

    public Mobile05200() {

    }

    public Mobile05200(AppCompatActivity activity) {
        this.activity = activity;
    }

    /*===================================================================================================================*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.mobile05200, parentViewGroup, false);

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기


        index = 0;
        grid_num = 2;
        one_grid_button.setImageResource(R.drawable.mobile05200_one_grid_off);
        two_grid_button.setImageResource(R.drawable.mobile05200_two_grid_on);
        mobile05201(); // 렌탈제품 리스트 가져오기


        return rootView;
    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        useFul = new UseFul(activity);

        /*===================================================================================================================*/

        // 탭 버튼 (8개) 아이디 저장
        for (int i=0; i<tab_buttons_resIDs.length; i++) {
            tab_buttons.add((Button) rootView.findViewById(tab_buttons_resIDs[i]));
        }


        one_grid_button = (AppCompatImageView) rootView.findViewById(R.id.one_grid_button);
        two_grid_button = (AppCompatImageView) rootView.findViewById(R.id.two_grid_button);


        rental_product_list_box = (LinearLayout) rootView.findViewById(R.id.rental_product_list_box);
        more_view_button = (Button) rootView.findViewById(R.id.more_view_button);
        loading_bar = (ProgressBar) rootView.findViewById(R.id.loading_bar);

        search_button = (Button) rootView.findViewById(R.id.search_button);
        search_bar = (ConstraintLayout) rootView.findViewById(R.id.search_bar);
        search_edittext = (EditText) rootView.findViewById(R.id.search_edittext);

        filter_button_box = (ConstraintLayout) rootView.findViewById(R.id.filter_button_box);
        filter_button = (Button) rootView.findViewById(R.id.filter_button);
        filter_dialog_box = (ConstraintLayout) rootView.findViewById(R.id.filter_dialog_box);


        search_close_button_box = (ConstraintLayout) rootView.findViewById(R.id.search_close_button_box);
        search_close_button = (Button) rootView.findViewById(R.id.search_close_button);


        for (int i=0; i<checkboxa_resIDs.length; i++) {
            checkBoxAs.add((CheckBoxA) rootView.findViewById(checkboxa_resIDs[i]));
            checkBoxTextViews.add((TextView) rootView.findViewById(checkboxa_text_resIDs[i]));
            checkBoxAs.get(i).setChainView(checkBoxTextViews.get(i));
        }

        clear_button = (Button) rootView.findViewById(R.id.clear_button);
        apply_button = (Button) rootView.findViewById(R.id.apply_button);

        for (int i=0; i<align_button_resIDs.length; i++) {
            align_buttons.add((Button) rootView.findViewById(align_button_resIDs[i]));
        }


    }


    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        for (int i=0; i<tab_buttons.size(); i++) {
            tab_buttons.get(i).setOnClickListener(tab_button_event);
        }

        one_grid_button.setOnClickListener(grid_button_click_event);
        two_grid_button.setOnClickListener(grid_button_click_event);

        more_view_button.setOnClickListener(more_view_button_event);
        search_button.setOnClickListener(search_button_event);
        filter_button.setOnClickListener(filter_button_event);
        apply_button.setOnClickListener(apply_button_event);
        search_close_button.setOnClickListener(search_close_button__click_event);
        clear_button.setOnClickListener(clear_button_click_event);

        for (int i=0; i<align_buttons.size(); i++) {
            align_buttons.get(i).setOnClickListener(align_button_click_event);
        }
    }

    // 큰 카테고리 클릭시 이벤트
    View.OnClickListener tab_button_event = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            rental_product_list_box.removeAllViews();

            int button_index = Integer.parseInt(v.getTag().toString());

            for (int i=0; i<tab_buttons.size(); i++) {
                tab_buttons.get(i).setBackgroundResource(R.drawable.ripple_button_type_7);
                tab_buttons.get(i).setTextColor(useFul.getRcolor(R.color.mobile05200_tab_button_text_normal_color));
            }
            tab_buttons.get(button_index).setBackgroundResource(R.drawable.ripple_button_type_2);
            tab_buttons.get(button_index).setTextColor(useFul.getRcolor(R.color.mobile05200_tab_button_text_active_color));

            int value = tab_value_array[button_index];
            bigCategory = value;
            checkedValues.clear();
            searchTitle = "";
            index = 0;
            alignBase = 40001001;

            mobile05201();

        }
    };


    int grid_num = 1;

    View.OnClickListener grid_button_click_event = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.one_grid_button: // 1단 버튼 클릭 했을 경우
                    one_grid_button.setImageResource(R.drawable.mobile05200_one_grid_on);
                    two_grid_button.setImageResource(R.drawable.mobile05200_two_grid_off);

                    index = 0;
                    grid_num = 1;
                    rental_product_list_box.removeAllViews();
                    mobile05201();

                    break;
                case R.id.two_grid_button: // 2단 버튼 클릭 했을 경우
                    one_grid_button.setImageResource(R.drawable.mobile05200_one_grid_off);
                    two_grid_button.setImageResource(R.drawable.mobile05200_two_grid_on);

                    index = 0;
                    grid_num = 2;
                    rental_product_list_box.removeAllViews();
                    mobile05201();

                    break;
            }
        }
    };


    View.OnClickListener more_view_button_event = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!isLoading) {
                mobile05201();
            } else {
                useFul.showToast("작업이 진행중입니다.");
            }
        }
    };


    View.OnClickListener search_button_event = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!isSearchVisible) {
                // 검색바 스르륵 열기
                showSearchBar();

            } else {
                // 검색 하기
                index = 0;
                rental_product_list_box.removeAllViews();

                searchTitle = search_edittext.getText().toString();

                mobile05201();

            }
        }
    };

    View.OnClickListener filter_button_event = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showFilterDialog();
        }
    };


    int temp_align_code;


    // 적용하기 클릭시 이벤트
    View.OnClickListener apply_button_event = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            checkCheckedValue(); // 제품종류(배열)
            // alignBase 정렬기준
            for (int i=0; i<checkedValues.size(); i++) {
                Log.i("MyTags", "checkedValues.get("+i+")" + checkedValues.get(i));
            }
            hideFilterDialog();
        }
    };

    View.OnClickListener clear_button_click_event = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            for (int i=0; i<checkBoxAs.size(); i++) {
                if (checkBoxAs.get(i).getValue()) {
                    checkBoxAs.get(i).setChecked(false);
                }
            }

            alignBase = 40001001;
            for (int i=0; i<align_buttons.size(); i++) {
                align_buttons.get(i).setBackgroundResource(R.drawable.ripple_button_type_10);
                align_buttons.get(i).setTextColor(Color.parseColor("#333333"));
            }
            align_buttons.get(0).setBackgroundResource(R.drawable.ripple_button_type_2);
            align_buttons.get(0).setTextColor(Color.parseColor("#ffffff"));

        }
    };





    View.OnClickListener align_button_click_event = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int currentSelectedIndex = Integer.parseInt(v.getTag().toString());

            for (int i=0; i<align_buttons.size(); i++) {
                align_buttons.get(i).setBackgroundResource(R.drawable.ripple_button_type_10);
                align_buttons.get(i).setTextColor(Color.parseColor("#333333"));
            }
            align_buttons.get(currentSelectedIndex).setBackgroundResource(R.drawable.ripple_button_type_2);
            align_buttons.get(currentSelectedIndex).setTextColor(Color.parseColor("#ffffff"));

            switch (currentSelectedIndex) {
                case 0: // 최신순
                    alignBase = 40001001;
                    break;
                case 1: // 인기순
                    alignBase = 40001002;
                    break;
                case 2: // 낮은가격순
                    alignBase = 40001003;
                    break;
                case 3: // 높은가격순
                    alignBase = 40001004;
                    break;
                case 4: // 리뷰많은순
                    alignBase = 40001005;
                    break;
                default:

                    break;
            }

        }
    };

    View.OnClickListener search_close_button__click_event = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            hideSearchBar();
        }
    };

    public void showFilterDialog() {
        filter_dialog_box.setVisibility(View.VISIBLE);

        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(filter_dialog_box,"alpha",0f, 1f);
        objectAnimator.setDuration(500);
        objectAnimator.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT));

        ObjectAnimator objectAnimator2 = ObjectAnimator.ofFloat(filter_dialog_box,"translationY",300, 0);
        objectAnimator2.setDuration(500);
        objectAnimator2.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT));

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator, objectAnimator2);
        animatorSet.start();

    }

    public void hideFilterDialog() {

        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(filter_dialog_box,"alpha",1f, 0f);
        objectAnimator.setDuration(500);
        objectAnimator.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT));

        ObjectAnimator objectAnimator2 = ObjectAnimator.ofFloat(filter_dialog_box,"translationY",0, 300);
        objectAnimator2.setDuration(500);
        objectAnimator2.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT));

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator, objectAnimator2);
        animatorSet.start();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                filter_dialog_box.setVisibility(View.GONE);
            }
        }, 500);
    }



    ArrayList<Integer> checkedValues = new ArrayList<Integer>();


    // 체크된 체크박스 값 확인하기
    public void checkCheckedValue() {
        checkedValues.clear();

        for (int i=0; i<checkBoxAs.size(); i++) {
            if (checkBoxAs.get(i).getValue()) {
                // 체크되어 있으면
                // 값 가져와 배열에 저장하기
                checkedValues.add(checkBoxAs.get(i).getCheckboxValue());
            }
        }
    }





    public void showSearchBar() {
        isSearchVisible = true;

        int width = useFul.getDeviceWidth();
        int searchButtonWidth = useFul.getRdimen(R.dimen.mobile05200_search_button_width);
        int searchBarMarginEnd = useFul.getRdimen(R.dimen.mobile05200_search_bar_marginEnd);

        int search_bar_width = width - (2 * searchBarMarginEnd);
        final ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) search_bar.getLayoutParams();
        params.width = search_bar_width;
        search_bar.setLayoutParams(params);

        ValueAnimator animator = ValueAnimator.ofInt(searchButtonWidth, search_bar_width);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                params.width = (Integer) valueAnimator.getAnimatedValue();
                search_bar.requestLayout();
            }
        });
        animator.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT));
        animator.setDuration(500);

        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(search_edittext,"alpha",0f, 1f);
        objectAnimator.setDuration(500);
        objectAnimator.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT));

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animator, objectAnimator);
        animatorSet.start();


        final ConstraintLayout.LayoutParams params2 = (ConstraintLayout.LayoutParams) filter_button_box.getLayoutParams();
        filter_button_box.setLayoutParams(params2);

        ValueAnimator animator2 = ValueAnimator.ofInt(useFul.getRdimen(R.dimen.mobile05200_filter_button_invisible_marginBottom), useFul.getRdimen(R.dimen.mobile05200_filter_button_visible_marginBottom));
        animator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                params2.bottomMargin = (Integer) valueAnimator.getAnimatedValue();
                filter_button_box.requestLayout();
            }
        });
        animator2.setDuration(500);
        animator2.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT));

        ObjectAnimator objectAnimator2 = ObjectAnimator.ofFloat(filter_button_box,"alpha",0f, 1f);
        objectAnimator2.setDuration(500);
        objectAnimator2.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT));

        AnimatorSet animatorSet2 = new AnimatorSet();
        animatorSet2.playTogether(animator2, objectAnimator2);
        animatorSet2.start();




        final ConstraintLayout.LayoutParams params3 = (ConstraintLayout.LayoutParams) search_close_button_box.getLayoutParams();
        search_close_button_box.setLayoutParams(params3);

        ValueAnimator animator3 = ValueAnimator.ofInt(useFul.getRdimen(R.dimen.mobile05200_filter_button_invisible_marginBottom), useFul.getRdimen(R.dimen.mobile05200_filter_button_visible_marginBottom));
        animator3.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                params3.bottomMargin = (Integer) valueAnimator.getAnimatedValue();
                params3.rightMargin = (Integer) valueAnimator.getAnimatedValue();
                search_close_button_box.requestLayout();
            }
        });
        animator3.setDuration(500);
        animator3.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT));
        animator3.start();


        ObjectAnimator objectAnimator4 = ObjectAnimator.ofFloat(search_close_button_box,"alpha",0f, 1f);
        objectAnimator4.setDuration(500);
        objectAnimator4.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT));
        objectAnimator4.start();

    }

    public void hideSearchBar() {
        isSearchVisible = false;

        int width = useFul.getDeviceWidth();
        int searchButtonWidth = useFul.getRdimen(R.dimen.mobile05200_search_button_width);
        int searchBarMarginEnd = useFul.getRdimen(R.dimen.mobile05200_search_bar_marginEnd);

        int search_bar_width = width - (2 * searchBarMarginEnd);
        final ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) search_bar.getLayoutParams();
        params.width = search_bar_width;
        search_bar.setLayoutParams(params);

        ValueAnimator animator = ValueAnimator.ofInt(search_bar_width, searchButtonWidth);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                params.width = (Integer) valueAnimator.getAnimatedValue();
                search_bar.requestLayout();
            }
        });
        animator.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT));
        animator.setDuration(500);

        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(search_edittext,"alpha",1f, 0f);
        objectAnimator.setDuration(500);
        objectAnimator.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT));

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animator, objectAnimator);
        animatorSet.start();


        final ConstraintLayout.LayoutParams params2 = (ConstraintLayout.LayoutParams) filter_button_box.getLayoutParams();
        filter_button_box.setLayoutParams(params2);

        ValueAnimator animator2 = ValueAnimator.ofInt(useFul.getRdimen(R.dimen.mobile05200_filter_button_visible_marginBottom), useFul.getRdimen(R.dimen.mobile05200_filter_button_invisible_marginBottom));
        animator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                params2.bottomMargin = (Integer) valueAnimator.getAnimatedValue();
                filter_button_box.requestLayout();
            }
        });
        animator2.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT));
        animator2.setDuration(500);

        ObjectAnimator objectAnimator2 = ObjectAnimator.ofFloat(filter_button_box,"alpha",1f, 0f);
        objectAnimator2.setDuration(500);
        objectAnimator2.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT));

        AnimatorSet animatorSet2 = new AnimatorSet();
        animatorSet2.playTogether(animator2, objectAnimator2);
        animatorSet2.start();



        final ConstraintLayout.LayoutParams params3 = (ConstraintLayout.LayoutParams) search_close_button_box.getLayoutParams();
        search_close_button_box.setLayoutParams(params3);
        ValueAnimator animator3 = ValueAnimator.ofInt(useFul.getRdimen(R.dimen.mobile05200_filter_button_visible_marginBottom), useFul.getRdimen(R.dimen.mobile05200_filter_button_invisible_marginBottom));
        animator3.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                params3.bottomMargin = (Integer) valueAnimator.getAnimatedValue();
                params3.rightMargin = (Integer) valueAnimator.getAnimatedValue();
                search_close_button_box.requestLayout();
            }
        });
        animator3.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT));
        animator3.setDuration(500);
        animator3.start();


        ObjectAnimator objectAnimator4 = ObjectAnimator.ofFloat(search_close_button_box,"alpha",1f, 0f);
        objectAnimator4.setDuration(500);
        objectAnimator4.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT));
        objectAnimator4.start();
    }

    /*===================================================================================================================*/

    // 렌탈제품 리스트 가져오기
    public void mobile05201() {
        isLoading = true;
        loading_bar.setVisibility(View.VISIBLE);

        String smallCategoryStrArray = "";
        for (int i=0; i<checkedValues.size(); i++) {
            smallCategoryStrArray += checkedValues.get(i);
            if (i != checkedValues.size()-1) {
                smallCategoryStrArray+=",";
            }
        }

        Log.i("MyTags", "bigCategory = "+bigCategory);
        Log.i("MyTags", "smallCategoryStrArray = "+smallCategoryStrArray);
        Log.i("MyTags", "searchTitle = "+searchTitle);
        Log.i("MyTags", "alignBase = "+alignBase);


        ContentValues params = new ContentValues();
        params.put("act", "mobile05201");
        params.put("member", new ShareInfo(activity).getMember());
        params.put("bigCategory", bigCategory); // 큰 카테고리
        params.put("smallCategory", smallCategoryStrArray); // 작은 카테고리
        params.put("searchTitle", searchTitle); // 검색어
        params.put("alignBase", alignBase); // 정렬기준
        params.put("index", index);
        params.put("view_num", view_num);


        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(05202);
        inLet.start();
    }

    // 렌탈제품 리스트 가져오기 후처리
    public void mobile05202(Object obj) {
        isLoading = false;

        String data = obj.toString();
        Log.i("MyTags", "mobile05202 data = "+data);

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");
            if (result.equals("ok")) {

                final ArrayList<RentalItem> items = new ArrayList<RentalItem>();

                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    int vbp = jsonObject1.getInt("vbp");
                    int big_category = jsonObject1.getInt("big_category");
                    int small_category = jsonObject1.getInt("small_category");
                    String normal_image_url = jsonObject1.getString("normal_image_url");
                    String title = jsonObject1.getString("title");
                    int coupon = jsonObject1.getInt("coupon");
                    int sale = jsonObject1.getInt("sale");
                    int gift = jsonObject1.getInt("gift");
                    String heart_value_str = jsonObject1.getString("heart_value");
                    int heart_value = 0;
                    if (heart_value_str.equals("")) {
                        heart_value = 0;
                    } else {
                        heart_value = Integer.parseInt(heart_value_str);
                    }
                    String price_string = jsonObject1.getString("price_string");
                    String[] price_string_array = price_string.split("   ");


                    RentalItem item = new RentalItem(activity, grid_num);
                    item.setPk(vbp);
                    item.setImageUrl(normal_image_url);

                    if (grid_num == 1) {
                        item.setTitle(useFul.cutString(title, 20));
                    } else {
                        item.setTitle(useFul.cutString(title, 9));
                    }
                    item.setCoupon(coupon);
                    item.setSale(sale);
                    item.setGift(gift);
                    item.setHeartValue(heart_value);
                    if (grid_num == 1) {
                        item.setPrice(price_string);
                    } else {
                        item.setPrice(price_string_array[0]);
                    }
                    item.setOnItemClickListener(onItemClickEvent);


                    items.add(item);
                }

                final ArrayList<RentalItem> items2 = items;
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (grid_num == 1) {
                            for (int i = 0; i < items2.size(); i++) {
                                rental_product_list_box.addView(items2.get(i));
                            }
                        } else {
                            for (int i = 0; i < items2.size(); i++) {
                                if (i % 2 == 0) {
                                    LinearLayout linearLayout = new LinearLayout(activity);
                                    linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                    params.bottomMargin = useFul.getRdimen(R.dimen.mobile05200_rental_two_grid_between_size);
                                    linearLayout.setLayoutParams(params);

                                    linearLayout.addView(items2.get(i));
                                    if ((i+1) != items2.size()) {
                                        linearLayout.addView(items2.get(i+1));
                                    } else {
                                        LinearLayout linearLayout2 = new LinearLayout(activity);
                                        linearLayout2.setOrientation(LinearLayout.HORIZONTAL);
                                        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                        params1.weight = 1f;
                                        params1.height = 100;
                                        linearLayout2.setLayoutParams(params1);
                                        linearLayout.addView(linearLayout2);
                                    }

                                    rental_product_list_box.addView(linearLayout);
                                    i++;
                                }
                            }

                        }

                        index += view_num;
                        Log.i("MyTags", "isLoading 다시 false로 만들음");
                        isLoading = false;
                        loading_bar.setVisibility(View.GONE);
                    }
                });

            } else {
                useFul.showToast("데이터가 없습니다.");
                isLoading = false;
                loading_bar.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    View.OnClickListener onItemClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int pk = Integer.parseInt(v.getTag().toString());

            Log.i("MyTags", "선택한 제품의 pk는 "+pk);
            Intent intent = new Intent(activity, Mobile06000.class);
            intent.putExtra("pk", pk);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
    };
}
