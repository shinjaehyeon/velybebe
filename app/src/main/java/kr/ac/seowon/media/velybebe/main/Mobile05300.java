package kr.ac.seowon.media.velybebe.main;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.InLet;
import kr.ac.seowon.media.velybebe.common.ShareInfo;
import kr.ac.seowon.media.velybebe.common.UseFul;
import kr.ac.seowon.media.velybebe.eventdetail.Mobile08000;

@SuppressLint("ValidFragment")
// 회원가입 STEP 01
public class Mobile05300 extends Fragment {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;

    /*===================================================================================================================*/

    View rootView;


    int index = 0;
    int view_num = 6;

    boolean isLoading;
    LinearLayout event_list_box;
    Button more_view_button;
    ProgressBar progress_bar;



    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 05302: // 이벤트 리스트 가져오기 후처리
                    mobile05302(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };

    /*===================================================================================================================*/

    public Mobile05300() {

    }

    public Mobile05300(AppCompatActivity activity) {
        this.activity = activity;
    }

    /*===================================================================================================================*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.mobile05300, parentViewGroup, false);

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기


        mobile05301(); // 이벤트 리스트 가져오기

        return rootView;
    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        useFul = new UseFul(activity);

        /*===================================================================================================================*/

        event_list_box = (LinearLayout) rootView.findViewById(R.id.event_list_box);
        progress_bar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
        more_view_button = (Button) rootView.findViewById(R.id.more_view_button);
    }


    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        more_view_button.setOnClickListener(more_view_button_click_event);

    }

    View.OnClickListener more_view_button_click_event = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!isLoading) {

                mobile05301();

            } else {
                UseFul.callVelyBebeNormalDialog(activity,"안내", "작업이 진행중입니다.");
            }
        }
    };

    /*===================================================================================================================*/

    // 이벤트 리스트 가져오기
    public void mobile05301() {
        isLoading = true;
        progress_bar.setVisibility(View.VISIBLE);

        ContentValues params = new ContentValues();
        params.put("act", "mobile05301");
        params.put("member", new ShareInfo(activity).getMember());
        params.put("index", index);
        params.put("view_num", view_num);

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(05302);
        inLet.start();
    }

    // 이벤트 리스트 가져오기 후처리
    public void mobile05302(Object obj) {
        String data = obj.toString();
        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {

                ArrayList<EventItem> items = new ArrayList<EventItem>();

                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    int event = jsonObject1.getInt("event");
                    String list_image_url = jsonObject1.getString("list_image_url");
                    String title = jsonObject1.getString("title");
                    String start_datetime = jsonObject1.getString("start_datetime");
                    String end_datetime = jsonObject1.getString("end_datetime");
                    int view_index = jsonObject1.getInt("view_index");
                    int status = jsonObject1.getInt("status");
                    String status_string = jsonObject1.getString("status_string");

                    EventItem item = new EventItem(activity);
                    item.setPk(event);
                    item.setEventImageUrl(list_image_url);
                    item.setEventTitle(title);
                    item.setEventPeriod(start_datetime.split(" ")[0]+" ~ "+end_datetime.split(" ")[0]);
                    item.setViewIndex(view_index);
                    item.setStatus(status);
                    item.setOnItemClickListener(itemOnClickEvent, item);

                    items.add(item);
                }

                final ArrayList<EventItem> temp = items;

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i=0; i<temp.size(); i++) {
                            event_list_box.addView(temp.get(i));
                        }

                        index += view_num;
                        isLoading = false;
                        progress_bar.setVisibility(View.GONE);
                    }
                });




            } else {
                useFul.showToast("이벤트가 더이상 없습니다.");
                isLoading = false;
                progress_bar.setVisibility(View.GONE);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    View.OnClickListener itemOnClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int pk = ((EventItem) v.getTag()).getPk();

            Log.i("MyTags", "클릭한 이벤트 pk = "+pk);
            Intent intent = new Intent(activity, Mobile08000.class);
            intent.putExtra("pk", pk);
            activity.startActivity(intent);
        }
    };
}
