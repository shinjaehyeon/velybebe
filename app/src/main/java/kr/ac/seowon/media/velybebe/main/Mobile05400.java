package kr.ac.seowon.media.velybebe.main;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.InLet;
import kr.ac.seowon.media.velybebe.common.ShareInfo;
import kr.ac.seowon.media.velybebe.common.UseFul;

@SuppressLint("ValidFragment")
// 회원가입 STEP 01
public class Mobile05400 extends Fragment {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;

    /*===================================================================================================================*/

    View rootView;

    int index = 0;
    int view_num = 6;
    boolean isLoading = false;


    TextView donate_point_textview;

    ProgressBar progress_bar;
    Button more_view_button;
    LinearLayout donate_company_list_box;




    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 5402: // 기부 포인트 가져오기 후처리
                    mobile05402(msg.obj);
                    break;

                case 5422: // 기부 업체 리스트 가져오기 후처리
                    mobile05422(msg.obj);
                    break;
            }
        }
    };

    /*===================================================================================================================*/

    public Mobile05400() {

    }

    public Mobile05400(AppCompatActivity activity) {
        this.activity = activity;
    }

    /*===================================================================================================================*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.mobile05400, parentViewGroup, false);

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기


        mobile05401(); // 기부 포인트 가져오기
        mobile05421(); // 기부 업체 리스트 가져오기


        return rootView;
    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        useFul = new UseFul(activity);

        /*===================================================================================================================*/

        donate_point_textview = (TextView) rootView.findViewById(R.id.donate_point_textview);

        progress_bar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
        more_view_button = (Button) rootView.findViewById(R.id.more_view_button);
        donate_company_list_box = (LinearLayout) rootView.findViewById(R.id.donate_company_list_box);
    }


    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {


    }


    /*===================================================================================================================*/

    // 기부 포인트 가져오기
    public void mobile05401() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile05401");
        params.put("member", new ShareInfo(activity).getMember());
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(5402);
        inLet.start();
    }

    // 기부 포인트 가져오기 후처리
    public void mobile05402(Object obj) {
        String data = obj.toString();
        Log.i("MyTags", "data = "+data);

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");
            if (result.equals("ok")) {

                int donate_point = jsonObject.getInt("donate_point");
                donate_point_textview.setText(donate_point+" P");

            }  else {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }




    // 기부 업체 리스트 가져오기
    public void mobile05421() {
        isLoading = true;
        progress_bar.setVisibility(View.VISIBLE);

        ContentValues params = new ContentValues();
        params.put("act", "mobile05421");
        params.put("member", new ShareInfo(activity).getMember());
        params.put("index", index);
        params.put("view_num", view_num);

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(5422);
        inLet.start();
    }

    // 기부 업체 리스트 가져오기 후처리
    public void mobile05422(Object obj) {
        String data = obj.toString();
        Log.i("MyTags", "data = "+data);

        isLoading = false;

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");
            if (result.equals("ok")) {

                ArrayList<DonateItem> items = new ArrayList<DonateItem>();

                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    int vbdc = jsonObject1.getInt("vbdc");
                    String title = jsonObject1.getString("title");
                    String description = jsonObject1.getString("description");
                    int status = jsonObject1.getInt("status");

                    DonateItem item = new DonateItem(activity);
                    item.setPk(vbdc);
                    item.setDonateTitle(title);
                    item.setDonateContent(useFul.cutString(description, 18));
                    item.setStatus(status);

                    items.add(item);
                }

                final ArrayList<DonateItem> items2 = items;

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i=0; i<items2.size(); i++) {
                            donate_company_list_box.addView(items2.get(i));
                        }

                        index += view_num;
                        progress_bar.setVisibility(View.GONE);
                        isLoading = false;
                    }
                });



            }  else {
                progress_bar.setVisibility(View.GONE);
                isLoading = false;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
