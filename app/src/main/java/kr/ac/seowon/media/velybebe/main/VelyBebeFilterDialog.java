package kr.ac.seowon.media.velybebe.main;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.UseFul;

public class VelyBebeFilterDialog extends Dialog {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;

    /*===================================================================================================================*/

    int dialog_box_width;
    int dialog_box_height;

    LinearLayout dialog_box;

    TextView dialog_title_textview;
    TextView dialog_content_textview;

    LinearLayout button_area;
    Button cancel_button;
    Button confirm_button;
    Button temp_button;
    Button temp_button2;

    /*===================================================================================================================*/

    public VelyBebeFilterDialog(@NonNull Context context) {
        super(context);
        this.context = context;
        this.activity = (AppCompatActivity) context;
    }

    public VelyBebeFilterDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        this.context = context;
        this.activity = (AppCompatActivity) context;
    }

    protected VelyBebeFilterDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        this.context = context;
        this.activity = (AppCompatActivity) context;
    }

    /*===================================================================================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //타이틀 바 삭제
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.velybebe_normal_dialog);

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기
    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        useFul = new UseFul(activity);

        /*===================================================================================================================*/

        dialog_box = (LinearLayout) findViewById(R.id.dialog_box);

        dialog_title_textview = (TextView) findViewById(R.id.dialog_title_textview);
        dialog_content_textview = (TextView) findViewById(R.id.dialog_content_textview);

        button_area = (LinearLayout) findViewById(R.id.button_area);
        cancel_button = (Button) findViewById(R.id.cancel_button);
        confirm_button = (Button) findViewById(R.id.confirm_button);
        temp_button = (Button) findViewById(R.id.temp_button);
        temp_button2 = (Button) findViewById(R.id.temp_button2);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {
        dialog_box.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        dialog_box_width = dialog_box.getMeasuredWidth();
        dialog_box_height = dialog_box.getMeasuredHeight();

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }

    /*===================================================================================================================*/

    // 표시될 때 보여줄 애니메이션 시작하기
    public void startDialogAnimation() {
        float pivotX = dialog_box_width / 2f;
        float pivotY = dialog_box_height / 2f;

        Log.i("MyTags", "pivotX = "+pivotX);
        Log.i("MyTags", "pivotY = "+pivotY);

        ScaleAnimation ani = new ScaleAnimation (0.8f, 1f, 0.8f, 1f, pivotX, pivotY);
        ani.setFillAfter(true);
        ani.setDuration(500);
        ani.setFillEnabled(true);
        ani.setInterpolator(AnimationUtils.loadInterpolator(activity, android.R.anim.bounce_interpolator));
        dialog_box.startAnimation(ani);
    }

    // 다이얼로그 타이틀 텍스트 설정하기
    public void setDialogTitle(String s) {
        dialog_title_textview.setText(s);
    }

    // 다이얼로그 내용 텍스트 설정하기
    public void setDialogContent(String s) {
        dialog_content_textview.setText(s);
    }

    // 취소버튼 클릭이벤트 설정하기
    public void setCancelButtonEvent(View.OnClickListener e) {
        cancel_button.setOnClickListener(e);
    }

    // 취소버튼 텍스트 설정하기
    public void setCancelButtonText(String s) {
        cancel_button.setText(s);
    }

    // 확인버튼 클릭이벤트 설정하기
    public void setConfirmButtonEvent(View.OnClickListener e) {
        confirm_button.setOnClickListener(e);
    }

    // 확인버튼 텍스트 설정하기
    public void setConfirmButtonText(String s) {
        confirm_button.setText(s);
    }

    // 버튼 보이는 옵션 설정하기 (b1 = 취소, b2 = 확인)
    public void buttonVisibleOptions(boolean b1, boolean b2) {
        if (b1 && b2) {
            temp_button2.setVisibility(View.GONE);
            cancel_button.setVisibility(View.VISIBLE);
            confirm_button.setVisibility(View.VISIBLE);
            temp_button.setVisibility(View.GONE);
        } else if (b1 == false && b2 == true) {
            temp_button2.setVisibility(View.GONE);

            LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams) cancel_button.getLayoutParams();
            params1.weight = 3;
            cancel_button.setVisibility(View.INVISIBLE);
            cancel_button.setLayoutParams(params1);

            LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams) confirm_button.getLayoutParams();
            params2.weight = 2;
            confirm_button.setVisibility(View.VISIBLE);
            confirm_button.setLayoutParams(params2);

            temp_button.setVisibility(View.INVISIBLE);
        } else if (b1 == true && b2 == false) {
            temp_button2.setVisibility(View.INVISIBLE);

            LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams) cancel_button.getLayoutParams();
            params1.weight = 2;
            params1.setMarginEnd(0);
            cancel_button.setVisibility(View.VISIBLE);
            cancel_button.setLayoutParams(params1);

            confirm_button.setVisibility(View.GONE);

            temp_button.setVisibility(View.INVISIBLE);
        } else {
            button_area.setVisibility(View.GONE);
        }
    }
}
