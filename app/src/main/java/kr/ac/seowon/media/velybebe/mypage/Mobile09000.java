package kr.ac.seowon.media.velybebe.mypage;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.LogoutResponseCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kr.ac.seowon.media.velybebe.MainActivity;
import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.InLet;
import kr.ac.seowon.media.velybebe.common.ShareInfo;
import kr.ac.seowon.media.velybebe.common.TopBar;
import kr.ac.seowon.media.velybebe.common.UrlImageGetDisplay;
import kr.ac.seowon.media.velybebe.common.UseFul;
import kr.ac.seowon.media.velybebe.common.VelyBebeNormalDialog;
import kr.ac.seowon.media.velybebe.login.Mobile03000;
import kr.ac.seowon.media.velybebe.orderstatus.Mobile11000;
import kr.ac.seowon.media.velybebe.rentalhistory.Mobile10000;

public class Mobile09000 extends AppCompatActivity {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;
    TopBar topBar;

    boolean isLoading = false;

    /*===================================================================================================================*/

    ConstraintLayout best_parent_layout;

    /*===================================================================================================================*/


    TextView name_textview;
    Button my_profile_view_button;



    Button rental_history_button; // 렌탈 내역
    Button point_coupon_membership_button; // 포인트/쿠폰/멤버쉽
    Button heart_history_button; // 찜한 내역
    Button bucket_button; // 장바구니
    Button order_status_button; // 주문현황
    Button logout_button; // 로그아웃

    Button my_question_status_button; // 내 문의 현황
    Button event_apply_history_button; // 이벤트 참여내역
    Button as_history_button; // A/S 신청내역


    ArrayList<TextView> num_textviews  = new ArrayList<TextView>();
    int[] num_textview_ids = {R.id.num_textview_index_0, R.id.num_textview_index_1, R.id.num_textview_index_2, R.id.num_textview_index_3, R.id.num_textview_index_4};



    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 9002: // 프로필 정보 가져오기 후처리
                    mobile09002(msg.obj);
                    break;
                case 9004: // 각 단계별 제품 수 가져오기 후처리
                    mobile09004(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };

    /*===================================================================================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile09000);


        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        getIntentInfo(); // 인텐트 정보 받아오기

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기


        mobile09001(); // 프로필 정보 가져오기
        mobile09003(); // 각 단계별 제품 수 가져오기
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Log.i("MyTags", "Main onStart 시작");
        new UseFul(activity).setAppRunning(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Log.i("MyTags", "Main onPause 시작");
        new UseFul(activity).setAppRunning(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Log.i("MyTags", "Main onResume 시작");
        new UseFul(activity).setAppRunning(1);
    }





    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(AppCompatActivity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // 인테트 정보 받아오기
    public void getIntentInfo() {

    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        useFul = new UseFul(activity);
        best_parent_layout = (ConstraintLayout) findViewById(R.id.best_parent_layout);
        topBar = (TopBar) findViewById(R.id.topbar);

        /*===================================================================================================================*/

        name_textview = (TextView) findViewById(R.id.name_textview);
        my_profile_view_button = (Button) findViewById(R.id.my_profile_view_button);

        rental_history_button = (Button) findViewById(R.id.rental_history_button);
        point_coupon_membership_button = (Button) findViewById(R.id.point_coupon_membership_button);
        heart_history_button = (Button) findViewById(R.id.heart_history_button);
        bucket_button = (Button) findViewById(R.id.bucket_button);
        order_status_button = (Button) findViewById(R.id.order_status_button);
        logout_button = (Button) findViewById(R.id.logout_button);
        my_question_status_button = (Button) findViewById(R.id.my_question_status_button);
        event_apply_history_button = (Button) findViewById(R.id.event_apply_history_button);
        as_history_button = (Button) findViewById(R.id.as_history_button);

        for (int i=0; i<num_textview_ids.length; i++) {
            num_textviews.add((TextView) findViewById(num_textview_ids[i]));
        }
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {
        // topBar.setTitle("회원가입");
        // topBar.setMenuSideBar();
        topBar.hideMenuButton();
        topBar.showBackButton();


    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        rental_history_button.setOnClickListener(mypageMajorButtonClickEvent);
        order_status_button.setOnClickListener(mypageMajorButtonClickEvent);
        logout_button.setOnClickListener(mypageMajorButtonClickEvent);
    }



    // 프로필 정보 가져오기
    public void mobile09001() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile09001");
        params.put("member", new ShareInfo(this).getMember());
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(9002);
        inLet.start();
    }

    // 프로필 정보 가져오기 후처리
    public void mobile09002(Object obj) {
        String data = obj.toString();
        Log.i("MyTags", "data = "+data);

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");
            if (result.equals("ok")) {

                String name = jsonObject.getString("name");
                name_textview.setText(name+"님");



            } else {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /*===================================================================================================================*/

    boolean isLogout;
    // 마이페이지 주요 버튼 클릭 이벤트
    View.OnClickListener mypageMajorButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.rental_history_button: // 렌탈 내역 버튼 클릭시 이벤트
                    useFul.goActivity(Mobile10000.class);
                    break;
                case R.id.order_status_button: // 주문현황 버튼 클릭시 이벤트
                    useFul.goActivity(Mobile11000.class);
                    break;
                case R.id.logout_button: // 로그아웃 버튼 클릭시 이벤트
                    isLogout = true;
                    callVelyBebeNormalDialog2("안내", "정말 로그아웃 하시겠습니까?");
                    break;
            }
        }
    };


    public void clearShareMemberInfo() {
        ShareInfo shareInfo = new ShareInfo(activity);

        shareInfo.setInputIntValue("member", 0);
        shareInfo.setInputIntValue("member_signupmethod", 0);
    }


    /*===================================================================================================================*/

    // 뒤로가기 할 시 경고 팝업창 띄우기
    public void callVelyBebeNormalDialog(String title, String content) {
        final VelyBebeNormalDialog dialog = new VelyBebeNormalDialog(this);
        dialog.show();
        dialog.startDialogAnimation();
        dialog.setDialogTitle(title);
        dialog.setDialogContent(content);
        dialog.buttonVisibleOptions(true, true);
        // dialog.setCancelButtonText("");
        // dialog.setConfirmButtonText("");
        dialog.setCancelButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setConfirmButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }


    boolean isFinish;
    // 통보 형식의 다이얼로그 호출
    public void callVelyBebeNormalDialog2(String title, String content) {
        final VelyBebeNormalDialog dialog = new VelyBebeNormalDialog(this);
        dialog.show();
        dialog.startDialogAnimation();
        dialog.setDialogTitle(title);
        dialog.setDialogContent(content);
        dialog.buttonVisibleOptions(false, true);
        if (isLogout) {
            dialog.buttonVisibleOptions(true, true);

        }

        // dialog.setCancelButtonText("");
        // dialog.setConfirmButtonText("");
        dialog.setCancelButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setConfirmButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLogout) {
                    goLogout();
                }
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (isFinish) {
                    finish();
                }

                isLogout = false;
            }
        });
    }


    public void goLogout() {
        mobile09011(); // 푸쉬토큰값 삭제하기



        Log.i("MyTags", "goLogout 실행");

        ShareInfo shareInfo = new ShareInfo(activity);

        Log.i("MyTags", "shareInfo.getMemberSingUpMethod() = "+shareInfo.getMemberSingUpMethod());

        switch (shareInfo.getMemberSingUpMethod()) {
            case 30001001: // 일반 로그인일 경우
                clearShareMemberInfo();
                finishAffinity();
                useFul.goActivity(Mobile03000.class);
                break;
            case 30001002: // 페이스북 로그인일 경우
                clearShareMemberInfo();
//                GraphRequest.Callback callback = new GraphRequest.Callback() {
//                    @Override
//                    public void onCompleted(GraphResponse response) {
//                        try {
//                            if(response.getError() != null) {
//
//                            }
//                            else if (response.getJSONObject().getBoolean("success")) {
//                                LoginManager.getInstance().logOut();
//                            }
//                        } catch (JSONException ex) { }
//                    }
//                };
//                GraphRequest request = new GraphRequest(AccessToken.getCurrentAccessToken(), "me/permissions", new Bundle(), HttpMethod.DELETE, callback);
//                request.executeAsync();
                LoginManager.getInstance().logOut();
                finishAffinity();
                useFul.goActivity(Mobile03000.class);
                break;
            case 30001003: // 카카오 로그인일 경우
                clearShareMemberInfo();
                UserManagement.getInstance().requestLogout(new LogoutResponseCallback() {
                    @Override
                    public void onCompleteLogout() {

                    }
                });
                finishAffinity();
                useFul.goActivity(Mobile03000.class);
                break;
        }
    }

    // 푸쉬토큰값 삭제하기
    public void mobile09011() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile09011");
        params.put("member", new ShareInfo(this).getMember());
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(9012);
        inLet.start();
    }

    // 푸쉬토큰값 삭제하기 후처리
    public void mobile09012(Object obj) {
        String data = obj.toString();
    }




    // 각 단계별 제품 수 가져오기
    public void mobile09003() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile09003");
        params.put("member", new ShareInfo(this).getMember());
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(9004);
        inLet.start();
    }

    // 각 단계별 제품 수 가져오기 후처리
    public void mobile09004(Object obj) {
        String data = obj.toString();

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");
            if (result.equals("ok")) {

                int index_0_num = jsonObject.getInt("index_0_num");
                int index_1_num = jsonObject.getInt("index_1_num");
                int index_2_num = jsonObject.getInt("index_2_num");
                int index_3_num = jsonObject.getInt("index_3_num");
                int index_4_num = jsonObject.getInt("index_4_num");

                num_textviews.get(0).setText(index_0_num+"");
                num_textviews.get(1).setText(index_1_num+"");
                num_textviews.get(2).setText(index_2_num+"");
                num_textviews.get(3).setText(index_3_num+"");
                num_textviews.get(4).setText(index_4_num+"");

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /*===================================================================================================================*/




    @Override
    public void onBackPressed() {
        super.onBackPressed();

        // callVelyBebeNormalDialog("안내", "정말 블리베베 앱을 종료하시겠습니까?"); // 뒤로가기 할 시 경고 팝업창 띄우기
    }
}
