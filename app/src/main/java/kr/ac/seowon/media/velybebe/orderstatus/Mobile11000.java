package kr.ac.seowon.media.velybebe.orderstatus;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daasuu.ei.Ease;
import com.daasuu.ei.EasingInterpolator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.InLet;
import kr.ac.seowon.media.velybebe.common.RealPathUtil;
import kr.ac.seowon.media.velybebe.common.ShareInfo;
import kr.ac.seowon.media.velybebe.common.TopBar;
import kr.ac.seowon.media.velybebe.common.UrlImageGetDisplay;
import kr.ac.seowon.media.velybebe.common.UseFul;
import kr.ac.seowon.media.velybebe.common.VelyBebeNormalDialog;
import kr.ac.seowon.media.velybebe.rentaldetail.Mobile06000;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Mobile11000 extends AppCompatActivity {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;
    TopBar topBar;

    boolean isLoading = false;

    /*===================================================================================================================*/

    ConstraintLayout best_parent_layout;

    /*===================================================================================================================*/


    LinearLayout list_area;
    ProgressBar progress_bar;


    int index;
    int view_num = 10;
    int previous_selected_index;
    int type_index;

    ArrayList<Button> buttons = new ArrayList<Button>();
    int[] button_ids = {R.id.button_index_0, R.id.button_index_1, R.id.button_index_2, R.id.button_index_3, R.id.button_index_4};

    ArrayList<TextView> num_textviews  = new ArrayList<TextView>();
    int[] num_textview_ids = {R.id.num_textview_index_0, R.id.num_textview_index_1, R.id.num_textview_index_2, R.id.num_textview_index_3, R.id.num_textview_index_4};

    ArrayList<TextView> text_textviews  = new ArrayList<TextView>();
    int[] text_textview_ids = {R.id.text_textview_index_0, R.id.text_textview_index_1, R.id.text_textview_index_2, R.id.text_textview_index_3, R.id.text_textview_index_4};

    /*===================================================================================================================*/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 11002: // 해당 단계에 해당하는 주문현황 렌탈아이템 리스트 가져오기 후처리
                    mobile11002(msg.obj);
                    break;
                case 11004: // 각 단계별 제품 수 가져오기 후처리
                    mobile11004(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };

    /*===================================================================================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile11000);


        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        getIntentInfo(); // 인텐트 정보 받아오기

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기



        mobile11001(); // 해당 단계에 해당하는 주문현황 렌탈아이템 리스트 가져오기
        mobile11003(); // 각 단계별 제품 수 가져오기


    }

    @Override
    protected void onStart() {
        super.onStart();
        // Log.i("MyTags", "Main onStart 시작");
        new UseFul(activity).setAppRunning(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Log.i("MyTags", "Main onPause 시작");
        new UseFul(activity).setAppRunning(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Log.i("MyTags", "Main onResume 시작");
        new UseFul(activity).setAppRunning(1);
    }





    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(AppCompatActivity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // 인테트 정보 받아오기
    public void getIntentInfo() {

    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        useFul = new UseFul(activity);
        best_parent_layout = (ConstraintLayout) findViewById(R.id.best_parent_layout);
        topBar = (TopBar) findViewById(R.id.topbar);

        /*===================================================================================================================*/

        list_area = (LinearLayout) findViewById(R.id.list_area);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);

        for (int i=0; i<button_ids.length; i++) {
            buttons.add((Button) findViewById(button_ids[i]));
            num_textviews.add((TextView) findViewById(num_textview_ids[i]));
            text_textviews.add((TextView) findViewById(text_textview_ids[i]));
        }
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {
        // topBar.setTitle("회원가입");
        // topBar.setMenuSideBar();
        topBar.hideMenuButton();
        topBar.showBackButton();



    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        for (int i=0; i<buttons.size(); i++) {
            buttons.get(i).setOnClickListener(buttonsClickEvent);
        }
    }





    /*===================================================================================================================*/

    View.OnClickListener buttonsClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int selectIndex = Integer.parseInt(v.getTag().toString());

            // 이전 인덱스 비활성화 효과 주기
            text_textviews.get(previous_selected_index).setTextColor(useFul.getRcolor(R.color.mobile11000_tab_button_text_inactive_color));
            num_textviews.get(previous_selected_index).setTextColor(useFul.getRcolor(R.color.mobile11000_tab_button_text_inactive_color));


            // 선택된 인덱스 활성화 효과 주기
            text_textviews.get(selectIndex).setTextColor(useFul.getRcolor(R.color.mobile11000_tab_button_text_active_color));
            num_textviews.get(selectIndex).setTextColor(useFul.getRcolor(R.color.mobile11000_tab_button_text_active_color));

            // 해당 단계에 해당하는 주문현황 렌탈아이템 리스트 가져오기
            list_area.removeAllViews();
            index = 0;
            type_index = selectIndex;
            mobile11001();


            previous_selected_index = selectIndex;
        }
    };


    // 해당 단계에 해당하는 주문현황 렌탈아이템 리스트 가져오기
    public void mobile11001() {
        progress_bar.setVisibility(View.VISIBLE);

        ContentValues params = new ContentValues();
        params.put("act", "mobile11001");
        params.put("member", new ShareInfo(this).getMember());
        params.put("index", index);
        params.put("view_num", view_num);
        params.put("type_index", type_index);

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(11002);
        inLet.start();
    }

    // 해당 단계에 해당하는 주문현황 렌탈아이템 리스트 가져오기 후처리
    public void mobile11002(Object obj) {
        String data = obj.toString();
        Log.i("MyTags", "mobile11002 data = "+data);

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");
            if (result.equals("ok")) {

                ArrayList<LinearLayout> items = new ArrayList<LinearLayout>();

                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    int rental = jsonObject1.getInt("rental");
                    int vbp = jsonObject1.getInt("vbp");
                    String product_image_url = jsonObject1.getString("product_image_url");
                    String product_title = jsonObject1.getString("product_title");
                    String period_string = jsonObject1.getString("period_string");
                    String start_datetime = jsonObject1.getString("start_datetime");
                    String end_datetime = jsonObject1.getString("end_datetime");
                    int pay_price = jsonObject1.getInt("pay_price");

                    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final LinearLayout orderStatusItem = (LinearLayout) inflater.inflate(R.layout.order_status_item, null);

                    orderStatusItem.setOnClickListener(orderStatusItemClickEvent);

                    // 제품 pk 설정하기
                    TextView vbp_textview = (TextView) orderStatusItem.findViewById(R.id.vbp_textview);
                    vbp_textview.setText(vbp+"");

                    // 제품 이미지 뿌리기
                    AppCompatImageView product_imageview = (AppCompatImageView) orderStatusItem.findViewById(R.id.product_imageview);
                    UrlImageGetDisplay u = new UrlImageGetDisplay(activity);
                    u.setImageUrl(product_image_url);
                    u.setImageView(product_imageview);
                    u.start();

                    // 제품명 뿌리기
                    TextView product_title_textview = (TextView) orderStatusItem.findViewById(R.id.product_title_textview);
                    product_title_textview.setText(product_title+"");

                    // 제품 기간 뿌리기
                    TextView period_textview = (TextView) orderStatusItem.findViewById(R.id.period_textview);
                    period_textview.setText(start_datetime.split(" ")[0]+" ~ "+end_datetime.split(" ")[0]+" ("+period_string+")");

                    // 결제한 가격 뿌리기
                    TextView price_textview = (TextView) orderStatusItem.findViewById(R.id.price_textview);
                    price_textview.setText(useFul.toNumFormat(pay_price)+"원");

                    items.add(orderStatusItem);
                }
                final ArrayList<LinearLayout> items2 = items;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i=0; i< items2.size(); i++) {
                            list_area.addView(items2.get(i));
                        }

                        index += view_num;
                        isLoading = false;
                        progress_bar.setVisibility(View.GONE);
                    }
                });


            } else {
                isLoading = false;
                progress_bar.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // 각 단계별 제품 수 가져오기
    public void mobile11003() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile11003");
        params.put("member", new ShareInfo(this).getMember());
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(11004);
        inLet.start();
    }

    // 각 단계별 제품 수 가져오기 후처리
    public void mobile11004(Object obj) {
        String data = obj.toString();

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");
            if (result.equals("ok")) {

                int index_0_num = jsonObject.getInt("index_0_num");
                int index_1_num = jsonObject.getInt("index_1_num");
                int index_2_num = jsonObject.getInt("index_2_num");
                int index_3_num = jsonObject.getInt("index_3_num");
                int index_4_num = jsonObject.getInt("index_4_num");

                num_textviews.get(0).setText(index_0_num+"");
                num_textviews.get(1).setText(index_1_num+"");
                num_textviews.get(2).setText(index_2_num+"");
                num_textviews.get(3).setText(index_3_num+"");
                num_textviews.get(4).setText(index_4_num+"");

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    View.OnClickListener orderStatusItemClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LinearLayout orderStatusItem = (LinearLayout) v;

            TextView vbp_textview = (TextView) orderStatusItem.findViewById(R.id.vbp_textview);
            int pk = Integer.parseInt(vbp_textview.getText().toString());

            // Log.i("MyTags", "선택한 pk = "+pk);
            Intent intent = new Intent(activity, Mobile06000.class);
            intent.putExtra("pk", pk);
            startActivity(intent);
        }
    };


    /*===================================================================================================================*/

    // 뒤로가기 할 시 경고 팝업창 띄우기
    public void callVelyBebeNormalDialog(String title, String content) {
        final VelyBebeNormalDialog dialog = new VelyBebeNormalDialog(this);
        dialog.show();
        dialog.startDialogAnimation();
        dialog.setDialogTitle(title);
        dialog.setDialogContent(content);
        dialog.buttonVisibleOptions(true, true);
        // dialog.setCancelButtonText("");
        // dialog.setConfirmButtonText("");
        dialog.setCancelButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setConfirmButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }



    boolean isFinish;
    // 통보 형식의 다이얼로그 호출
    public void callVelyBebeNormalDialog2(String title, String content) {
        final VelyBebeNormalDialog dialog = new VelyBebeNormalDialog(this);
        dialog.show();
        dialog.startDialogAnimation();
        dialog.setDialogTitle(title);
        dialog.setDialogContent(content);
        dialog.buttonVisibleOptions(false, true);


        // dialog.setCancelButtonText("");
        // dialog.setConfirmButtonText("");
        dialog.setCancelButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setConfirmButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (isFinish) {
                    finish();
                }
            }
        });
    }












    /*===================================================================================================================*/




    @Override
    public void onBackPressed() {
        super.onBackPressed();

        // callVelyBebeNormalDialog("안내", "정말 블리베베 앱을 종료하시겠습니까?"); // 뒤로가기 할 시 경고 팝업창 띄우기
    }
}
