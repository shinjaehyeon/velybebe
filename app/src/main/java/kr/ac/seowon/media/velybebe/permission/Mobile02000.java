package kr.ac.seowon.media.velybebe.permission;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.InLet;
import kr.ac.seowon.media.velybebe.common.PermissionCheck;
import kr.ac.seowon.media.velybebe.common.ShareInfo;
import kr.ac.seowon.media.velybebe.common.UseFul;
import kr.ac.seowon.media.velybebe.login.Mobile03000;
import kr.ac.seowon.media.velybebe.main.Mobile05000;

public class Mobile02000 extends AppCompatActivity {
    AppCompatActivity activity;
    Context context;

    /*===================================================================================================================*/

    ConstraintLayout best_parent_layout;

    Button okay_button;

    /*===================================================================================================================*/

    private static final int REQUEST_PERMISSION_CODE = 1;
    private static String[] PERMISSION_ALL = {
            Manifest.permission.INTERNET,
            Manifest.permission.CHANGE_WIFI_MULTICAST_STATE,
            Manifest.permission.CHANGE_WIFI_STATE,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.VIBRATE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static String[] PERMISSION_ALL_CHECK = {
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    /*===================================================================================================================*/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {

                default:

                    break;
            }
        }
    };

    /*===================================================================================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile02000);


        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        getIntentInfo(); // 인텐트 정보 받아오기

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기


    }

    @Override
    protected void onStart() {
        super.onStart();
        // Log.i("MyTags", "Main onStart 시작");
        new UseFul(activity).setAppRunning(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Log.i("MyTags", "Main onPause 시작");
        new UseFul(activity).setAppRunning(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Log.i("MyTags", "Main onResume 시작");
        new UseFul(activity).setAppRunning(1);
    }






    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(AppCompatActivity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // 인테트 정보 받아오기
    public void getIntentInfo() {

    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        best_parent_layout = (ConstraintLayout) findViewById(R.id.best_parent_layout);


        okay_button = (Button) findViewById(R.id.okay_button);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        okay_button.setOnClickListener(okay_button_event);
    }

    /*===================================================================================================================*/

    View.OnClickListener okay_button_event = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startPermission();
        }
    };

    /*===================================================================================================================*/

    public void startPermission() {
        try {
            if (!checkPermissions(activity, PERMISSION_ALL)) {
                // 하나라도 권한이 허용 되지 않은게 있다면
                requestAllPermissions(activity);
            } else {
                // 모든 권한이 허용되었다면

            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public boolean checkPermissions(Activity activity, String[] permission) throws IllegalAccessException {
        int[] grantlist = new int[permission.length];
        Log.i("MyTag", "permission.length = "+permission.length);
        for (int i=0; i<permission.length; i++) {
            grantlist[i] = ContextCompat.checkSelfPermission(activity, permission[i]);
        }
        for (int i=0; i<grantlist.length; i++) {
            Log.i("MyTag", "grantlist["+i+"] = "+grantlist[i]);
            if (grantlist[i] == PackageManager.PERMISSION_DENIED) {
                return false;
            }
        }
        return true;
    }

    public static void requestAllPermissions(Activity activity) {
        Log.i("MyTag", "requestAllPermissions() 실행");
        ActivityCompat.requestPermissions(activity, PERMISSION_ALL, REQUEST_PERMISSION_CODE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            try {
                if (checkPermissions(activity, PERMISSION_ALL_CHECK)) {
                    // 모든 권한 허락 요청 받음
                    new UseFul(activity).showToast("제시된 권한을 모두 허용하셨습니다.");

                    // 앱에 로그인되어 있는지 여부 확인
                    if (new ShareInfo(activity).getMember() != 0) {
                        // 로그인 되어 있으면
                        // 메인 페이지로 이동
                        goActivity(Mobile05000.class);

                    } else {
                        // 로그인 되어 있지 않으면
                        // 로그인 페이지로 이동
                        goActivity(Mobile03000.class);
                    }
                } else {
                    showRequestAgainDialog();
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void showRequestAgainDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("앱에 필요한 권한을 모두 허용해주셔야 이용 가능합니다. 설정에서 권한을 모두 허용해 주세요.");
        builder.setPositiveButton("설정바로가기", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                try {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).setData(Uri.parse("package:"+getPackageName()));
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                    Intent intent = new Intent(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
                    startActivity(intent);
                }
            }
        });
        builder.setNegativeButton("취소", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ActivityCompat.finishAffinity(activity);
            }
        });
        builder.create();
        builder.show();
    }


    public void goActivity(Class c) {
        Log.i("MyTags", "goActivity 실행");
        Intent intent = new Intent(activity, c);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    /*===================================================================================================================*/

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
