package kr.ac.seowon.media.velybebe.popularproductitemset;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.UrlImageGetDisplay;
import kr.ac.seowon.media.velybebe.common.UseFul;

public class PopularProductItem extends LinearLayout {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;
    View v;


    AppCompatImageView prodoctImageView;
    TextView product_title;
    TextView product_price;

    int pk;



    public PopularProductItem(Context context) {
        super(context);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }

    public PopularProductItem(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }

    public PopularProductItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }




    public void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.popular_product_item, this, false);
        addView(v);


        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectObjectAndId() {

        prodoctImageView = (AppCompatImageView) v.findViewById(R.id.prodoctImageView);
        product_title = (TextView) v.findViewById(R.id.product_title);
        product_price = (TextView) v.findViewById(R.id.product_price);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }


    public void setPk(final int pk) {
        this.pk = pk;
    }

    public int getPk() {
        return this.pk;
    }



    public void setImageUrl(String s) {
        UrlImageGetDisplay u = new UrlImageGetDisplay(activity);
        u.setImageUrl(s);
        u.setImageView(prodoctImageView);
        u.start();
    }

    public void setTitle(String s) {
        product_title.setText(s);
    }

    public void setPrice(String s) {
        product_price.setText(s);
    }

    public void setItemOnClickListener(View.OnClickListener e, PopularProductItem item){
        setTag(item);
        setOnClickListener(e);
    }



}
