package kr.ac.seowon.media.velybebe.rentalapply;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.InLet;
import kr.ac.seowon.media.velybebe.common.ShareInfo;
import kr.ac.seowon.media.velybebe.common.TopBar;
import kr.ac.seowon.media.velybebe.common.UrlImageGetDisplay;
import kr.ac.seowon.media.velybebe.common.UseFul;
import kr.ac.seowon.media.velybebe.common.VelyBebeNormalDialog;
import kr.ac.seowon.media.velybebe.rentaldetail.DetailImageFragment;
import kr.ac.seowon.media.velybebe.rentaldetail.TogetherViewProductItem;
import kr.ac.seowon.media.velybebe.signup.Mobile04200;

public class Mobile07000 extends AppCompatActivity {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;
    TopBar topBar;

    boolean isLoading = false;

    /*===================================================================================================================*/

    ConstraintLayout best_parent_layout;
    int product_pk;

    /*===================================================================================================================*/

    AppCompatImageView product_bg_blur_image;
    AppCompatImageView product_image_circle;

    /*===================================================================================================================*/

    TextView title_textview;

    /*===================================================================================================================*/

    LinearLayout price_list_box;

    /*===================================================================================================================*/

    Button post_find_button;

    EditText name_edittext;
    EditText post_edittext;
    EditText basic_addr_edittext;
    EditText detail_addr_edittext;

    ConstraintLayout daum_webview_box;
    WebView daum_webView;

    /*===================================================================================================================*/

    LinearLayout pay_way_list_box;

    /*===================================================================================================================*/

    TextView total_price_textview;

    /*===================================================================================================================*/

    Button rental_apply_button;

    /*===================================================================================================================*/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 07002: // 해당 제품 상세정보 가져오기 후처리
                    mobile07002(msg.obj);
                    break;
                case 07007: // 렌탈 신청 정보 저장하기 후처리
                    mobile07007(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };

    /*===================================================================================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile07000);


        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        getIntentInfo(); // 인텐트 정보 받아오기

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기


        viewPayWayList(); // 결제수단 리스트 뿌리기

        mobile07001(); // 해당 제품 상세정보 가져오기

    }

    @Override
    protected void onStart() {
        super.onStart();
        // Log.i("MyTags", "Main onStart 시작");
        new UseFul(activity).setAppRunning(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Log.i("MyTags", "Main onPause 시작");
        new UseFul(activity).setAppRunning(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Log.i("MyTags", "Main onResume 시작");
        new UseFul(activity).setAppRunning(1);
    }







    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(AppCompatActivity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // 인테트 정보 받아오기
    public void getIntentInfo() {
        product_pk = getIntent().getIntExtra("pk", 0);
    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        useFul = new UseFul(activity);
        best_parent_layout = (ConstraintLayout) findViewById(R.id.best_parent_layout);
        topBar = (TopBar) findViewById(R.id.topbar);

        /*===================================================================================================================*/

        product_bg_blur_image = (AppCompatImageView) findViewById(R.id.product_bg_blur_image);
        product_image_circle = (AppCompatImageView) findViewById(R.id.product_image_circle);

        /*===================================================================================================================*/

        title_textview = (TextView) findViewById(R.id.title_textview);

        /*===================================================================================================================*/

        price_list_box = (LinearLayout) findViewById(R.id.price_list_box);

        /*===================================================================================================================*/

        post_find_button = (Button) findViewById(R.id.post_find_button);

        name_edittext = (EditText) findViewById(R.id.name_edittext);
        post_edittext = (EditText) findViewById(R.id.post_edittext);
        basic_addr_edittext = (EditText) findViewById(R.id.basic_addr_edittext);
        detail_addr_edittext = (EditText) findViewById(R.id.detail_addr_edittext);

        daum_webview_box = (ConstraintLayout) findViewById(R.id.daum_webview_box);
        daum_webView = (WebView) findViewById(R.id.daum_webview);

        /*===================================================================================================================*/

        pay_way_list_box = (LinearLayout) findViewById(R.id.pay_way_list_box);

        /*===================================================================================================================*/

        total_price_textview = (TextView) findViewById(R.id.total_price_textview);


        rental_apply_button = (Button) findViewById(R.id.rental_apply_button);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {
        // topBar.setTitle("회원가입");
        // topBar.setMenuSideBar();
        topBar.hideMenuButton();
        topBar.showBackButton();

        product_image_circle.setBackground(new ShapeDrawable(new OvalShape()));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            product_image_circle.setClipToOutline(true);
        }
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        post_find_button.setOnClickListener(post_find_button_click_event);
        rental_apply_button.setOnClickListener(rental_apply_button_click_event);

        daum_webview_box.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                daum_webview_box.setVisibility(View.GONE);
                daum_webView.setVisibility(View.GONE);
            }
        });
    }

    // 렌탈신청하기 버튼 클릭시 이벤트
    View.OnClickListener rental_apply_button_click_event = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mobile07005()) { // 입력해야할 내용이 다 입력됬는지 확인하기
                mobile07006(); // 렌탈 신청 정보 저장하기
            }
        }
    };

    // 입력해야할 내용이 다 입력됬는지 확인하기

    int checked_price_pk;
    int checked_cure_price;
    String checked_name;
    String checked_post;
    String checked_basic_addr;
    String checked_detail_addr;
    int checked_pay_code;

    public boolean mobile07005() {
        // 가격이 선택 됬는지 확인하기
        int price_pk = 0;
        int cure_price = 0;
        for (int i=0; i<rentalPriceItems.size(); i++) {
            if (rentalPriceItems.get(i).isChecked()) {
                price_pk = rentalPriceItems.get(i).getPk();
                cure_price = rentalPriceItems.get(i).getPrice();
                break;
            }
        }
        if (price_pk == 0) {
            callVelyBebeNormalDialog2("안내", "원하시는 기간&가격을 선택해주세요.");
            return false;
        }
        checked_price_pk = price_pk;
        checked_cure_price = cure_price;


        // 주소가 입력 되었는지 확인하기
        String name = name_edittext.getText().toString().trim();
        if (name.equals("")) {
            callVelyBebeNormalDialog2("안내", "배송 받을 사람의 이름을 적어주세요.");
            return false;
        }
        checked_name = name;


        String post = post_edittext.getText().toString();
        if (post.equals("")) {
            callVelyBebeNormalDialog2("안내", "우편번호찾기를 통해 주소를 입력해주세요.");
            return false;
        }
        checked_post = post;

        String basic_addr = basic_addr_edittext.getText().toString();
        if (basic_addr.equals("")) {
            callVelyBebeNormalDialog2("안내", "우편번호찾기를 통해 주소를 입력해주세요.");
            return false;
        }
        checked_basic_addr = basic_addr;

        String detail_addr = detail_addr_edittext.getText().toString().trim();
        if (detail_addr.equals("")) {
            callVelyBebeNormalDialog2("안내", "나머지 상세주소를 입력해주세요.");
            return false;
        }
        checked_detail_addr = detail_addr;

        // 결제수단 확인하기
        int pay_code = 0;
        for (int i=0; i<payWayItems.size(); i++) {
            if (payWayItems.get(i).isChecked()) {
                pay_code = payWayItems.get(i).getPayCode();
                break;
            }
        }
        if (pay_code == 0) {
            callVelyBebeNormalDialog2("안내", "원하시는 결제수단을 선택해주세요.");
            return false;
        }
        checked_pay_code = pay_code;

        return true;
    }

    // 렌탈 신청 정보 저장하기
    public void mobile07006() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile07006");
        params.put("member", new ShareInfo(this).getMember());
        params.put("product_pk", product_pk);
        params.put("price_pk", checked_price_pk);
        params.put("cure_price", checked_cure_price);
        params.put("name", checked_name);
        params.put("post", checked_post);
        params.put("basic_addr", checked_basic_addr);
        params.put("detail_addr", checked_detail_addr);
        params.put("pay_code", checked_pay_code);


        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(07007);
        inLet.start();
    }

    boolean isFinish = false;

    // 렌탈 신청 정보 저장하기 후처리
    public void mobile07007(Object obj) {
        String data = obj.toString();
        Log.i("MyTags", "mobile07007 data = "+data);

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                isFinish = true;
                callVelyBebeNormalDialog2("안내", "렌탈신청이 완료되었습니다.");
            } else if (result.equals("zero")) {
                isFinish = true;
                callVelyBebeNormalDialog2("안내", "해당 제품 재고가 전부 소진되었습니다. 다음에 다시 주문해주세요!");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    ArrayList<PayWayItem> payWayItems = new ArrayList<PayWayItem>();
    int[] payCodes = {40004001, 40004002, 40004003, 40004004};
    String[] payCodesStr = {"페이코 간편결제 (PAYCO)", "신용카드/체크카드", "계좌이체", "휴대폰 소액결제"};
    public void viewPayWayList() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (int i=0; i<4; i++) {
                    PayWayItem item = new PayWayItem(activity);
                    item.setPayCode(payCodes[i]);
                    item.setPayString(payCodesStr[i]);
                    item.setItemOnClickListener(payWayItemListener, item);
                    payWayItems.add(item);

                    pay_way_list_box.addView(item);
                }
            }
        });
    }

    View.OnClickListener payWayItemListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            for (int i=0; i<payWayItems.size(); i++) {
                if (payWayItems.get(i).isChecked()) {
                    payWayItems.get(i).setInActive(); // 비활성화
                }
            }

            ((PayWayItem) v.getTag()).setActive(); // 활성화
        }
    };

    /*===================================================================================================================*/

    boolean isFirstAddrSearch = true;
    boolean isAddrSearchWindowVisible = false;

    // 우편번호 찾기 버튼 클릭시 이벤트
    View.OnClickListener post_find_button_click_event = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            isAddrSearchWindowVisible = true;

            if (isFirstAddrSearch) {
                init_webView();
                isFirstAddrSearch = false;
            }
            daum_webview_box.setVisibility(View.VISIBLE);
            daum_webView.setVisibility(View.VISIBLE);
        }
    };

    public void init_webView() {
        // JavaScript 허용
        daum_webView.getSettings().setJavaScriptEnabled(true);
        // JavaScript의 window.open 허용
        daum_webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        // JavaScript이벤트에 대응할 함수를 정의 한 클래스를 붙여줌
        daum_webView.addJavascriptInterface(new AndroidBridge(), "TestApp");
        // web client 를 chrome 으로 설정
        daum_webView.setWebChromeClient(new WebChromeClient());
        // webview url load. php 파일 주소
        daum_webView.loadUrl("http://jwisedom.co.kr/velybebe/daum_address.php");

    }

    private class AndroidBridge {
        @JavascriptInterface
        public void setAddress(final String arg1, final String arg2, final String arg3) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    daum_webview_box.setVisibility(View.GONE);
                    daum_webView.setVisibility(View.GONE);
                    isAddrSearchWindowVisible = false;


                    post_edittext.setText(arg1);

                    String basic_addr_string = arg2;
                    if (!arg3.equals("")) {
                        basic_addr_string += " ("+arg3+")";
                    }
                    basic_addr_edittext.setText(basic_addr_string);


                    // WebView를 초기화 하지않으면 재사용할 수 없음
                    init_webView();
                }
            });
        }
    }

    /*===================================================================================================================*/

    // 해당 제품 상세정보 가져오기
    public void mobile07001() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile07001");
        params.put("member", new ShareInfo(this).getMember());
        params.put("product_pk", product_pk);

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(07002);
        inLet.start();
    }

    ArrayList<RentalPriceItem> rentalPriceItems = new ArrayList<RentalPriceItem>();

    // 해당 제품 상세정보 가져오기 후처리
    public void mobile07002(Object obj) {
        String data = obj.toString();
        Log.i("MyTags", "mobile07002 data = "+data);

        isLoading = false;

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");
            if (result.equals("ok")) {
                String title = jsonObject.getString("title");
                String normal_image_url = jsonObject.getString("normal_image_url");

                // 이미지 뿌리기
                UrlImageGetDisplay u = new UrlImageGetDisplay(activity);
                u.setImageUrl(normal_image_url);
                u.setImageView(product_image_circle);
                u.start();

                UrlImageGetDisplay u2 = new UrlImageGetDisplay(activity);
                u2.setImageUrl(normal_image_url);
                u2.setImageView(product_bg_blur_image);
                u2.setBlur(true);
                u2.start();



                // 제품명 뿌리기
                title_textview.setText(title);



                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    int vbpp = jsonObject1.getInt("vbpp"); // 가격 pk
                    int period = jsonObject1.getInt("period");
                    String period_string = jsonObject1.getString("period_string");
                    int price = jsonObject1.getInt("price");


                    RentalPriceItem item = new RentalPriceItem(activity);
                    item.setPk(vbpp);
                    item.setPeriod(period_string);
                    item.setPrice(price);
                    item.setItemOnClickListener(rentalPriceItemOnClickEvent, item);

                    rentalPriceItems.add(item);
                }

                final ArrayList<RentalPriceItem> runRentalPriceItems = rentalPriceItems;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i=0; i<runRentalPriceItems.size(); i++) {
                            price_list_box.addView(runRentalPriceItems.get(i));
                        }
                    }
                });






            } else {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    View.OnClickListener rentalPriceItemOnClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            for (int i=0; i<rentalPriceItems.size(); i++) {
                if (rentalPriceItems.get(i).isChecked()) {
                    rentalPriceItems.get(i).setInActive(); // 비활성화
                }
            }

            ((RentalPriceItem) v.getTag()).setActive(); // 활성화
            int price = ((RentalPriceItem) v.getTag()).getPrice();

            // 총 가격 부분 여기서 작성하기
            total_price_textview.setText(useFul.toNumFormat(price)+" 원");
        }
    };

    /*===================================================================================================================*/

    // 뒤로가기 할 시 경고 팝업창 띄우기
    public void callVelyBebeNormalDialog(String title, String content) {
        final VelyBebeNormalDialog dialog = new VelyBebeNormalDialog(this);
        dialog.show();
        dialog.startDialogAnimation();
        dialog.setDialogTitle(title);
        dialog.setDialogContent(content);
        dialog.buttonVisibleOptions(true, true);
        // dialog.setCancelButtonText("");
        // dialog.setConfirmButtonText("");
        dialog.setCancelButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setConfirmButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    // 통보 형식의 다이얼로그 호출
    public void callVelyBebeNormalDialog2(String title, String content) {
        final VelyBebeNormalDialog dialog = new VelyBebeNormalDialog(this);
        dialog.show();
        dialog.startDialogAnimation();
        dialog.setDialogTitle(title);
        dialog.setDialogContent(content);
        dialog.buttonVisibleOptions(false, true);
        // dialog.setCancelButtonText("");
        // dialog.setConfirmButtonText("");
        dialog.setCancelButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setConfirmButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (isFinish) {
                    finish();
                }
            }
        });
    }



    /*===================================================================================================================*/





    @Override
    public void onBackPressed() {
        // super.onBackPressed();

        if (isAddrSearchWindowVisible) {
            daum_webview_box.setVisibility(View.GONE);
            daum_webView.setVisibility(View.GONE);
            isAddrSearchWindowVisible = false;
        } else {

            callVelyBebeNormalDialog("안내", "정말 결제 페이지를 떠나시겠습니까?");

        }
        // callVelyBebeNormalDialog("안내", "정말 블리베베 앱을 종료하시겠습니까?"); // 뒤로가기 할 시 경고 팝업창 띄우기
    }
}
