package kr.ac.seowon.media.velybebe.rentalapply;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.UseFul;

public class PayWayItem extends LinearLayout {
    View v;
    AppCompatActivity activity;
    Context context;
    UseFul useFul;

    /**************************************************************/

    int pay_code;
    String pay_str;
    boolean isChecked;

    Button button;
    TextView pay_textview;

    /**************************************************************/


    public void setPayCode(int n) {
        this.pay_code = n;
    }
    public int getPayCode() {
        return this.pay_code;
    }

    public void setPayString(String s) {

        this.pay_textview.setText(s);
    }

    public boolean isChecked() {
        return this.isChecked;
    }

    public void setActive() {
        isChecked = true;

        pay_textview.setTextColor(useFul.getRcolor(R.color.mobile07000_price_item_text_active_color));
        button.setBackgroundResource(R.drawable.ripple_button_type_12);
    }

    public void setInActive() {
        isChecked = false;

        pay_textview.setTextColor(useFul.getRcolor(R.color.mobile07000_price_item_text_inactive_color));
        button.setBackgroundResource(R.drawable.ripple_button_type_11);
    }

    public void setItemOnClickListener(View.OnClickListener e, PayWayItem item) {
        button.setTag(item);
        button.setOnClickListener(e);
    }





    public PayWayItem(Context context) {
        super(context);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }
    public PayWayItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }
    public PayWayItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }

    public void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.pay_way_item, this, false);
        addView(v);

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기


    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        button = (Button) v.findViewById(R.id.button);
        pay_textview = (TextView) v.findViewById(R.id.pay_textview);
    }


    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }
}
