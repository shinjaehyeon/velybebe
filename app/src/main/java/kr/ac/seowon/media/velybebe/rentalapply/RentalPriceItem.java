package kr.ac.seowon.media.velybebe.rentalapply;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.UseFul;

public class RentalPriceItem extends LinearLayout {
    View v;
    AppCompatActivity activity;
    Context context;
    UseFul useFul;


    private boolean isChecked;

    int price_pk;
    int price;

    Button button;
    TextView period_textview;
    TextView price_textview;


    public void setPk(int n) {
        this.price_pk = n;
    }
    public int getPk() {
        return this.price_pk;
    }

    public boolean isChecked() {
        return this.isChecked;
    }

    public void setPrice(int n) {
        this.price = n;
        String s = useFul.toNumFormat(n);
        price_textview.setText(s);
    }


    public int getPrice() {
        return this.price;
    }

    public void setPeriod(String s) {
        period_textview.setText(s);
    }

    public void setActive() {
        isChecked = true;

        period_textview.setTextColor(useFul.getRcolor(R.color.mobile07000_price_item_text_active_color));
        price_textview.setTextColor(useFul.getRcolor(R.color.mobile07000_price_item_text_active_color));
        button.setBackgroundResource(R.drawable.ripple_button_type_12);
    }

    public void setInActive() {
        isChecked = false;

        period_textview.setTextColor(useFul.getRcolor(R.color.mobile07000_price_item_text_inactive_color));
        price_textview.setTextColor(useFul.getRcolor(R.color.mobile07000_price_item_text_inactive_color));
        button.setBackgroundResource(R.drawable.ripple_button_type_11);
    }

    public void setItemOnClickListener(View.OnClickListener e, RentalPriceItem item) {
        button.setTag(item);
        button.setOnClickListener(e);
    }


    public RentalPriceItem(Context context) {
        super(context);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }
    public RentalPriceItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }
    public RentalPriceItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }

    public void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.rental_price_item, this, false);
        addView(v);

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기


    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        button = (Button) v.findViewById(R.id.button);
        period_textview = (TextView) v.findViewById(R.id.period_textview);
        price_textview = (TextView) v.findViewById(R.id.price_textview);
    }


    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }
}
