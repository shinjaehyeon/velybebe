package kr.ac.seowon.media.velybebe.rentaldetail;

import android.animation.ObjectAnimator;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daasuu.ei.Ease;
import com.daasuu.ei.EasingInterpolator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.InLet;
import kr.ac.seowon.media.velybebe.common.ShareInfo;
import kr.ac.seowon.media.velybebe.common.TopBar;
import kr.ac.seowon.media.velybebe.common.UrlImageGetDisplay;
import kr.ac.seowon.media.velybebe.common.UseFul;
import kr.ac.seowon.media.velybebe.common.VelyBebeNormalDialog;
import kr.ac.seowon.media.velybebe.main.Mobile05100;
import kr.ac.seowon.media.velybebe.main.Mobile05200;
import kr.ac.seowon.media.velybebe.main.Mobile05300;
import kr.ac.seowon.media.velybebe.main.Mobile05400;
import kr.ac.seowon.media.velybebe.popularproductitemset.PopularProductItem;
import kr.ac.seowon.media.velybebe.rentalapply.Mobile07000;

public class Mobile06000 extends AppCompatActivity {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;
    TopBar topBar;

    boolean isLoading = false;

    /*===================================================================================================================*/

    ConstraintLayout best_parent_layout;
    int product_pk;

    /*===================================================================================================================*/

    int image_total_num;
    ViewPager viewPager;
    ArrayList<DetailImageFragment> detailImages = new ArrayList<DetailImageFragment>();

    TextView current_image_page_num;
    TextView total_image_num;

    /*==================================================================================================================*/

    LinearLayout price_list_box;

    LinearLayout coupon_layout;
    LinearLayout gift_layout;
    LinearLayout sale_layout;

    TextView title_textivew;
    TextView lefted_stock_num_textview;

    /*==================================================================================================================*/

    LinearLayout together_view_prodoct_parent_box;
    LinearLayout together_view_prodoct_list_box;
    TextView together_no_result;
    ProgressBar together_progress_bar;

    /*===================================================================================================================*/

    AppCompatImageView detail_page_imageview;
    ProgressBar detail_image_progress_bar;

    /*===================================================================================================================*/

    ConstraintLayout heart_icon_box;
    AppCompatImageView heart_icon;
    Button heart_button;
    Button go_rental_button;

    /*===================================================================================================================*/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 05204: // 해당상품 좋아요 로그 남기기 후처리
                    mobile05204();
                    break;
                case 06002: // 렌탈 제품 상세 정보 가져오기 후처리
                    mobile06002(msg.obj); // 렌탈 제품 상세 정보 가져오기 후처리
                    break;
                case 06004: // 함께 본 상품 정보 가져오기
                    mobile06004(msg.obj); // 함께 본 상품 정보 가져오기 후처리
                    break;
                case 6006: // 재고 확인하기 후처리
                    mobile06006(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };

    /*===================================================================================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile06000);


        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        getIntentInfo(); // 인텐트 정보 받아오기

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기


        mobile06001();// 렌탈 제품 상세 정보 가져오기

        mobile06003(); // 함께 본 상품 정보 가져오기

    }

    @Override
    protected void onStart() {
        super.onStart();
        // Log.i("MyTags", "Main onStart 시작");
        new UseFul(activity).setAppRunning(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Log.i("MyTags", "Main onPause 시작");
        new UseFul(activity).setAppRunning(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Log.i("MyTags", "Main onResume 시작");
        mobile06005(); // 재고 확인하기
        new UseFul(activity).setAppRunning(1);
    }





    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(AppCompatActivity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // 인테트 정보 받아오기
    public void getIntentInfo() {
        product_pk = getIntent().getIntExtra("pk", 0);
    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        useFul = new UseFul(activity);
        best_parent_layout = (ConstraintLayout) findViewById(R.id.best_parent_layout);
        topBar = (TopBar) findViewById(R.id.topbar);

        /*===================================================================================================================*/

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        current_image_page_num = (TextView) findViewById(R.id.current_image_page_num);
        total_image_num = (TextView) findViewById(R.id.total_image_num);

        /*===================================================================================================================*/

        title_textivew = (TextView) findViewById(R.id.title);

        price_list_box = (LinearLayout) findViewById(R.id.price_list_box);
        coupon_layout = (LinearLayout) findViewById(R.id.coupon);
        gift_layout = (LinearLayout) findViewById(R.id.gift);
        sale_layout = (LinearLayout) findViewById(R.id.sale);

        lefted_stock_num_textview = (TextView) findViewById(R.id.lefted_stock_num_textview);

        /*===================================================================================================================*/

        together_view_prodoct_parent_box = (LinearLayout) findViewById(R.id.together_view_prodoct_parent_box);
        together_view_prodoct_list_box = (LinearLayout) findViewById(R.id.together_view_prodoct_list_box);
        together_no_result = (TextView) findViewById(R.id.together_no_result);
        together_progress_bar = (ProgressBar) findViewById(R.id.together_progress_bar);

        /*===================================================================================================================*/

        detail_page_imageview = (AppCompatImageView) findViewById(R.id.detail_page_imageview);
        detail_image_progress_bar = (ProgressBar) findViewById(R.id.detail_image_progress_bar);

        /*===================================================================================================================*/

        heart_icon_box = (ConstraintLayout) findViewById(R.id.heart_icon_box);
        heart_icon = (AppCompatImageView) findViewById(R.id.heart_icon);
        heart_button = (Button) findViewById(R.id.heart_button);
        go_rental_button = (Button) findViewById(R.id.go_rental_button);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {
        // topBar.setTitle("회원가입");
        // topBar.setMenuSideBar();
        topBar.hideMenuButton();
        topBar.showBackButton();


    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        heart_button.setOnClickListener(heart_button_click_event);
        go_rental_button.setOnClickListener(go_rental_button_click_event);
    }

    /*===================================================================================================================*/

    View.OnClickListener heart_button_click_event = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!isLoading) {
                if (heart_value == 0 || heart_value == 77771002) {
                    mobile05203(77771001);
                } else {
                    mobile05203(77771002);
                }


            } else {
                useFul.showToast("작업이 진행중입니다.");
            }
        }
    };

    View.OnClickListener go_rental_button_click_event = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(activity, Mobile07000.class);
            intent.putExtra("pk", product_pk);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
    };

    /*===================================================================================================================*/

    // 렌탈 제품 상세 정보 가져오기
    public void mobile06001() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile06001");
        params.put("member", new ShareInfo(this).getMember());
        params.put("product_pk", product_pk);

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(06002);
        inLet.start();
    }

    // 렌탈 제품 상세 정보 가져오기 후처리
    public void mobile06002(Object obj) {
        String data = obj.toString();
        Log.i("MyTags", "mobile06002 data = "+data);


        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {

                String title = jsonObject.getString("title");
                final String normal_image_url = jsonObject.getString("normal_image_url"); // 기본 사진 (필수)
                String image_url_1 = jsonObject.getString("image_url_1"); // 부가 사진 1 (선택)
                String image_url_2 = jsonObject.getString("image_url_2"); // 부가 사진 2 (선택)
                String image_url_3 = jsonObject.getString("image_url_3"); // 부가 사진 3 (선택)
                String image_url_4 = jsonObject.getString("image_url_4"); // 부가 사진 4 (선택)
                final String detail_image_url = jsonObject.getString("detail_image_url"); // 상세페이지 (필수)
                String price_string = jsonObject.getString("price_string");
                String[] price_strings = price_string.split("   ");
                int coupon = jsonObject.getInt("coupon");
                int sale = jsonObject.getInt("sale");
                int gift = jsonObject.getInt("gift");
                String heart_value_str = jsonObject.getString("heart_value");
                int heart_value = 0;
                if (heart_value_str.equals("")) {
                    heart_value = 0;
                } else {
                    heart_value = Integer.parseInt(heart_value_str);
                }
                int lefted_stock_num = jsonObject.getInt("lefted_stock_num"); // 남은 재고수




                for (int i=0; i<price_strings.length; i++) {
                    String temp_period = price_strings[i].split(" - ")[0];
                    String temp_price = price_strings[i].split(" - ")[1];
                    int temp_price_int = Integer.parseInt(temp_price.trim());

                    price_strings[i] = temp_period + " - "+ useFul.toNumFormat(temp_price_int)+"원";
                }
                final String[] real_price_strings = price_strings;




                // 이미지 뿌리기
                if (!normal_image_url.equals("")) {
                    image_total_num++;
                    DetailImageFragment detailImageFragment = new DetailImageFragment(activity);
                    detailImageFragment.setImageUrl(normal_image_url);
                    detailImages.add(detailImageFragment);
                }
                if (!image_url_1.equals("")) {
                    image_total_num++;
                    DetailImageFragment detailImageFragment = new DetailImageFragment(activity);
                    detailImageFragment.setImageUrl(image_url_1);
                    detailImages.add(detailImageFragment);
                }
                if (!image_url_2.equals("")) {
                    image_total_num++;
                    DetailImageFragment detailImageFragment = new DetailImageFragment(activity);
                    detailImageFragment.setImageUrl(image_url_2);
                    detailImages.add(detailImageFragment);
                }
                if (!image_url_3.equals("")) {
                    image_total_num++;
                    DetailImageFragment detailImageFragment = new DetailImageFragment(activity);
                    detailImageFragment.setImageUrl(image_url_3);
                    detailImages.add(detailImageFragment);
                }
                if (!image_url_4.equals("")) {
                    image_total_num++;
                    DetailImageFragment detailImageFragment = new DetailImageFragment(activity);
                    detailImageFragment.setImageUrl(image_url_4);
                    detailImages.add(detailImageFragment);
                }

                total_image_num.setText(image_total_num+"");

                viewPager.setAdapter(new ViewPagerAdapter(activity.getSupportFragmentManager()));
                viewPager.setCurrentItem(0);
                viewPager.setOffscreenPageLimit(3);


                // 제품명 뿌리기
                title_textivew.setText(title);


                // 가격 정보 뿌리기
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        price_list_box.removeAllViews();

                        for (int i=0; i<real_price_strings.length; i++) {
                            LayoutInflater inflater = (LayoutInflater) getSystemService( Context.LAYOUT_INFLATER_SERVICE);
                            TextView textView = (TextView) inflater.inflate(R.layout.rental_detail_price_list_textview, null);
                            textView.setText(real_price_strings[i]);
                            price_list_box.addView(textView);
                        }
                    }
                });



                // 쿠폰, 증정, 세일 여부 확인
                if (coupon != 90001001) {
                    coupon_layout.setVisibility(View.VISIBLE);
                } else {
                    coupon_layout.setVisibility(View.GONE);
                }
                if (sale != 90001001) {
                    sale_layout.setVisibility(View.VISIBLE);
                } else {
                    sale_layout.setVisibility(View.GONE);
                }
                if (gift != 90001001) {
                    gift_layout.setVisibility(View.VISIBLE);
                } else {
                    gift_layout.setVisibility(View.GONE);
                }


                // 상세페이지 이미지 뿌리기
                if (!detail_image_url.equals("")) {
                    Log.i("MyTags", "상세이미지 뿌리기 시작!");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            UrlImageGetDisplay u = new UrlImageGetDisplay(activity);
                            u.setProgressBar(detail_image_progress_bar);
                            u.setImageUrl(detail_image_url);
                            u.setImageView(detail_page_imageview);
                            u.start();
                        }
                    });
                }



                // 하트 아이콘 여부
                if (heart_value == 0 || heart_value == 77771002) {
                    this.heart_value = 77771002;
                    heart_icon.setImageResource(R.drawable.ic_mobile05200_heart_off);
                } else {
                    this.heart_value = 77771001;
                    heart_icon.setImageResource(R.drawable.ic_mobile05200_heart_on);
                }




                lefted_stock_num_textview.setText("남은 재고수 : "+lefted_stock_num);

                // 재고수가 0이면 구매버튼 비활성화 하기
                if (lefted_stock_num == 0) {
                    go_rental_button.setBackgroundResource(R.drawable.ripple_button_type_3);
                    go_rental_button.setText("재고 소진");
                    go_rental_button.setOnClickListener(null);
                }


            } else {

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    // 함께 본 상품 정보 가져오기
    public void mobile06003() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile06003");
        params.put("member", new ShareInfo(this).getMember());
        params.put("product_pk", product_pk);

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(06004);
        inLet.start();
    }

    // 함께 본 상품 정보 가져오기 후처리
    public void mobile06004(Object obj) {

        String data = obj.toString();
        Log.i("MyTags", "together data = "+data);


        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                int total_num = jsonObject.getInt("total_num");
                if (total_num >= 1) {
                    together_no_result.setVisibility(View.GONE);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                    ArrayList<TogetherViewProductItem> temp_items = new ArrayList<TogetherViewProductItem>();

                    for (int i=0; i<jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                        int product_pk = jsonObject1.getInt("product_pk");
                        String normal_image_url = jsonObject1.getString("normal_image_url");
                        String title = jsonObject1.getString("title");
                        String period_string = jsonObject1.getString("period_string");
                        int price = jsonObject1.getInt("price");

                        // 함께 본 상품 리스트 아이템 레이아웃 만들고 와서 다시 여기로!
                        TogetherViewProductItem item = new TogetherViewProductItem(activity);
                        item.setPk(product_pk);
                        item.setImageUrl(normal_image_url);
                        item.setProductTitle(title);
                        item.setPriceString(period_string+" - "+useFul.toNumFormat(price));
                        temp_items.add(item);
                    }

                    final ArrayList<TogetherViewProductItem> items = temp_items;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            for (int i=0; i<items.size(); i++) {
                                together_view_prodoct_list_box.addView(items.get(i));
                            }
                            together_progress_bar.setVisibility(View.GONE);
                        }
                    });




                } else {
                    together_no_result.setVisibility(View.VISIBLE);
                    together_progress_bar.setVisibility(View.GONE);
                }
            } else {
                together_no_result.setVisibility(View.VISIBLE);
                together_progress_bar.setVisibility(View.GONE);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    int heart_value;

    // 재고 확인하기
    public void mobile06005() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile06005");
        params.put("member", new ShareInfo(this).getMember());
        params.put("product_pk", product_pk);

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(6006);
        inLet.start();
    }

    // 재고 확인하기 후처리
    public void mobile06006(Object obj) {
        String data = obj.toString();
        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");


            if (result.equals("ok")) {
                int leftef_stock_num = jsonObject.getInt("leftef_stock_num");
                lefted_stock_num_textview.setText("남은 재고수 : "+leftef_stock_num);

                if (leftef_stock_num == 0) {
                    go_rental_button.setBackgroundResource(R.drawable.ripple_button_type_3);
                    go_rental_button.setText("재고 소진");
                    go_rental_button.setOnClickListener(null);
                }
            } else {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }




    // 해당상품 좋아요 로그 남기기
    public void mobile05203(int heartLog) {
        isLoading = true;
        heart_value = heartLog;

        ContentValues params = new ContentValues();
        params.put("act", "mobile05203");
        params.put("member", new ShareInfo(activity).getMember());
        params.put("product_pk", product_pk);
        params.put("log", heartLog);
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(05204);
        inLet.start();
    }

    // 해당상품 좋아요 로그 남기기 후처리
    public void mobile05204() {
        isLoading = false;

        if (heart_value == 77771001) {
            heart_icon.setImageResource(R.drawable.ic_mobile05200_heart_on);
            useFul.showToast("해당 상품을 좋아요 등록하셨습니다.");
        } else {
            heart_icon.setImageResource(R.drawable.ic_mobile05200_heart_off);
            useFul.showToast("해당 상품을 좋아요 해제하셨습니다.");
        }
    }




    private class ViewPagerAdapter extends FragmentPagerAdapter {
        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            activeEffect(position);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            // super.destroyItem(container, position, object);
        }

        @Override
        public Fragment getItem(int position) {
            return detailImages.get(position);
        }
        @Override
        public int getCount() {
            return image_total_num;
        }
    }

    public void activeEffect(int n) {
        current_image_page_num.setText((n+1)+"");
    }


    /*===================================================================================================================*/

    // 뒤로가기 할 시 경고 팝업창 띄우기
    public void callVelyBebeNormalDialog(String title, String content) {
        final VelyBebeNormalDialog dialog = new VelyBebeNormalDialog(this);
        dialog.show();
        dialog.startDialogAnimation();
        dialog.setDialogTitle(title);
        dialog.setDialogContent(content);
        dialog.buttonVisibleOptions(true, true);
        // dialog.setCancelButtonText("");
        // dialog.setConfirmButtonText("");
        dialog.setCancelButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setConfirmButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    // 통보 형식의 다이얼로그 호출
    public void callVelyBebeNormalDialog2(String title, String content) {
        final VelyBebeNormalDialog dialog = new VelyBebeNormalDialog(this);
        dialog.show();
        dialog.startDialogAnimation();
        dialog.setDialogTitle(title);
        dialog.setDialogContent(content);
        dialog.buttonVisibleOptions(false, true);
        // dialog.setCancelButtonText("");
        // dialog.setConfirmButtonText("");
        dialog.setCancelButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setConfirmButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }



    /*===================================================================================================================*/


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        // callVelyBebeNormalDialog("안내", "정말 블리베베 앱을 종료하시겠습니까?"); // 뒤로가기 할 시 경고 팝업창 띄우기
    }
}
