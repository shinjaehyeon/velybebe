package kr.ac.seowon.media.velybebe.rentaldetail;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.UrlImageGetDisplay;
import kr.ac.seowon.media.velybebe.common.UseFul;

public class TogetherViewProductItem extends LinearLayout {
    View v;
    AppCompatActivity activity;
    Context context;
    UseFul useFul;


    int pk;

    AppCompatImageView imageview;
    TextView title;
    TextView price_str_textview;



    public TogetherViewProductItem(Context context) {
        super(context);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }
    public TogetherViewProductItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }
    public TogetherViewProductItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }


    public void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.together_view_product_item, this, false);
        addView(v);

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기

    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        imageview = (AppCompatImageView) v.findViewById(R.id.imageview);
        title = (TextView) v.findViewById(R.id.title);
        price_str_textview = (TextView) v.findViewById(R.id.price_str_textview);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }




    public void setImageUrl(String s) {
        UrlImageGetDisplay u = new UrlImageGetDisplay(activity);
        u.setImageUrl(s);
        u.setImageView(imageview);
        u.start();
    }

    public void setProductTitle(String s) {
        title.setText(s);
    }

    public void setPriceString(String s) {
        price_str_textview.setText(s);
    }

    public void setPk(int n) {
        this.pk = n;
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, Mobile06000.class);
                intent.putExtra("pk", pk);
                activity.startActivity(intent);
            }
        });
    }

}
