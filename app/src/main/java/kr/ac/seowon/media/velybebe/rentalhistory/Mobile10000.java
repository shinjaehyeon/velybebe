package kr.ac.seowon.media.velybebe.rentalhistory;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daasuu.ei.Ease;
import com.daasuu.ei.EasingInterpolator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.InLet;
import kr.ac.seowon.media.velybebe.common.RealPathUtil;
import kr.ac.seowon.media.velybebe.common.ShareInfo;
import kr.ac.seowon.media.velybebe.common.TopBar;
import kr.ac.seowon.media.velybebe.common.UrlImageGetDisplay;
import kr.ac.seowon.media.velybebe.common.UseFul;
import kr.ac.seowon.media.velybebe.common.VelyBebeNormalDialog;
import kr.ac.seowon.media.velybebe.rentaldetail.Mobile06000;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Mobile10000 extends AppCompatActivity {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;
    TopBar topBar;

    static final int GET_PICTURE_URI = 1;
    String mCurrentPhotoPath;

    boolean isLoading = false;
    boolean isReviewWindowVisibile = false;

    /*===================================================================================================================*/

    ConstraintLayout best_parent_layout;

    /*===================================================================================================================*/

    LinearLayout tab_index_0_display;
    LinearLayout tab_index_1_display;
    LinearLayout ongoing_rental_list_box;
    LinearLayout terminated_rental_list_box;
    LinearLayout review_write_bg_black;

    ConstraintLayout review_write_window;

    EditText review_content_edittext;

    ArrayList<AppCompatImageView> starButtons = new ArrayList<AppCompatImageView>();
    ArrayList<AppCompatImageView> attach_images = new ArrayList<AppCompatImageView>();

    ArrayList<Button> attach_image_buttons = new ArrayList<Button>();
    ArrayList<Button> attach_image_delete_buttons = new ArrayList<Button>();
    Button tab_button_index_0;
    Button tab_button_index_1;
    Button ongoing_rental_more_view_button;
    Button terminated_rental_more_view_button;
    Button write_cancel_button;
    Button write_complete_button;

    ProgressBar ongoing_rental_progressbar;
    ProgressBar terminated_rental_progressbar;

    ArrayList<String> attach_image_real_paths = new ArrayList<String>();

    int index0 = 0;
    int view_num0 = 6;
    int index1 = 0;
    int view_num1 = 6;

    int temp_num = 0;


    int review_product_pk;
    int review_product_star_num;
    int selectAttachButtonIndex;

    String review_product_content;


    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 10002: // 진행중 또는 종료된 렌탈 리스트 가져오기 후처리
                    mobile10002(msg.obj);
                    break;

                default:

                    break;
            }
        }
    };

    /*===================================================================================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile10000);


        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        getIntentInfo(); // 인텐트 정보 받아오기

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기


        mobile10001(0); // 진행중 렌탈 리스트 가져오기


    }

    @Override
    protected void onStart() {
        super.onStart();
        // Log.i("MyTags", "Main onStart 시작");
        new UseFul(activity).setAppRunning(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Log.i("MyTags", "Main onPause 시작");
        new UseFul(activity).setAppRunning(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Log.i("MyTags", "Main onResume 시작");
        new UseFul(activity).setAppRunning(1);
    }





    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(AppCompatActivity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // 인테트 정보 받아오기
    public void getIntentInfo() {

    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        useFul = new UseFul(activity);
        best_parent_layout = (ConstraintLayout) findViewById(R.id.best_parent_layout);
        topBar = (TopBar) findViewById(R.id.topbar);

        /*===================================================================================================================*/

        tab_index_0_display = (LinearLayout) findViewById(R.id.tab_index_0_display);
        tab_index_1_display = (LinearLayout) findViewById(R.id.tab_index_1_display);
        ongoing_rental_list_box = (LinearLayout) findViewById(R.id.ongoing_rental_list_box);
        terminated_rental_list_box = (LinearLayout) findViewById(R.id.terminated_rental_list_box);
        review_write_bg_black = (LinearLayout) findViewById(R.id.review_write_bg_black);
        review_write_window = (ConstraintLayout) findViewById(R.id.review_write_window);

        review_content_edittext = (EditText) findViewById(R.id.review_content_edittext);

        starButtons.add((AppCompatImageView) findViewById(R.id.star_0_imageview));
        starButtons.add((AppCompatImageView) findViewById(R.id.star_1_imageview));
        starButtons.add((AppCompatImageView) findViewById(R.id.star_2_imageview));
        starButtons.add((AppCompatImageView) findViewById(R.id.star_3_imageview));
        starButtons.add((AppCompatImageView) findViewById(R.id.star_4_imageview));
        attach_images.add((AppCompatImageView) findViewById(R.id.attach_image_0));
        attach_images.add((AppCompatImageView) findViewById(R.id.attach_image_1));
        attach_images.add((AppCompatImageView) findViewById(R.id.attach_image_2));
        attach_images.add((AppCompatImageView) findViewById(R.id.attach_image_3));

        attach_image_real_paths.add("");
        attach_image_real_paths.add("");
        attach_image_real_paths.add("");
        attach_image_real_paths.add("");

        attach_image_buttons.add((Button) findViewById(R.id.attach_image_0_button));
        attach_image_buttons.add((Button) findViewById(R.id.attach_image_1_button));
        attach_image_buttons.add((Button) findViewById(R.id.attach_image_2_button));
        attach_image_buttons.add((Button) findViewById(R.id.attach_image_3_button));
        attach_image_delete_buttons.add((Button) findViewById(R.id.attach_image_0_delete_button));
        attach_image_delete_buttons.add((Button) findViewById(R.id.attach_image_1_delete_button));
        attach_image_delete_buttons.add((Button) findViewById(R.id.attach_image_2_delete_button));
        attach_image_delete_buttons.add((Button) findViewById(R.id.attach_image_3_delete_button));
        tab_button_index_0 = (Button) findViewById(R.id.tab_button_index_0);
        tab_button_index_1 = (Button) findViewById(R.id.tab_button_index_1);
        ongoing_rental_more_view_button = (Button) findViewById(R.id.ongoing_rental_more_view_button);
        terminated_rental_more_view_button = (Button) findViewById(R.id.terminated_rental_more_view_button);
        write_cancel_button = (Button) findViewById(R.id.write_cancel_button);
        write_complete_button = (Button) findViewById(R.id.write_complete_button);

        ongoing_rental_progressbar = (ProgressBar) findViewById(R.id.ongoing_rental_progressbar);
        terminated_rental_progressbar = (ProgressBar) findViewById(R.id.terminated_rental_progressbar);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {
        // topBar.setTitle("회원가입");
        // topBar.setMenuSideBar();
        topBar.hideMenuButton();
        topBar.showBackButton();


    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        ongoing_rental_more_view_button.setOnClickListener(moveViewButtonClickEvent);
        terminated_rental_more_view_button.setOnClickListener(moveViewButtonClickEvent);

        tab_button_index_0.setOnClickListener(tabButtonClickEvent);
        tab_button_index_1.setOnClickListener(tabButtonClickEvent);

        for (int i=0; i<starButtons.size(); i++) {
            starButtons.get(i).setOnClickListener(starButtonsClickEvent);
        }

        for (int i=0; i<attach_image_buttons.size(); i++) {
            attach_image_buttons.get(i).setOnClickListener(attach_image_buttons_click_event);
            attach_image_delete_buttons.get(i).setOnClickListener(attach_image_delete_buttons_click_event);
        }


        write_cancel_button.setOnClickListener(write_chain_button_click_event);
        write_complete_button.setOnClickListener(write_chain_button_click_event);
    }





    /*===================================================================================================================*/

    // 진행중 또는 종료된 렌탈 리스트 가져오기
    public void mobile10001(int status) {
        isLoading = true;

        if (status == 40003006) {
            terminated_rental_progressbar.setVisibility(View.VISIBLE);
        } else {
            ongoing_rental_progressbar.setVisibility(View.VISIBLE);
        }

        ContentValues params = new ContentValues();
        params.put("act", "mobile10001");
        params.put("member", new ShareInfo(this).getMember());
        if (status == 40003006) {
            params.put("index", index1);
            params.put("view_num", view_num1);
        } else {
            params.put("index", index0);
            params.put("view_num", view_num0);
        }
        params.put("status", status);

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(10002);
        inLet.start();
    }

    // 진행중 또는 종료된 렌탈 리스트 가져오기 후처리
    public void mobile10002(Object obj) {
        String data = obj.toString();
        Log.i("MyTags", "mobile10002 data = "+data);

        isLoading = false;

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");
            final int status = jsonObject.getInt("status");

            if (result.equals("ok")) {
                ArrayList<LinearLayout> items = new ArrayList<>();
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    int rental = jsonObject1.getInt("rental");
                    int vbp = jsonObject1.getInt("vbp");
                    String product_image_url = jsonObject1.getString("product_image_url");
                    String product_title = jsonObject1.getString("product_title");
                    int price_pk = jsonObject1.getInt("price_pk");
                    String rental_period = jsonObject1.getString("rental_period");
                    String start_datetime = jsonObject1.getString("start_datetime");
                    String end_datetime = jsonObject1.getString("end_datetime");
                    int cure_price = jsonObject1.getInt("cure_price");
                    int average_star_num = jsonObject1.getInt("average_star_num");


                    LayoutInflater inflater = (LayoutInflater) getSystemService( Context.LAYOUT_INFLATER_SERVICE);
                    LinearLayout ongoingRentalItem = (LinearLayout) inflater.inflate(R.layout.ongoing_rental_item, null);

                    // 제품 고유번호 넣기
                    TextView vbp_textview = (TextView) ongoingRentalItem.findViewById(R.id.vbp_textview);
                    vbp_textview.setText(vbp+"");

                    // 이미지 넣기
                    AppCompatImageView product_imageview = (AppCompatImageView) ongoingRentalItem.findViewById(R.id.product_imageview);
                    UrlImageGetDisplay u = new UrlImageGetDisplay(activity);
                    u.setImageUrl(product_image_url);
                    u.setImageView(product_imageview);
                    u.start();

                    // 제품 이름 넣기
                    TextView product_title_textview = (TextView) ongoingRentalItem.findViewById(R.id.product_title_textview);
                    product_title_textview.setText(product_title);

                    // 날짜 기간 표시
                    String start_small_date = start_datetime.split(" ")[0];
                    String end_small_date = end_datetime.split(" ")[0];
                    TextView period_textview = (TextView) ongoingRentalItem.findViewById(R.id.period_textview);
                    period_textview.setText(start_small_date+" ~ "+end_small_date+" ("+rental_period+")");

                    // 가격 표시
                    TextView price_textview = (TextView) ongoingRentalItem.findViewById(R.id.price_textview);
                    price_textview.setText(useFul.toNumFormat(cure_price)+"원");

                    // 별점 표시
                    ArrayList<AppCompatImageView> star_imageviews = new ArrayList<AppCompatImageView>();
                    star_imageviews.add((AppCompatImageView) ongoingRentalItem.findViewById(R.id.star_1_imageview));
                    star_imageviews.add((AppCompatImageView) ongoingRentalItem.findViewById(R.id.star_2_imageview));
                    star_imageviews.add((AppCompatImageView) ongoingRentalItem.findViewById(R.id.star_3_imageview));
                    star_imageviews.add((AppCompatImageView) ongoingRentalItem.findViewById(R.id.star_4_imageview));
                    star_imageviews.add((AppCompatImageView) ongoingRentalItem.findViewById(R.id.star_5_imageview));
                    for (int ii=0; ii<average_star_num; ii++) {
                        star_imageviews.get(ii).setImageResource(R.drawable.mobile00000_start_active_icon);
                    }

                    // 버튼 표시
                    Button button1 = (Button) ongoingRentalItem.findViewById(R.id.button1);
                    Button button2 = (Button) ongoingRentalItem.findViewById(R.id.button2);

                    button1.setTag(vbp+""); // 제품상세보기 버튼에는 제품 고유번호를 태그로 달기
                    button1.setOnClickListener(productDetailViewButtonClickEvent);

                    // 요청받은 status 값에 따라 두번째 버튼 역할이 달라짐
                    button2.setTag(vbp);
                    if (status == 40003006) {
                        // 종료된 렌탈일 경우
                        button2.setText("리뷰 작성");
                        button2.setOnClickListener(reviewWriteButtonClickEvent);
                    } else {
                        // 진행중 렌탈일 경우
                        button2.setText("리뷰 보러가기");
                        button2.setOnClickListener(reviewViewButtonClickEvent);
                    }

                    items.add(ongoingRentalItem);
                }

                final ArrayList<LinearLayout> items2 = items;

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i=0; i<items2.size(); i++) {
                            if (status == 40003006) {
                                terminated_rental_list_box.addView(items2.get(i));
                            } else {
                                ongoing_rental_list_box.addView(items2.get(i));
                            }
                        }

                        if (status == 40003006) {
                            index1 += view_num1;
                            terminated_rental_progressbar.setVisibility(View.GONE);
                        } else {
                            index0 += view_num0;
                            ongoing_rental_progressbar.setVisibility(View.GONE);
                        }

                        isLoading = false;
                    }
                });



            } else {
                isLoading = false;
                useFul.showToast("데이터가 없습니다.");
                if (status == 40003006) {
                    terminated_rental_progressbar.setVisibility(View.GONE);
                } else {
                    ongoing_rental_progressbar.setVisibility(View.GONE);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }




    // 제품 상세보기 버튼 클릭했을 경우
    View.OnClickListener productDetailViewButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int product_pk = Integer.parseInt(v.getTag().toString());
            // Log.i("MyTags", "(제품 상세보기) 선택한 제품 pk = "+product_pk);

            Intent intent = new Intent(activity, Mobile06000.class);
            intent.putExtra("pk", product_pk);
            startActivity(intent);
        }
    };

    // 리뷰 보러가기 버튼 클릭했을 경우
    View.OnClickListener reviewViewButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int product_pk = Integer.parseInt(v.getTag().toString());
            Log.i("MyTags", "(리뷰 보러가기) 선택한 제품 pk = "+product_pk);
        }
    };

    // 리뷰 작성 버튼 클릭했을 경우
    View.OnClickListener reviewWriteButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int product_pk = Integer.parseInt(v.getTag().toString());
            Log.i("MyTags", "(리뷰 작성) 선택한 제품 pk = "+product_pk);


            review_product_pk = product_pk;

            showReviewWindow();
            // hideReviewWindow();

        }
    };

    // 더보기 버튼을 클릭했을 때
    View.OnClickListener moveViewButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ongoing_rental_more_view_button: // 진행중 렌탈 더보기 버튼 클릭시
                    mobile10001(0); // 진행중 렌탈 리스트 가져오기
                    break;
                case R.id.terminated_rental_more_view_button: // 종료된 렌탈 더보기 버튼 클릭시
                    mobile10001(40003006); // 종료된 렌탈 리스트 가져오기
                    break;
                default:

                    break;
            }
        }
    };

    // 진행중 렌탈, 종료된 렌탈 탭 버튼 클릭했을 시 이벤트
    View.OnClickListener tabButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tab_button_index_0: // 진행중 렌탈 버튼 클릭시
                    tab_button_index_0.setBackgroundResource(R.drawable.ripple_button_type_2);
                    tab_button_index_0.setTextColor(Color.parseColor("#ffffff"));
                    tab_button_index_1.setBackgroundResource(R.drawable.ripple_button_type_6);
                    tab_button_index_1.setTextColor(Color.parseColor("#999999"));

                    tab_index_0_display.setVisibility(View.VISIBLE);
                    tab_index_1_display.setVisibility(View.GONE);
                    break;
                case R.id.tab_button_index_1: // 종료된 렌탈 버튼 클릭시
                    if (temp_num == 0) {
                        mobile10001(40003006); // 종료된 렌탈 리스트 가져오기
                        temp_num++;
                    }
                    tab_button_index_0.setBackgroundResource(R.drawable.ripple_button_type_6);
                    tab_button_index_0.setTextColor(Color.parseColor("#999999"));
                    tab_button_index_1.setBackgroundResource(R.drawable.ripple_button_type_2);
                    tab_button_index_1.setTextColor(Color.parseColor("#ffffff"));

                    tab_index_0_display.setVisibility(View.GONE);
                    tab_index_1_display.setVisibility(View.VISIBLE);
                    break;
                default:

                    break;
            }
        }
    };

    // 별점 클릭했을 시 이벤트
    View.OnClickListener starButtonsClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int starIndex = Integer.parseInt(v.getTag().toString());
            review_product_star_num = starIndex + 1;

            for (int i=0; i<starButtons.size(); i++) {
                if (i <= starIndex) {
                    starButtons.get(i).setImageResource(R.drawable.mobile00000_start_active_icon);
                } else {
                    starButtons.get(i).setImageResource(R.drawable.mobile00000_start_inactive_icon);
                }
            }
        }
    };

    // 사진 선택 버튼 클릭 시 이벤트
    View.OnClickListener attach_image_buttons_click_event = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int index = Integer.parseInt(v.getTag().toString());

            selectAttachButtonIndex = index;
            if (selectAttachButtonIndex != 0) {
                if (attach_image_real_paths.get(selectAttachButtonIndex-1).equals("")) {
                    callVelyBebeNormalDialog2("안내", "이미지를 좌측에서 순서대로 업로드해주세요.");
                } else {
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
                    intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, GET_PICTURE_URI);
                }
            } else {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
                intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, GET_PICTURE_URI);
            }


        }
    };

    // 첨부 이미지 삭제 버튼 클릭시 이벤트
    View.OnClickListener attach_image_delete_buttons_click_event = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int index = Integer.parseInt(v.getTag().toString());
            attach_image_real_paths.set(index, "");
            attach_images.get(index).setImageBitmap(null);
        }
    };

    // 작성취소, 작성완료 버튼 클릭시 이벤트
    View.OnClickListener write_chain_button_click_event = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.write_cancel_button: // 작성취소
                    callVelyBebeNormalDialog3("경고", "작성을 취소하시면 작성했던 내용이 모두 초기화 됩니다. 취소하시겠습니까?");
                    break;
                case R.id.write_complete_button: // 작성완료
                    checkReview(); // 리뷰 내용이 잘 입력되었는지 체크하기
                    break;
            }
        }
    };



    // 리뷰 창 열기
    public void showReviewWindow() {
        isReviewWindowVisibile = true;

        // 배경 레이아웃
        review_write_bg_black.setVisibility(View.VISIBLE);
        ObjectAnimator animator2 = ObjectAnimator.ofFloat(review_write_bg_black, "alpha", 0f, 1f);
        animator2.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT)); // github에서 라이브러리 가져와야 함.
        animator2.setDuration(600);
        animator2.start();

        // 리뷰 창
        review_write_window.setVisibility(View.VISIBLE);
        ObjectAnimator animator3 = ObjectAnimator.ofFloat(review_write_window, "alpha", 0f, 1f);
        animator3.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT)); // github에서 라이브러리 가져와야 함.
        animator3.setDuration(600);
        animator3.start();

        ObjectAnimator animator4 = ObjectAnimator.ofFloat(review_write_window, "translationY", 300f, 0f);
        animator4.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT)); // github에서 라이브러리 가져와야 함.
        animator4.setDuration(600);
        animator4.start();
    }

    // 리뷰 창 닫기
    public void hideReviewWindow() {
        isReviewWindowVisibile = false;

        // 배경 레이아웃
        review_write_bg_black.setVisibility(View.VISIBLE);
        ObjectAnimator animator2 = ObjectAnimator.ofFloat(review_write_bg_black, "alpha", 1f, 0f);
        animator2.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT)); // github에서 라이브러리 가져와야 함.
        animator2.setDuration(600);
        animator2.start();

        // 리뷰 창
        review_write_window.setVisibility(View.VISIBLE);
        ObjectAnimator animator3 = ObjectAnimator.ofFloat(review_write_window, "alpha", 1f, 0f);
        animator3.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT)); // github에서 라이브러리 가져와야 함.
        animator3.setDuration(600);
        animator3.start();

        ObjectAnimator animator4 = ObjectAnimator.ofFloat(review_write_window, "translationY", 0f, 300f);
        animator4.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT)); // github에서 라이브러리 가져와야 함.
        animator4.setDuration(600);
        animator4.start();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                review_write_bg_black.setVisibility(View.GONE);
                review_write_window.setVisibility(View.GONE);
            }
        }, 600);
    }

    // 리뷰창 내용 초기화
    public void clearReviewWindow() {
        review_product_star_num = 0;
        review_content_edittext.setText("");
        for (int i=0; i<attach_image_real_paths.size(); i++) {
            attach_image_real_paths.set(i, "");
            attach_images.get(i).setImageBitmap(null);
        }
        for (int i=0; i<starButtons.size(); i++) {
            starButtons.get(i).setImageResource(R.drawable.mobile00000_start_inactive_icon);
        }
    }

    // 리뷰 내용이 잘 입력되었는지 체크하기
    public boolean checkReview() {
        if (review_product_star_num == 0) {
            callVelyBebeNormalDialog2("안내", "별점을 등록해주세요.");
            return false;
        }

        String content = review_content_edittext.getText().toString().trim();

        if (content.equals("")) {
            callVelyBebeNormalDialog2("안내", "리뷰 내용을 입력해주세요.");
            return false;
        }


        review_product_content = content;


        goSend();


        return true;
    }

    /*===================================================================================================================*/

    // 뒤로가기 할 시 경고 팝업창 띄우기
    public void callVelyBebeNormalDialog(String title, String content) {
        final VelyBebeNormalDialog dialog = new VelyBebeNormalDialog(this);
        dialog.show();
        dialog.startDialogAnimation();
        dialog.setDialogTitle(title);
        dialog.setDialogContent(content);
        dialog.buttonVisibleOptions(true, true);
        // dialog.setCancelButtonText("");
        // dialog.setConfirmButtonText("");
        dialog.setCancelButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setConfirmButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    // 뒤로가기 할 시 경고 팝업창 띄우기
    public void callVelyBebeNormalDialog3(String title, String content) {
        final VelyBebeNormalDialog dialog = new VelyBebeNormalDialog(this);
        dialog.show();
        dialog.startDialogAnimation();
        dialog.setDialogTitle(title);
        dialog.setDialogContent(content);
        dialog.buttonVisibleOptions(true, true);
        // dialog.setCancelButtonText("");
        // dialog.setConfirmButtonText("");
        dialog.setCancelButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setConfirmButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                clearReviewWindow();
                hideReviewWindow();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    public void callVelyBebeNormalDialog4(String title, String content) {
        final VelyBebeNormalDialog dialog = new VelyBebeNormalDialog(this);
        dialog.show();
        dialog.startDialogAnimation();
        dialog.setDialogTitle(title);
        dialog.setDialogContent(content);
        dialog.buttonVisibleOptions(false, true);
        // dialog.setCancelButtonText("");
        // dialog.setConfirmButtonText("");
        dialog.setCancelButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setConfirmButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                clearReviewWindow();
                hideReviewWindow();
            }
        });
    }


    boolean isFinish;
    // 통보 형식의 다이얼로그 호출
    public void callVelyBebeNormalDialog2(String title, String content) {
        final VelyBebeNormalDialog dialog = new VelyBebeNormalDialog(this);
        dialog.show();
        dialog.startDialogAnimation();
        dialog.setDialogTitle(title);
        dialog.setDialogContent(content);
        dialog.buttonVisibleOptions(false, true);


        // dialog.setCancelButtonText("");
        // dialog.setConfirmButtonText("");
        dialog.setCancelButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setConfirmButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (isFinish) {
                    finish();
                }
            }
        });
    }









    private void goSend(){
        int lastExistImageIndex = -1;
        for (int i=0; i<attach_image_real_paths.size(); i++) {
            if (attach_image_real_paths.get(i).equals("")) {
                break;
            } else {
                lastExistImageIndex = i;
            }
        }



        RequestBody requestBody = null;
        if (lastExistImageIndex == 0) {
            requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("act", "mobile10011")
                    .addFormDataPart("member", String.valueOf(new ShareInfo(activity).getMember()))
                    .addFormDataPart("review_product_pk", String.valueOf(review_product_pk))
                    .addFormDataPart("review_product_star_num", String.valueOf(review_product_star_num))
                    .addFormDataPart("review_product_content", review_product_content)
                    .addFormDataPart("image0", "image0.jpg", RequestBody.create(MultipartBody.FORM, new File(attach_image_real_paths.get(0))))
                    .build();
        } else if (lastExistImageIndex == 1) {
            requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("act", "mobile10011")
                    .addFormDataPart("member", String.valueOf(new ShareInfo(activity).getMember()))
                    .addFormDataPart("review_product_pk", String.valueOf(review_product_pk))
                    .addFormDataPart("review_product_star_num", String.valueOf(review_product_star_num))
                    .addFormDataPart("review_product_content", review_product_content)
                    .addFormDataPart("image0", "image0.jpg", RequestBody.create(MultipartBody.FORM, new File(attach_image_real_paths.get(0))))
                    .addFormDataPart("image1", "image1.jpg", RequestBody.create(MultipartBody.FORM, new File(attach_image_real_paths.get(1))))
                    .build();
        } else if (lastExistImageIndex == 2) {
            requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("act", "mobile10011")
                    .addFormDataPart("member", String.valueOf(new ShareInfo(activity).getMember()))
                    .addFormDataPart("review_product_pk", String.valueOf(review_product_pk))
                    .addFormDataPart("review_product_star_num", String.valueOf(review_product_star_num))
                    .addFormDataPart("review_product_content", review_product_content)
                    .addFormDataPart("image0", "image0.jpg", RequestBody.create(MultipartBody.FORM, new File(attach_image_real_paths.get(0))))
                    .addFormDataPart("image1", "image1.jpg", RequestBody.create(MultipartBody.FORM, new File(attach_image_real_paths.get(1))))
                    .addFormDataPart("image2", "image2.jpg", RequestBody.create(MultipartBody.FORM, new File(attach_image_real_paths.get(2))))
                    .build();
        } else if (lastExistImageIndex == 3) {
            requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("act", "mobile10011")
                    .addFormDataPart("member", String.valueOf(new ShareInfo(activity).getMember()))
                    .addFormDataPart("review_product_pk", String.valueOf(review_product_pk))
                    .addFormDataPart("review_product_star_num", String.valueOf(review_product_star_num))
                    .addFormDataPart("review_product_content", review_product_content)
                    .addFormDataPart("image0", "image0.jpg", RequestBody.create(MultipartBody.FORM, new File(attach_image_real_paths.get(0))))
                    .addFormDataPart("image1", "image1.jpg", RequestBody.create(MultipartBody.FORM, new File(attach_image_real_paths.get(1))))
                    .addFormDataPart("image2", "image2.jpg", RequestBody.create(MultipartBody.FORM, new File(attach_image_real_paths.get(2))))
                    .addFormDataPart("image3", "image3.jpg", RequestBody.create(MultipartBody.FORM, new File(attach_image_real_paths.get(3))))
                    .build();
        } else if (lastExistImageIndex == 4) {
            requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("act", "mobile10011")
                    .addFormDataPart("member", String.valueOf(new ShareInfo(activity).getMember()))
                    .addFormDataPart("review_product_pk", String.valueOf(review_product_pk))
                    .addFormDataPart("review_product_star_num", String.valueOf(review_product_star_num))
                    .addFormDataPart("review_product_content", review_product_content)
                    .addFormDataPart("image0", "image0.jpg", RequestBody.create(MultipartBody.FORM, new File(attach_image_real_paths.get(0))))
                    .addFormDataPart("image1", "image1.jpg", RequestBody.create(MultipartBody.FORM, new File(attach_image_real_paths.get(1))))
                    .addFormDataPart("image2", "image2.jpg", RequestBody.create(MultipartBody.FORM, new File(attach_image_real_paths.get(2))))
                    .addFormDataPart("image4", "image4.jpg", RequestBody.create(MultipartBody.FORM, new File(attach_image_real_paths.get(4))))
                    .build();
        }

        final Request request = new Request.Builder()
                .url("http://jwisedom.co.kr/velybebe/outlet.php")
                .post(requestBody)
                .build();

        OkHttpClient client = new OkHttpClient();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.i("MyTags", "실패.."+e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String result = response.body().string();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (result.equals("ok")) {
                            callVelyBebeNormalDialog4("안내", "리뷰가 정상적으로 작성되었습니다.");
                        } else {
                            UseFul.callVelyBebeNormalDialog(activity, "안내", "리뷰를 작성하던 도중 오류가 발생하였습니다. 다시 시도해주세요.");
                        }
                    }
                });


            }
        });
    }


    /*===================================================================================================================*/



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GET_PICTURE_URI) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    Uri uri = data.getData();
                    String realPath = RealPathUtil.getRealPath(activity, uri);
                    attach_image_real_paths.set(selectAttachButtonIndex, realPath);
                    // Log.i("MyTags", "realPath = "+realPath);

                    final Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                    Log.i("MyTags", "bitmap = "+bitmap.toString());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.i("MyTags", "attach_images.setImageBitmap 실행 ");

                            attach_images.get(selectAttachButtonIndex).setImageBitmap(bitmap);
                        }
                    });
                    // Glide.with(mContext).load(data.getData()).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(imageView); // OOM 없애기위해 그레들사용



                } catch (Exception e) {
                    Log.e("test", e.getMessage());
                }
            }
        }
    }


    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        if (isReviewWindowVisibile) {
            callVelyBebeNormalDialog3("경고", "작성했던 내용이 모두 초기화 됩니다. 취소하시겠습니까?");
        } else {
            finish();
        }
        // callVelyBebeNormalDialog("안내", "정말 블리베베 앱을 종료하시겠습니까?"); // 뒤로가기 할 시 경고 팝업창 띄우기
    }
}
