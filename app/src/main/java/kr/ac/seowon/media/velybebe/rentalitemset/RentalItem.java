package kr.ac.seowon.media.velybebe.rentalitemset;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.print.PageRange;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.InLet;
import kr.ac.seowon.media.velybebe.common.ShareInfo;
import kr.ac.seowon.media.velybebe.common.UrlImageGetDisplay;
import kr.ac.seowon.media.velybebe.common.UseFul;

public class RentalItem extends LinearLayout {
    View v;
    AppCompatActivity activity;
    UseFul useFul;
    Context context;

    int gridnum;


    int pk;
    int heart_value;

    LinearLayout body;
    AppCompatImageView imageView;
    AppCompatImageView heart_icon;
    TextView title_textview;
    TextView price_str_textview;
    LinearLayout coupon;
    LinearLayout gift;
    LinearLayout sale;

    boolean isLoading = false;


    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what){
                case 05204: // 해당상품 좋아요 로그 남기기 후처리
                    mobile05204(msg.obj);
                    break;
            }
        }
    };


    public RentalItem(Context context, int gridnum) {
        super(context);
        this.gridnum = gridnum;
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }

    public RentalItem(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }

    public RentalItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }

    public void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        if (gridnum == 1) {
            v = li.inflate(R.layout.rental_item, this, false);
        } else {
            v = li.inflate(R.layout.rental_item_2, this, false);
        }
        addView(v);

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기
    }



    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectObjectAndId() {
        body = (LinearLayout) v.findViewById(R.id.body);
        imageView = (AppCompatImageView) v.findViewById(R.id.imageView);
        heart_icon = (AppCompatImageView) v.findViewById(R.id.heart_icon);
        title_textview = (TextView) v.findViewById(R.id.title_textview);
        price_str_textview = (TextView) v.findViewById(R.id.price_str_textview);
        coupon = (LinearLayout) v.findViewById(R.id.coupon);
        gift = (LinearLayout) v.findViewById(R.id.gift);
        sale = (LinearLayout) v.findViewById(R.id.sale);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.weight = 1f;
        setLayoutParams(params);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        heart_icon.setOnClickListener(heart_value_click_event);
    }

    View.OnClickListener heart_value_click_event = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!isLoading) {
                if (heart_value == 0 || heart_value == 77771002) {
                    mobile05203(77771001);
                } else {
                    mobile05203(77771002);
                }


            } else {
                useFul.showToast("작업이 진행중입니다.");
            }
        }
    };

    // 해당상품 좋아요 로그 남기기
    public void mobile05203(int heartLog) {
        isLoading = true;
        heart_value = heartLog;

        ContentValues params = new ContentValues();
        params.put("act", "mobile05203");
        params.put("member", new ShareInfo(activity).getMember());
        params.put("product_pk", pk);
        params.put("log", heartLog);
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(05204);
        inLet.start();
    }

    // 해당상품 좋아요 로그 남기기 후처리
    public void mobile05204(Object obj) {
        isLoading = false;

        String data = obj.toString();
        Log.i("MyTags", "mobile05204 = "+data);

        if (heart_value == 77771001) {
            heart_icon.setImageResource(R.drawable.ic_mobile05200_heart_on);
            useFul.showToast("해당 상품을 좋아요 등록하셨습니다.");
        } else {
            heart_icon.setImageResource(R.drawable.ic_mobile05200_heart_off);
            useFul.showToast("해당 상품을 좋아요 해제하셨습니다.");
        }
    }




    public void setImageUrl(String s) {
        UrlImageGetDisplay u = new UrlImageGetDisplay(activity);
        u.setImageUrl(s);
        u.setImageView(imageView);
        u.start();
    }

    public void setTitle(String s) {
        title_textview.setText(s);
    }

    public void setPrice(String s) {
        price_str_textview.setText(s);
    }

    public void setHeartValue(int n) {
        heart_value = n;
        if (n == 0 || n == 77771002) {
            heart_icon.setImageResource(R.drawable.ic_mobile05200_heart_off);
        } else {
            heart_icon.setImageResource(R.drawable.ic_mobile05200_heart_on);
        }
    }

    public void setCoupon(int n) {
        if (n != 90001001) {
            coupon.setVisibility(View.VISIBLE);
        } else {
            coupon.setVisibility(View.GONE);
        }
    }

    public void setSale(int n) {
        if (n != 90001001) {
            sale.setVisibility(View.VISIBLE);
        } else {
            sale.setVisibility(View.GONE);
        }
    }

    public void setGift(int n) {
        if (n != 90001001) {
            gift.setVisibility(View.VISIBLE);
        } else {
            gift.setVisibility(View.GONE);
        }
    }

    public void setPk(int n) {
        this.pk = n;
    }

    public void setOnItemClickListener(View.OnClickListener e) {
        setOnClickListener(e);
        setTag(pk);
    }
}
