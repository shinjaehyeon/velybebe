package kr.ac.seowon.media.velybebe.sidebar;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.UseFul;

public class SideBarMenuButton extends LinearLayout {
    View v;
    AppCompatActivity activity;
    Context context;
    UseFul useFul;

    /**************************************************************/


    Button button;
    TextView button_text;
    AppCompatImageView right_icon;
    LinearLayout border_bottom;


    public void setButtonText(String s) {
        button_text.setText(s);
    }

    public void setButtonOnClickListener(View.OnClickListener e) {
        button.setTag(button_text);
        button.setOnClickListener(e);
    }


    /**************************************************************/

    public SideBarMenuButton(Context context) {
        super(context);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }
    public SideBarMenuButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }
    public SideBarMenuButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
        initView();
    }

    public void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.side_bar_menu_button, this, false);
        addView(v);

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기


    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        button = (Button) v.findViewById(R.id.button);
        button_text = (TextView) v.findViewById(R.id.button_text);
        right_icon = (AppCompatImageView) v.findViewById(R.id.right_icon);
        border_bottom = (LinearLayout) v.findViewById(R.id.border_bottom);
    }


    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }
}
