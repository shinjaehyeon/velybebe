package kr.ac.seowon.media.velybebe.signup;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import org.json.JSONException;
import org.json.JSONObject;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.InLet;
import kr.ac.seowon.media.velybebe.common.ShareInfo;
import kr.ac.seowon.media.velybebe.common.TopBar;
import kr.ac.seowon.media.velybebe.common.UseFul;
import kr.ac.seowon.media.velybebe.common.VelyBebeNormalDialog;
import kr.ac.seowon.media.velybebe.login.Mobile03000;

public class Mobile04000 extends AppCompatActivity {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;
    TopBar topBar;

    /*===================================================================================================================*/

    ConstraintLayout best_parent_layout;

    /*===================================================================================================================*/


    boolean isLoading = false;


    String backDialogTitle = "안내";
    String backDialogContent = "현재 회원가입 중이라면 입력한\n정보가 초기화 될 수 있습니다.\n\n뒤로가기 하시겠습니까?";
    String backDialogContent2 = "현재 회원가입 중이라면 입력한\n정보가 초기화 될 수 있습니다.\n\n취소 하시겠습니까?";



    LinearLayout sign_up_prev__next_button;
    Button nagative_button;
    Button positive_button;


    int currentPageNumber = 1;
    Mobile04100 step01_fragment;
    Mobile04200 step02_fragment;
    Mobile04300 step03_fragment;


    /*===================================================================================================================*/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 04002:
                    mobile04002(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };

    /*===================================================================================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile04000);


        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        getIntentInfo(); // 인텐트 정보 받아오기

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기


        setFragment(); // 회원가입 프래그먼트 설정하기

    }

    @Override
    protected void onStart() {
        super.onStart();
        // Log.i("MyTags", "Main onStart 시작");
        new UseFul(activity).setAppRunning(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Log.i("MyTags", "Main onPause 시작");
        new UseFul(activity).setAppRunning(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Log.i("MyTags", "Main onResume 시작");
        new UseFul(activity).setAppRunning(1);
    }






    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(AppCompatActivity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // 인테트 정보 받아오기
    public void getIntentInfo() {

    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        useFul = new UseFul(activity);
        best_parent_layout = (ConstraintLayout) findViewById(R.id.best_parent_layout);
        topBar = (TopBar) findViewById(R.id.topbar);

        /*===================================================================================================================*/

        sign_up_prev__next_button = (LinearLayout) findViewById(R.id.sign_up_prev__next_button);
        nagative_button = (Button) findViewById(R.id.nagative_button);
        positive_button = (Button) findViewById(R.id.positive_button);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {
        topBar.setTitle("회원가입");
        topBar.showTitle();
        topBar.showBackButton();
        topBar.hideLogo();
        topBar.hideMenuButton();
        topBar.hideMyPageButton();
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        topBar.setBackButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callVelyBebeNormalDialog(backDialogTitle, backDialogContent); // 뒤로가기 할 시 경고 팝업창 띄우기
            }
        });


        nagative_button.setOnClickListener(pageChangeButtonEvent);
        positive_button.setOnClickListener(pageChangeButtonEvent);

    }

    // 회원가입 프래그먼트 설정하기
    public void setFragment() {
        step01_fragment = new Mobile04100(activity);

        step02_fragment = new Mobile04200(activity);

        step03_fragment = new Mobile04300(activity);



        showStep01(); // 최초 실행시 첫번째 STEP 01 단계 화면 보여주기
    }

    public void showStep01() {
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment, step01_fragment);
        fragmentTransaction.commit();
    }

    public void showStep02() {
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment, step02_fragment);
        fragmentTransaction.commit();
    }

    public void showStep03() {
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment, step03_fragment);
        fragmentTransaction.commit();
    }

    /*===================================================================================================================*/

    View.OnClickListener pageChangeButtonEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.nagative_button:
                    callVelyBebeNormalDialog(backDialogTitle, backDialogContent2);
                    break;
                case R.id.positive_button:
                    switch (currentPageNumber) {
                        case 1: // 현재 페이지가 STEP 01 일 경우
                            // 약관동의여부 확인하기
                            int result = step01_fragment.isAllChecked();

                            switch (result) {
                                case 1:
                                    callVelyBebeNormalDialog2("안내", "블리베베 서비스 이용약관에 동의해주세요.");
                                    break;
                                case 2:
                                    callVelyBebeNormalDialog2("안내", "블리베베 개인정보처리방침에 동의해주세요.");
                                    break;
                                case 300:
                                    currentPageNumber = 2;
                                    showStep02();
                                    break;
                                default:

                                    break;
                            }

                            break;
                        case 2: // 현재 페이지가 STEP 02 일 경우
                            // 정보 다 입력됬는지 확인하기
                            if (step02_fragment.formCheck()) {
                                String name = step02_fragment.checked_name;
                                String birthday = step02_fragment.checked_birthday;
                                int gender = step02_fragment.checked_gender;
                                String email = step02_fragment.checked_email;
                                String password = step02_fragment.checked_password;
                                String addr1 = step02_fragment.checked_addr1;
                                String addr2 = step02_fragment.checked_addr2;
                                String addr3 = step02_fragment.checked_addr3;
                                String phone_number = step02_fragment.checked_phone_number;

                                mobile04001(name,birthday,gender,email,password,addr1,addr2,addr3,phone_number); // 서버에 회원정보 추가하기
                            }

                            break;
                        case 3: // 현재 페이지가 STEP 03 일 경우



                            break;
                        default:

                            break;
                    }
                    break;
                default:

                    break;
            }
        }
    };

    // 서버에 회원정보 추가하기
    public void mobile04001(String name,String birthday,int gender,String email,String password,String addr1,String addr2,String addr3,String phone_number) {
        ContentValues params = new ContentValues();
        params.put("act", "mobile04001");
        params.put("member", new ShareInfo(this).getMember());
        params.put("name", name);
        params.put("birthday", birthday);
        params.put("gender", gender);
        params.put("email", email);
        params.put("password", password);
        params.put("addr1", addr1);
        params.put("addr2", addr2);
        params.put("addr3", addr3);
        params.put("phone_number", phone_number);

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(04002);
        inLet.start();
    }

    // 서버에 회원정보 추가하기 후처리
    public void mobile04002(Object obj) {
        String data = obj.toString();
        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                currentPageNumber = 3;
                showStep03();

                nagative_button.setVisibility(View.GONE);
                positive_button.setText("로그인하러 가기");
                positive_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        goActivity(Mobile03000.class);
                        finish();
                    }
                });
            } else if (result.equals("no")) {
                callVelyBebeNormalDialog2("안내", "회원가입 도중 문제가 발생하였습니다. 해당 문제가 지속될 경우 관리자에게 문의 바랍니다.");
            } else {
                callVelyBebeNormalDialog2("안내", "회원가입 도중 치명적인 오류가 발생하였습니다. 이 메세지를 보시면 즉시 개발자에게 이 사실을 전해주시길 바랍니다.");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*===================================================================================================================*/

    // 뒤로가기 할 시 경고 팝업창 띄우기
    public void callVelyBebeNormalDialog(String title, String content) {
        final VelyBebeNormalDialog dialog = new VelyBebeNormalDialog(this);
        dialog.show();
        dialog.startDialogAnimation();
        dialog.setDialogTitle(title);
        dialog.setDialogContent(content);
        dialog.buttonVisibleOptions(true, true);
        // dialog.setCancelButtonText("");
        // dialog.setConfirmButtonText("");
        dialog.setCancelButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setConfirmButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    // 통보 형식의 다이얼로그 호출
    public void callVelyBebeNormalDialog2(String title, String content) {
        final VelyBebeNormalDialog dialog = new VelyBebeNormalDialog(this);
        dialog.show();
        dialog.startDialogAnimation();
        dialog.setDialogTitle(title);
        dialog.setDialogContent(content);
        dialog.buttonVisibleOptions(false, true);
        // dialog.setCancelButtonText("");
        // dialog.setConfirmButtonText("");
        dialog.setCancelButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setConfirmButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }


    public void goActivity(Class c) {
        Intent intent = new Intent(activity, c);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    /*===================================================================================================================*/

    @Override
    public void onBackPressed() {
        // super.onBackPressed();

        callVelyBebeNormalDialog(backDialogTitle, backDialogContent); // 뒤로가기 할 시 경고 팝업창 띄우기
    }
}
