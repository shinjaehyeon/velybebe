package kr.ac.seowon.media.velybebe.signup;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.CheckBoxA;
import kr.ac.seowon.media.velybebe.common.InLet;
import kr.ac.seowon.media.velybebe.common.ShareInfo;
import kr.ac.seowon.media.velybebe.common.TopBar;
import kr.ac.seowon.media.velybebe.common.UseFul;

@SuppressLint("ValidFragment")
// 회원가입 STEP 01
public class Mobile04100 extends Fragment {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;

    /*===================================================================================================================*/

    View rootView;

    LinearLayout all_okay_button_box;
    CheckBoxA all_okay_checkbox;

    LinearLayout service_okay_button_box;
    CheckBoxA service_okay_checkbox;

    LinearLayout personal_okay_button_box;
    CheckBoxA personal_okay_checkbox;

    ArrayList<TextView> termsTextView = new ArrayList<TextView>();


    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 4102: // 약관동읜 내용 가져오기 후처리
                    mobile04102(msg.obj);
                    break;
            }
        }
    };

    /*===================================================================================================================*/

    public Mobile04100() {

    }

    public Mobile04100(AppCompatActivity activity) {
        this.activity = activity;
    }

    /*===================================================================================================================*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.mobile04100, parentViewGroup, false);

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기

        mobile04101(); // 약관동의 내용 가져오기

        return rootView;
    }

    // 약관동의 내용 가져오기
    public void mobile04101() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile04101");
        params.put("member", new ShareInfo(activity).getMember());
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(4102);
        inLet.start();
    }

    // 약관동의 내용 가져오기 후처리
    public void mobile04102(Object obj) {
        String data = obj.toString();
        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");
            if (result.equals("ok")) {

                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    String type = jsonObject1.getString("type");
                    String description = jsonObject1.getString("description");

                    termsTextView.get(i).setText(description);
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        useFul = new UseFul(activity);

        /*===================================================================================================================*/

        all_okay_button_box = (LinearLayout) rootView.findViewById(R.id.all_okay_button_box);
        all_okay_checkbox = (CheckBoxA) rootView.findViewById(R.id.all_okay_checkbox);

        service_okay_button_box = (LinearLayout) rootView.findViewById(R.id.service_okay_button_box);
        service_okay_checkbox = (CheckBoxA) rootView.findViewById(R.id.service_okay_checkbox);

        personal_okay_button_box = (LinearLayout) rootView.findViewById(R.id.personal_okay_button_box);
        personal_okay_checkbox = (CheckBoxA) rootView.findViewById(R.id.personal_okay_checkbox);


        termsTextView.add((TextView) rootView.findViewById(R.id.service_content));
        termsTextView.add((TextView) rootView.findViewById(R.id.personal_content));
    }


    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {
        all_okay_checkbox.setChainView(all_okay_button_box);
        service_okay_checkbox.setChainView(service_okay_button_box);
        personal_okay_checkbox.setChainView(personal_okay_button_box);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        all_okay_checkbox.setCheckboxCustomEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (all_okay_checkbox.getValue() == true) {
                    all_okay_checkbox.setChecked(false);
                    service_okay_checkbox.setChecked(false);
                    personal_okay_checkbox.setChecked(false);
                } else {
                    all_okay_checkbox.setChecked(true);
                    service_okay_checkbox.setChecked(true);
                    personal_okay_checkbox.setChecked(true);
                }
            }
        });

    }


    /*===================================================================================================================*/

    public int isAllChecked() {
        if (!service_okay_checkbox.getValue()) {
            return 1;
        }

        if (!personal_okay_checkbox.getValue()) {
            return 2;
        }

        return 300;
    }
}
