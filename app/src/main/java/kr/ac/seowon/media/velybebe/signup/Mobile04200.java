package kr.ac.seowon.media.velybebe.signup;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.InLet;
import kr.ac.seowon.media.velybebe.common.RadioButtonA;
import kr.ac.seowon.media.velybebe.common.RadioButtonManager;
import kr.ac.seowon.media.velybebe.common.ShareInfo;
import kr.ac.seowon.media.velybebe.common.UseFul;
import kr.ac.seowon.media.velybebe.common.VelyBebeNormalDialog;

@SuppressLint("ValidFragment")
// 회원가입 STEP 01
public class Mobile04200 extends Fragment {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;

    /*===================================================================================================================*/

    View rootView;


    boolean isLoading = false;


    EditText name_edittext;


    EditText birthday_edittext;


    RadioButtonManager radioButtonManager;
    LinearLayout male_radio_box;
    LinearLayout female_radio_box;
    RadioButtonA male_radio;
    RadioButtonA female_radio;


    boolean is_email_overlap_checked = false;
    EditText email_edittext;
    Button email_overlap_check_button;




    EditText pw_edittext;
    EditText pw_check_edittext;



    Button addr_search_button;
    EditText post_number_edittext;
    EditText basic_addr_edittext;
    EditText detail_addr_edittext;


    boolean isPhoneNumberChecked = false;
    EditText phone_number_edittext;
    Button get_number_button;
    EditText confirm_number_edittext;
    Button confirm_number_check_button;


    Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 04202: // 이메일 중복 확인하기 후처리
                    mobile04202(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };


    /*===================================================================================================================*/

    private Handler handler;

    /*===================================================================================================================*/

    public Mobile04200() {

    }

    public Mobile04200(AppCompatActivity activity) {
        this.activity = activity;
    }

    /*===================================================================================================================*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.mobile04200, parentViewGroup, false);

        // mVerificationInProgress = savedInstanceState.getBoolean(KEY_VERIFY_IN_PROGRESS);

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기

        handler = new Handler();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                init_webView();
            }
        }, 500);



        setPhoneSMS();

        return rootView;
    }



    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        useFul = new UseFul(activity);

        /*===================================================================================================================*/

        radioButtonManager = new RadioButtonManager(activity);

        male_radio_box = (LinearLayout) rootView.findViewById(R.id.male_radio_box);
        female_radio_box = (LinearLayout) rootView.findViewById(R.id.female_radio_box);

        male_radio = (RadioButtonA) rootView.findViewById(R.id.male_radio);
        male_radio.setValue(30003001);
        male_radio.setContainView(male_radio_box);
        female_radio = (RadioButtonA) rootView.findViewById(R.id.female_radio);
        female_radio.setValue(30003002);
        female_radio.setContainView(female_radio_box);

        name_edittext = (EditText) rootView.findViewById(R.id.name_edittext);
        birthday_edittext = (EditText) rootView.findViewById(R.id.birthday_edittext);

        email_edittext = (EditText) rootView.findViewById(R.id.email_edittext);
        email_overlap_check_button = (Button) rootView.findViewById(R.id.email_overlap_check_button);
        pw_edittext = (EditText) rootView.findViewById(R.id.pw_edittext);
        pw_check_edittext = (EditText) rootView.findViewById(R.id.pw_check_edittext);

        addr_search_button = (Button) rootView.findViewById(R.id.addr_search_button);
        post_number_edittext = (EditText) rootView.findViewById(R.id.post_number_edittext);
        basic_addr_edittext = (EditText) rootView.findViewById(R.id.basic_addr_edittext);
        detail_addr_edittext = (EditText) rootView.findViewById(R.id.detail_addr_edittext);


        daum_webview_box = (ConstraintLayout) rootView.findViewById(R.id.daum_webview_box);
        daum_webView = (WebView) rootView.findViewById(R.id.daum_webview);




        phone_number_edittext = (EditText) rootView.findViewById(R.id.phone_number_edittext);
        get_number_button = (Button) rootView.findViewById(R.id.get_number_button);
        confirm_number_edittext = (EditText) rootView.findViewById(R.id.confirm_number_edittext);
        confirm_number_check_button = (Button) rootView.findViewById(R.id.confirm_number_check_button);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {
        radioButtonManager.pushRadioButton(male_radio);
        radioButtonManager.pushRadioButton(female_radio);
        radioButtonManager.pushComplete();
        radioButtonManager.setDefaultCheckedIndex(0);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

        addr_search_button.setOnClickListener(addr_search_button_event);
        daum_webview_box.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                daum_webview_box.setVisibility(View.GONE);
                daum_webView.setVisibility(View.GONE);
            }
        });


        get_number_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validatePhoneNumber()) {
                    return;
                }

                phone_number_edittext.setEnabled(false);

                String phoneNumber = phone_number_edittext.getText().toString();
                phoneNumber = "+82"+phoneNumber.substring(1, phoneNumber.length());

                Log.i("MyTYags", "phoneNumber = "+phoneNumber);

                callVelyBebeNormalDialog("안내", "인증번호가 발송되었습니다. 잠시만 기다려주시면 자동인증이 완료됩니다!");
                startPhoneNumberVerification(phoneNumber);
            }
        });


        confirm_number_check_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = confirm_number_edittext.getText().toString();
                if (TextUtils.isEmpty(code)) {
                    useFul.showToast("공백입니다.");
                    return;
                }

                verifyPhoneNumberWithCode(mVerificationId, code);
            }
        });

        // 이메일중복확인 버튼 클릭시 이벤트 설정하기
        email_overlap_check_button.setOnClickListener(emailOverlapButtonEvent);

        // 이메일 입력할 때마다 중복확인 false로 바꾸기
        email_edittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                is_email_overlap_checked = false;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                is_email_overlap_checked = false;
            }

            @Override
            public void afterTextChanged(Editable s) {
                is_email_overlap_checked = false;
            }
        });
    }

    View.OnClickListener addr_search_button_event = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            daum_webview_box.setVisibility(View.VISIBLE);
            daum_webView.setVisibility(View.VISIBLE);
        }
    };

    View.OnClickListener emailOverlapButtonEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!isLoading) {
                String email = email_edittext.getText().toString();
                if (email.trim().equals("")) {
                    callVelyBebeNormalDialog("안내", "이메일을 입력해주세요.");
                } else if (useFul.isExistEmptySpace(email)) {
                    callVelyBebeNormalDialog("안내", "이메일에는 공백이 들어갈 수 없습니다. 다시 입력해주세요.");
                } else if (!useFul.isValidEmail(email)) {
                    callVelyBebeNormalDialog("안내", "이메일 양식이 올바르지 않습니다. 다시 입력해주세요.");
                } else {
                    Log.i("MyTags", "mobile04201() 실행됨");

                    isLoading = true;
                    mobile04201(email); // 이메일 중복 확인하기
                }
            }
        }
    };

    // 이메일 중복 확인하기
    public void mobile04201(String email) {
        ContentValues params = new ContentValues();
        params.put("act", "mobile04201");
        params.put("member", new ShareInfo(activity).getMember());
        params.put("email", email);
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler2);
        inLet.setHandlerRequestNumber(04202);
        inLet.start();
    }

    // 이메일 중복 확인하기 후처리
    public void mobile04202(Object obj) {
        String data = obj.toString();
        Log.i("MyTags", "data = "+data);

        isLoading = false;

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");
            if (result.equals("ok")) {
                is_email_overlap_checked = true;
                callVelyBebeNormalDialog("안내", "이용 가능한 이메일 입니다.");
            } else if (result.equals("no")) {
                is_email_overlap_checked = false;
                callVelyBebeNormalDialog("안내", "이메일 중복확인 도중 오류가 발생하였습니다.");
            } else if (result.equals("already")) {
                is_email_overlap_checked = false;
                callVelyBebeNormalDialog("안내", "이미 가입되어 있는 이메일 입니다.");
            } else {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    String checked_name;
    String checked_birthday;
    int checked_gender;
    String checked_email;
    String checked_password;
    String checked_addr1;
    String checked_addr2;
    String checked_addr3;
    String checked_phone_number;

    public boolean formCheck() {
        // 이름 체크
        String name = name_edittext.getText().toString();
        if (name.trim().equals("")) {
            callVelyBebeNormalDialog("안내", "이름을 입력해주세요.");
            return false;
        }
        checked_name = name;


        // 생년월일 체크
        String birthday = birthday_edittext.getText().toString();
        if (!useFul.dateCheck(birthday, "yyyyMMdd")) {
            callVelyBebeNormalDialog("안내", "생년월일을 정해진 양식대로 적어주세요.");
            return false;
        }
        checked_birthday = birthday;



        // 성별 체크
        int gender = radioButtonManager.getCheckedRadioValue();
        if (gender == -1) {
            callVelyBebeNormalDialog("안내", "성별을 선택해주세요.");
            return false;
        }
        checked_gender = gender;



        // 이메일 체크
        String email = email_edittext.getText().toString();
        if (email.trim().equals("")) {
            callVelyBebeNormalDialog("안내", "이메일을 입력해주세요.");
            return false;
        }

        if (useFul.isExistEmptySpace(email)) {
            callVelyBebeNormalDialog("안내", "이메일에는 공백이 들어갈 수 없습니다. 다시 입력해주세요.");
            return false;
        }

        if (!useFul.isValidEmail(email)) {
            callVelyBebeNormalDialog("안내", "이메일 양식이 올바르지 않습니다. 다시 입력해주세요.");
            return false;
        }

        if (!is_email_overlap_checked) {
            callVelyBebeNormalDialog("안내", "이메일 중복확인을 해주세요.");
            return false;
        }

        checked_email = email;



        // 비밀번호 체크
        String password = pw_edittext.getText().toString();
        if (password.trim().equals("")) {
            callVelyBebeNormalDialog("안내", "비밀번호를 입력해주세요.");
            return false;
        }

        if (!useFul.validationPasswd(password)) {
            callVelyBebeNormalDialog("안내", "비밀번호는 8자 ~ 16자 이어야 하며, 숫자, 문자, 특수문자가 각각 1개 이상 포함되어야 합니다.");
            return false;
        }

        String password_check = pw_check_edittext.getText().toString();
        if (!password.equals(password_check)) {
            callVelyBebeNormalDialog("안내", "비밀번호와 비밀번호확인이 일치하지 않습니다. 다시 확인해주세요.");
            return false;
        }

        checked_password = password;



        // 주소 체크
        String post_number = post_number_edittext.getText().toString();
        String basic_addr = basic_addr_edittext.getText().toString();
        String detail_addr = detail_addr_edittext.getText().toString();

        if (post_number.equals("")) {
            callVelyBebeNormalDialog("안내", "주소검색을 통해 주소를 입력해주세요.");
            return false;
        }

        if (detail_addr.equals("")) {
            callVelyBebeNormalDialog("안내", "나머지 상세주소를 적어주세요. \n렌탈 배송지 등으로 활용될 수 있습니다.");
            return false;
        }

        checked_addr1 = post_number;
        checked_addr2 = basic_addr;
        checked_addr3 = detail_addr;




        // 휴대폰 체크
        String phone_number = phone_number_edittext.getText().toString();
        if (!isPhoneNumberChecked) {
            callVelyBebeNormalDialog("안내", "휴대폰 인증을 해주세요.");
            return false;
        }

        checked_phone_number = phone_number;


        return true;
    }

    public void callVelyBebeNormalDialog(String title, String content) {
        final VelyBebeNormalDialog dialog = new VelyBebeNormalDialog(activity);
        dialog.show();
        dialog.setDialogTitle(title);
        dialog.setDialogContent(content);
        dialog.buttonVisibleOptions(false, true);
        // dialog.setCancelButtonText("");
        // dialog.setConfirmButtonText("");
        dialog.setCancelButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setConfirmButtonEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }


    /*===================================================================================================================*/
    // 주소 검색 부분 시작

    private ConstraintLayout daum_webview_box;
    private WebView daum_webView;


    public void init_webView() {

        // JavaScript 허용
        daum_webView.getSettings().setJavaScriptEnabled(true);

        // JavaScript의 window.open 허용
        daum_webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        // JavaScript이벤트에 대응할 함수를 정의 한 클래스를 붙여줌
        daum_webView.addJavascriptInterface(new AndroidBridge(), "TestApp");

        // web client 를 chrome 으로 설정
        daum_webView.setWebChromeClient(new WebChromeClient());

        // webview url load. php 파일 주소
        daum_webView.loadUrl("http://jwisedom.co.kr/velybebe/daum_address.php");

    }

    private class AndroidBridge {
        @JavascriptInterface
        public void setAddress(final String arg1, final String arg2, final String arg3) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    daum_webview_box.setVisibility(View.GONE);
                    daum_webView.setVisibility(View.GONE);


                    post_number_edittext.setText(arg1);

                    String basic_addr_string = arg2;
                    if (!arg3.equals("")) {
                        basic_addr_string += " ("+arg3+")";
                    }
                    basic_addr_edittext.setText(basic_addr_string);

                    Log.i("MtYags", "arg1 = "+arg1);
                    Log.i("MtYags", "arg2 = "+arg2);
                    Log.i("MtYags", "arg3 = "+arg3);
                    // daum_result.setText(String.format("(%s) %s %s", arg1, arg2, arg3));
                    // WebView를 초기화 하지않으면 재사용할 수 없음
                    init_webView();
                }
            });
        }
    }


    // 주소 검색 부분 종료
    /*===================================================================================================================*/




    /*===================================================================================================================*/
    // 휴대전화 인증 부분 시작

    private static final String TAG = "PhoneAuthActivity";

    private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";

    private static final int STATE_INITIALIZED = 1;
    private static final int STATE_CODE_SENT = 2;
    private static final int STATE_VERIFY_FAILED = 3;
    private static final int STATE_VERIFY_SUCCESS = 4;
    private static final int STATE_SIGNIN_FAILED = 5;
    private static final int STATE_SIGNIN_SUCCESS = 6;

    // [START declare_auth]
    private FirebaseAuth mAuth;
    // [END declare_auth]

    private boolean mVerificationInProgress = false;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;




    public void setPhoneSMS() {
        mAuth = FirebaseAuth.getInstance();

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {

                Log.d(TAG, "onVerificationCompleted:" + credential);
                mVerificationInProgress = false;

                updateUI(STATE_VERIFY_SUCCESS, credential);
                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Log.w(TAG, "onVerificationFailed", e);

                mVerificationInProgress = false;
                if (e instanceof FirebaseAuthInvalidCredentialsException) {

                    phone_number_edittext.setEnabled(true);
                    useFul.showToast("휴대폰번호 양식에 맞지 않습니다.");

                } else if (e instanceof FirebaseTooManyRequestsException) {

                    // Snackbar.make(findViewById(android.R.id.content), "Quota exceeded.", Snackbar.LENGTH_SHORT).show();

                }

                updateUI(STATE_VERIFY_FAILED);
            }

            @Override
            public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token) {
                Log.d(TAG, "onCodeSent:" + verificationId);

                mVerificationId = verificationId;
                mResendToken = token;

                updateUI(STATE_CODE_SENT);
            }
        };
    }


    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);

        if (mVerificationInProgress && validatePhoneNumber()) {
            startPhoneNumberVerification(phone_number_edittext.getText().toString());
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_VERIFY_IN_PROGRESS, mVerificationInProgress);
    }

    private void startPhoneNumberVerification(String phoneNumber) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(phoneNumber, 60, TimeUnit.SECONDS, activity, mCallbacks);
        mVerificationInProgress = true;
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithPhoneAuthCredential(credential);
    }

    private void resendVerificationCode(String phoneNumber, PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(phoneNumber, 60, TimeUnit.SECONDS, activity, mCallbacks, token);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential).addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success");

                    FirebaseUser user = task.getResult().getUser();
                    // [START_EXCLUDE]
                    updateUI(STATE_SIGNIN_SUCCESS, user);
                    // [END_EXCLUDE]
                } else {
                    // Sign in failed, display a message and update the UI
                    Log.w(TAG, "signInWithCredential:failure", task.getException());
                    if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                        // The verification code entered was invalid
                        // [START_EXCLUDE silent]
                        // mVerificationField.setError("Invalid code.");
                        phone_number_edittext.setEnabled(true);
                        useFul.showToast("휴대폰 인증 시도중 오류가 발생하였습니다.");
                        // [END_EXCLUDE]
                    }
                    // [START_EXCLUDE silent]
                    // Update UI
                    updateUI(STATE_SIGNIN_FAILED);
                    // [END_EXCLUDE]
                }
            }
        });
    }

    private void signOut() {
        mAuth.signOut();
        updateUI(STATE_INITIALIZED);
    }

    private void updateUI(int uiState) {
        updateUI(uiState, mAuth.getCurrentUser(), null);
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            updateUI(STATE_SIGNIN_SUCCESS, user);
        } else {
            updateUI(STATE_INITIALIZED);
        }
    }

    private void updateUI(int uiState, FirebaseUser user) {
        updateUI(uiState, user, null);
    }

    private void updateUI(int uiState, PhoneAuthCredential cred) {
        updateUI(uiState, null, cred);
    }

    private void updateUI(int uiState, FirebaseUser user, PhoneAuthCredential cred) {
        switch (uiState) {
            case STATE_INITIALIZED:
                // Initialized state, show only the phone number field and start button

                // mDetailText.setText(null);
                break;
            case STATE_CODE_SENT:
                // Code sent state, show the verification field, the
                // mDetailText.setText(R.string.status_code_sent);
                break;
            case STATE_VERIFY_FAILED:
                // Verification has failed, show all options
                // mDetailText.setText(R.string.status_verification_failed);
                break;
            case STATE_VERIFY_SUCCESS:
                // mDetailText.setText(R.string.status_verification_succeeded);
                // Set the verification text based on the credential
                Log.i("MyYTASS", "STATE_VERIFY_SUCCESS 실행");

                if (cred != null) {
                    if (cred.getSmsCode() != null) {
                        phone_number_edittext.setEnabled(false);
                        confirm_number_edittext.setEnabled(false);

                        get_number_button.setTextColor(Color.parseColor("#777777"));
                        get_number_button.setBackgroundResource(R.drawable.ripple_button_type_3);
                        get_number_button.setOnClickListener(null);

                        confirm_number_check_button.setText("인증완료");
                        confirm_number_check_button.setOnClickListener(null);
                        confirm_number_check_button.setBackgroundResource(R.drawable.ripple_button_type_5);

                        isPhoneNumberChecked = true;

                        Log.i("MyYTASS", "cred.getSmsCode() = "+cred.getSmsCode());
                        confirm_number_edittext.setText(cred.getSmsCode());
                    } else {
                        confirm_number_edittext.setText("인증번호 입력!");
                    }
                }

                break;
            case STATE_SIGNIN_FAILED:
                // No-op, handled by sign-in check
                // mDetailText.setText(R.string.status_sign_in_failed);
                break;
            case STATE_SIGNIN_SUCCESS:
                // Np-op, handled by sign-in check
                break;
        }

        if (user == null) {
            // Signed out
            // mPhoneNumberViews.setVisibility(View.VISIBLE);
            // mSignedInViews.setVisibility(View.GONE);

            // mStatusText.setText(R.string.signed_out);
        } else {
            // Signed in
            // mPhoneNumberViews.setVisibility(View.GONE);
            // mSignedInViews.setVisibility(View.VISIBLE);

            // enableViews(mPhoneNumberField, mVerificationField);
            // phone_number_edittext.setText(null);
            // confirm_number_edittext.setText(null);

            // mStatusText.setText(R.string.signed_in);
            // mDetailText.setText(getString(R.string.firebase_status_fmt, user.getUid()));
        }
    }

    private boolean validatePhoneNumber() {
        String phoneNumber = phone_number_edittext.getText().toString();
        if (TextUtils.isEmpty(phoneNumber)) {
            // phone_number_edittext.setError("Invalid phone number.");
            useFul.showToast("휴대폰 번호 양식에 맞지 않습니다.");
            return false;
        }

        return true;
    }

//    @Override
//    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.button_start_verification:
//                if (!validatePhoneNumber()) {
//                    return;
//                }
//
//                startPhoneNumberVerification(mPhoneNumberField.getText().toString());
//                break;
//            case R.id.button_verify_phone:
//                String code = mVerificationField.getText().toString();
//                if (TextUtils.isEmpty(code)) {
//                    mVerificationField.setError("Cannot be empty.");
//                    return;
//                }
//
//                verifyPhoneNumberWithCode(mVerificationId, code);
//                break;
//            case R.id.button_resend:
//                resendVerificationCode(mPhoneNumberField.getText().toString(), mResendToken);
//                break;
//            case R.id.sign_out_button:
//                signOut();
//                break;
//        }
//    }

    // 휴대전화 인증 부분 종료
    /*===================================================================================================================*/
}
