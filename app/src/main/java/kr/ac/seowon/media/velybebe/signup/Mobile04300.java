package kr.ac.seowon.media.velybebe.signup;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.UseFul;

@SuppressLint("ValidFragment")
// 회원가입 STEP 01
public class Mobile04300 extends Fragment {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;

    /*===================================================================================================================*/

    View rootView;

    /*===================================================================================================================*/

    public Mobile04300() {

    }

    public Mobile04300(AppCompatActivity activity) {
        this.activity = activity;
    }

    /*===================================================================================================================*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.mobile04300, parentViewGroup, false);

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기


        return rootView;
    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        useFul = new UseFul(activity);

        /*===================================================================================================================*/


    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {


    }


    /*===================================================================================================================*/


}
