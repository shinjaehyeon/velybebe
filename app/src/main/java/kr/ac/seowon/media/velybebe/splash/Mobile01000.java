package kr.ac.seowon.media.velybebe.splash;

import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import kr.ac.seowon.media.velybebe.R;
import kr.ac.seowon.media.velybebe.common.InLet;
import kr.ac.seowon.media.velybebe.common.PermissionCheck;
import kr.ac.seowon.media.velybebe.common.ShareInfo;
import kr.ac.seowon.media.velybebe.common.UseFul;
import kr.ac.seowon.media.velybebe.eventdetail.Mobile08000;
import kr.ac.seowon.media.velybebe.login.Mobile03000;
import kr.ac.seowon.media.velybebe.main.Mobile05000;
import kr.ac.seowon.media.velybebe.mypage.Mobile09000;
import kr.ac.seowon.media.velybebe.permission.Mobile02000;
import kr.ac.seowon.media.velybebe.rentaldetail.Mobile06000;

public class Mobile01000 extends AppCompatActivity {
    AppCompatActivity activity;
    Context context;

    /*===================================================================================================================*/

    ConstraintLayout best_parent_layout;

    int countNumber;
    int loading_bar_box_width;

    ImageView logoImageView;
    ConstraintLayout loading_bar_box;
    LinearLayout loading_bar;

    Timer timer;

    PermissionCheck permissionCheck;

    int linkPage;
    int linkPageValue;

    /*===================================================================================================================*/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 01002:
                    mobile01002(msg.obj);
                    break;
                case 1111:
                    switch (Integer.parseInt(msg.obj.toString())) {
                        case 1:
                            ScaleAnimation ani = new ScaleAnimation (0f, 0.3f, 1f, 1f, 0, 0);
                            ani.setFillAfter(true);
                            ani.setDuration(500);
                            ani.setFillEnabled(true);
                            ani.setInterpolator(AnimationUtils.loadInterpolator(activity, android.R.anim.decelerate_interpolator));
                            loading_bar.startAnimation(ani);
                            break;
                        case 2:
                            Log.i("MyTags", "2 실행");
                            ScaleAnimation ani2 = new ScaleAnimation (0.3f, 0.65f, 1f, 1f, 0, 0);
                            ani2.setFillAfter(true);
                            ani2.setDuration(350);
                            ani2.setFillEnabled(true);
                            ani2.setInterpolator(AnimationUtils.loadInterpolator(activity, android.R.anim.decelerate_interpolator));
                            loading_bar.startAnimation(ani2);
                            break;
                        case 3:
                            timer.cancel();

                            Log.i("MyTags", "3 실행");
                            ScaleAnimation ani3 = new ScaleAnimation (0.65f, 1f, 1f, 1f, 0, 0);
                            ani3.setFillAfter(true);
                            ani3.setDuration(700);
                            ani3.setFillEnabled(true);
                            ani3.setInterpolator(AnimationUtils.loadInterpolator(activity, android.R.anim.decelerate_interpolator));
                            loading_bar.startAnimation(ani3);

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    // code..
                                    mobile01001(); // 앱 구동전 필요한 데이터 가져오기
                                }
                            }, 700);


                            break;
                        default:

                            break;
                    }
                    break;
                default:

                    break;
            }
        }
    };

    /*===================================================================================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile01000);


        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        getIntentInfo(); // 인텐트 정보 받아오기

        setConnectObjectAndId(); // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기


        loading_bar_box.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        loading_bar_box_width = loading_bar_box.getMeasuredWidth();

        timer = new Timer();
        timer.schedule(loadingBarTimer, 0, 800);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Log.i("MyTags", "Main onStart 시작");
        new UseFul(activity).setAppRunning(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Log.i("MyTags", "Main onPause 시작");
        new UseFul(activity).setAppRunning(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Log.i("MyTags", "Main onResume 시작");
        new UseFul(activity).setAppRunning(1);
    }






    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(AppCompatActivity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // 인테트 정보 받아오기
    public void getIntentInfo() {
        linkPage = getIntent().getIntExtra("linkPage", -1);
        linkPageValue = getIntent().getIntExtra("linkPageValue", -1);
    }

    // xml 레이아웃에 있는 객체 또는 코드상의 객체를 변수와 연결하기
    public void setConnectObjectAndId() {
        best_parent_layout = (ConstraintLayout) findViewById(R.id.best_parent_layout);

        logoImageView = (ImageView) findViewById(R.id.logoImageView);
        loading_bar_box = (ConstraintLayout) findViewById(R.id.loading_bar_box);
        loading_bar = (LinearLayout) findViewById(R.id.loading_bar);

        permissionCheck = new PermissionCheck(this);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }

    /*===================================================================================================================*/

    TimerTask loadingBarTimer = new TimerTask() {
        //TimerTask 추상클래스를 선언하자마자 run()을 강제로 정의하도록 한다.
        @Override
        public void run() {
            countNumber++;

            Message msg = handler.obtainMessage();
            msg.obj = countNumber;
            msg.what = 1111;

            handler.sendMessage(msg);
        }
    };

    // 앱 구동전 필요한 데이터 가져오기
    public void mobile01001() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile01001");
        params.put("member", new ShareInfo(this).getMember());
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(01002);
        inLet.start();
    }
    // 앱 구동전 필요한 데이터 가져오기 후처리
    public void mobile01002(Object obj) {
        String data = obj.toString();
        Log.i("MyTags", "data = "+data);

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {



                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                    // 안드로이드 버전이 6.0 미만 일 때
                    // 권한 체크를 하지 않음
                    // 앱에 로그인되어 있는지 여부 확인
                    if (new ShareInfo(activity).getMember() != 0) {
                        // 로그인 되어 있으면
                        // 메인 페이지로 이동
                        if (linkPage == -1) {
                            goActivity(Mobile05000.class);
                        } else {
                            goLinkPage();
                        }


                    } else {
                        // 로그인 되어 있지 않으면
                        // 로그인 페이지로 이동
                        goActivity(Mobile03000.class);

                    }

                } else {
                    // 안드로이드 버전이 6.0 이상 일 때
                    // 권한 체크를 반드시 해야함
                    if (!permissionCheck.checkPermissions()) {
                        // 하나라도 권한이 허용 되지 않은게 있다면
                        // 접근권한 페이지로 이동
                        goActivity(Mobile02000.class);
                    } else {
                        // 모든 권한이 허용 되었다면
                        // 앱에 로그인되어 있는지 여부 확인
                        if (new ShareInfo(activity).getMember() != 0) {
                            // 로그인 되어 있으면
                            // 메인 페이지로 이동
                            if (linkPage == -1) {
                                goActivity(Mobile05000.class);
                            } else {
                                goLinkPage();
                            }

                        } else {
                            // 로그인 되어 있지 않으면
                            // 로그인 페이지로 이동
                            goActivity(Mobile03000.class);
                        }
                    }
                }



            } else if (result.equals("no")) {

            } else {

            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }


    public void goActivity(Class c) {
        Intent intent = new Intent(activity, c);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }


    public void goLinkPage() {
        switch (linkPage) {
            case 0: // 없음
                goActivity(Mobile05000.class);
                break;
            case 999991001: // 메인페이지
                Intent intent = new Intent(getApplicationContext(), Mobile05000.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

                break;
            case 999991002: // 마이페이지
                Intent intent3 = new Intent(getApplicationContext(), Mobile05000.class);
                intent3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent3);

                Intent intent2 = new Intent(getApplicationContext(), Mobile09000.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent2);

                finish();
                break;
            case 999991003: // 제품상세페이지
                Intent intent4 = new Intent(getApplicationContext(), Mobile05000.class);
                intent4.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent4);

                Intent intent5 = new Intent(getApplicationContext(), Mobile06000.class);
                intent5.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent5.putExtra("pk", linkPageValue);
                startActivity(intent5);

                finish();
                break;
            case 999991004: // 이벤트상세페이지
                Intent intent6 = new Intent(getApplicationContext(), Mobile05000.class);
                intent6.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent6);

                Intent intent7 = new Intent(getApplicationContext(), Mobile08000.class);
                intent7.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent7.putExtra("pk", linkPageValue);
                startActivity(intent7);

                finish();
                break;
        }
    }

    /*===================================================================================================================*/

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
